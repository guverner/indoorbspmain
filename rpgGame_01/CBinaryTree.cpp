#include "CBinaryTree.h"

CNode::CNode()
{
	left = NULL;
	right = NULL;
	id = -1;
	type = NORMAL_NODE;
}

CNode::~CNode()
{
	//NEED TO BE CHANGED!!!
	delete left;
	left = NULL;
	delete right;
	right = NULL;
}

void CNode::addDataPoly(tWall* dataPoly_)
{
	m_vDataPolygons.push_back(dataPoly_);
}

void CNode::addDataPoly(tWall& dataPoly_)
{
	m_vDataPolygons.push_back(&dataPoly_);
}

void CNode::addDataPolys(vector<tWall*>& dataPolys_)
{
	for(int indDataPoly = 0; indDataPoly < dataPolys_.size(); indDataPoly++)
	{
		m_vDataPolygons.push_back(dataPolys_[indDataPoly]);
	}
}

vector<tWall*>* CNode::getDataPolys()
{
	return &m_vDataPolygons;
}

vector<tWall*>* CNode::getDivPolys()
{
	return &m_vDivPolygons;
}

tWall* CNode::getDivPoly(int index)
{
	for(int indDivPoly = 0; indDivPoly < m_vDivPolygons.size(); indDivPoly++)
	{
		if(indDivPoly == index)
		{
			return m_vDivPolygons[indDivPoly];
		}
	}
}

void CNode::addDivPoly(tWall& divPoly)
{
	tWall* divWall = new tWall(divPoly);
	m_vDivPolygons.push_back(divWall);
}

void CNode::addDivPoly(tWall* divPoly)
{
	m_vDivPolygons.push_back(divPoly);
}

CBinaryTree::CBinaryTree()
{
	numNodes = 0;
	root = NULL;
}

CBinaryTree::CBinaryTree(vector<tWall*>& walls_)
{
	numNodes = 0;
	root = NULL;

	createBinaryTree(walls_);
}

CBinaryTree::~CBinaryTree()
{
	if(root != NULL)
	{
		delete root;
		root = NULL;
	}
}

void CBinaryTree::findMidLeftRightSets(vector<tWall*>& divPlanes_, tWall& mainDivPlane_,
							vector<tWall*>& return_leftPlanes, vector<tWall*>& return_rightPlanes,
							vector<tWall*>& return_midPlanes)
{
	vector<tWall*> leftWalls;
	vector<tWall*> rightWalls;
	vector<tWall*> middleWalls;

	for(int indDivPlane = 0; indDivPlane < divPlanes_.size(); indDivPlane++)
	{
		tPlane mainDivPlane(mainDivPlane_.getPlain());

		//find location of the first vertice of current plane (wall)
		int onLeftResult1 = onLeftForWall(mainDivPlane.vertices[1], mainDivPlane.vertices[0],
							divPlanes_[indDivPlane]->getPlain().vertices[0]);
		//find location of the second vertice of current plane (wall)
		int onLeftResult2 = onLeftForWall(mainDivPlane.vertices[1], mainDivPlane.vertices[0],
							divPlanes_[indDivPlane]->getPlain().vertices[1]);

		if((onLeftResult1 == 1 && onLeftResult2 == 0) ||//if one point lie on the left and other similar to divider plane
			(onLeftResult1 == 0 && onLeftResult2 == 1) ||//if one point lie on the left and other similar to divider plane
			(onLeftResult1 == 1 && onLeftResult2 == 1))//if both points on the left
		{
			leftWalls.push_back(divPlanes_[indDivPlane]);
			//cout << "wall " << div_walls_[i_div_Wall]->getId() << " on the left" << endl;
		}
		else if((onLeftResult1 == 2 && onLeftResult2 == 0) ||//if one point lie on the right and other similar to divider plane
			(onLeftResult1 == 0 && onLeftResult2 == 2) ||//if one point lie on the right and other similar to divider plane
			(onLeftResult1 == 2 && onLeftResult2 == 2))//if both points on the right
		{
			rightWalls.push_back(divPlanes_[indDivPlane]);
			//cout << "wall " << div_walls_[i_div_Wall]->getId() << " on the right" << endl;
		}
		else//if both points are lie on the divider plane
		{
			middleWalls.push_back(divPlanes_[indDivPlane]);
			//cout << "wall " << div_walls_[i_div_Wall]->getId() << " in the middle" << endl;
		}
	}

	//cout << endl << "Left:" << leftWalls.size() << endl;
	//cout << "Right:" << rightWalls.size() << endl;
	//cout << "Middle:" << middleWalls.size() << endl;

	return_leftPlanes = leftWalls;
	return_rightPlanes = rightWalls;
	return_midPlanes = middleWalls;
}

tWall* CBinaryTree::findDivPlane(vector<tWall*>& walls_)
{
	//Find center of the model-----------------------
	Vector3d centerModel;

	for(int indWall = 0; indWall < walls_.size(); indWall++)
	{
		for(int intVertice = 0; intVertice < 4; intVertice++)
		{
			centerModel.x += walls_[indWall]->getPlain().vertices[intVertice].x;
			centerModel.y += walls_[indWall]->getPlain().vertices[intVertice].y;
			centerModel.z += walls_[indWall]->getPlain().vertices[intVertice].z;
		}
	}

	centerModel.x /= walls_.size();
	centerModel.y /= walls_.size();
	centerModel.z /= walls_.size();
	//--------------------------------------------------

	//Choose planes for separations randomly------------
	const int NUM_TAKEN_IND_FOR_SEP_WALLS = 3;//how many planes (walls) will be choosen to find best one
	int randIndexWall[NUM_TAKEN_IND_FOR_SEP_WALLS] = {0};//contain indexes of the choosen planes (walls)

	for(int indWall = 0; indWall < NUM_TAKEN_IND_FOR_SEP_WALLS; indWall++)
	{
		randIndexWall[indWall] = rand() % walls_.size();

		int indLastPlane = indWall - 1;
		while(indWall != 0 && indLastPlane >= 0)
		{
			while(randIndexWall[indWall] == randIndexWall[indLastPlane])
			{
				randIndexWall[indWall] = rand() % walls_.size();
			}

			indLastPlane--;
		}
	}
	//--------------------------------------------------

	//Find centers of the walls-------------------------
	Vector3d choosenWallsCenters[NUM_TAKEN_IND_FOR_SEP_WALLS];//centers of the randomly choosen planes (walls)

	for(int indChoosenSepWall = 0; indChoosenSepWall < NUM_TAKEN_IND_FOR_SEP_WALLS; indChoosenSepWall++)
	{
		int indexWall = randIndexWall[indChoosenSepWall];
		tPlane planeWall(walls_[indexWall]->getPlain());

		Vector3d centerWall((planeWall.vertices[0].x + planeWall.vertices[2].x) / 2,
						(planeWall.vertices[0].y + planeWall.vertices[2].y) / 2,
						(planeWall.vertices[0].z + planeWall.vertices[2].z) / 2);

		choosenWallsCenters[indChoosenSepWall] = centerWall;
	}
	//--------------------------------------------------

	//Find distances between choosen wall's centers and scene (input model) center
	float distsPlaneCentToModCent[NUM_TAKEN_IND_FOR_SEP_WALLS];//distances from the planes centers to the model (scene) center
	for(int indCenterPlane = 0; indCenterPlane < NUM_TAKEN_IND_FOR_SEP_WALLS; indCenterPlane++)
	{
		float xd = fabs(choosenWallsCenters[indCenterPlane].x - centerModel.x);
		float yd = fabs(choosenWallsCenters[indCenterPlane].y - centerModel.y);
		float zd = fabs(choosenWallsCenters[indCenterPlane].z - centerModel.z);

		distsPlaneCentToModCent[indCenterPlane] = (xd * xd + yd * yd + zd * zd);
	}
	//--------------------------------------------------

	//Go throw all (except first one) distnaces and choose smallest--------
	int indBestDist = 0;

	for(int indDist = 1; indDist < NUM_TAKEN_IND_FOR_SEP_WALLS; indDist++)
	{
		if(distsPlaneCentToModCent[indDist] < distsPlaneCentToModCent[indBestDist])
		{
			indBestDist = indDist;
		}
	}
	//--------------------------------------------------

	int indexSeparatingWall = randIndexWall[indBestDist];
	return walls_[indexSeparatingWall];
}

void CBinaryTree::processNodes(CNode& node, vector<tWall*>& divPlanes, vector<tWall*>& dataPlanes)
{
	if(divPlanes.size() == 1)//if there is only one div plane
	{
		node.addDivPoly(divPlanes[0]);
		node.id = this->numNodes;

		if(dataPlanes.size() > 0)//if there is some data planes
		{
			vector<tWall*> dataLeftPlanesSet;
			vector<tWall*> dataRightPlanesSet;
			vector<tWall*> dataMidPlanesSet;

			//find dataLeftPlanesSet, dataRightPlanesSet, dataMidPlanesSet according to divPlanes[0] from dataPlanes
			findMidLeftRightSets(dataPlanes, *divPlanes[0], dataLeftPlanesSet, dataRightPlanesSet, dataMidPlanesSet);

			//left child----------------------------leaf node with more than one data plane
			if(dataLeftPlanesSet.size() > 0) 
			{
				CNode* leftNode = new CNode();
				leftNode->type = LEAF_NODE;
				this->numNodes++;
				leftNode->id = this->numNodes;
				node.left = leftNode;

				if(dataMidPlanesSet.size() > 0)
				{
					//add middle data planes set to the left data planes set
					for(int indMiddleDataPoly = 0; indMiddleDataPoly < dataMidPlanesSet.size(); indMiddleDataPoly++)
					{
						dataLeftPlanesSet.push_back(dataMidPlanesSet[indMiddleDataPoly]);
					}
				}

				leftNode->addDataPolys(dataLeftPlanesSet);

				//cout << "numDataPolys = " << leftNode->getDataPolys()->size() << endl;
			}
			else//left child----------------------------leaf node with one data plane
			{
				CNode* leftNode = new CNode();
				leftNode->type = LEAF_NODE;
				this->numNodes++;
				leftNode->id = this->numNodes;
				node.left = leftNode;
				//add divider poly
				leftNode->addDataPolys(divPlanes);

				//cout << "numDataPolys = " << leftNode->getDataPolys()->size() << endl;
			}
			//------------------------------------------

			//right child-------------------------------leaf node with more than one data plane
			if(dataRightPlanesSet.size() > 0)
			{
				CNode* rightNode = new CNode();
				rightNode->type = LEAF_NODE;
				this->numNodes++;
				rightNode->id = this->numNodes;
				node.right = rightNode;

				if(dataMidPlanesSet.size() > 0)
				{
					//add middle data planes set to the right data planes set
					for(int indMiddleDataPoly = 0; indMiddleDataPoly < dataMidPlanesSet.size(); indMiddleDataPoly++)
					{
						dataRightPlanesSet.push_back(dataMidPlanesSet[indMiddleDataPoly]);
					}
				}

				rightNode->addDataPolys(dataRightPlanesSet);

				cout << "numDataPolys = " << rightNode->getDataPolys()->size() << endl;
			}
			else//right child-------------------------------leaf node with one data plane
			{
				CNode* rightNode = new CNode();
				rightNode->type = LEAF_NODE;
				this->numNodes++;
				rightNode->id = this->numNodes;
				node.right = rightNode;
				//add divider poly
				rightNode->addDataPolys(divPlanes);

				//cout << "numDataPolys = " << rightNode->getDataPolys()->size() << endl;
			}
			//------------------------------------------
		}
		else
		{
			MessageBox(NULL,"Error in function processNodes!","ERROR",MB_OK|MB_ICONSTOP);
		}

		return;
	}
	else//if there is more than one div plane
	{
		tWall* mainDivPlane = findDivPlane(divPlanes);//find best div plane
		node.addDivPoly(mainDivPlane);
		node.id = this->numNodes;

		vector<tWall*> copyWalls(divPlanes);

		//remove choosen divider plane from the divPlanes
		for(int indWallPlane = 0; indWallPlane < copyWalls.size(); indWallPlane++)
		{
			if(mainDivPlane->getId() == copyWalls[indWallPlane]->getId())
			{
				copyWalls.erase(copyWalls.begin() + indWallPlane);
			}
		}

		vector<tWall*> copyDataPolys(dataPlanes);//copy of the data polygons vector in the parameter

		//for div polygons
		vector<tWall*> leftPlanesSet;//div set
		vector<tWall*> rightPlanesSet;//div set
		vector<tWall*> midPlanesSet;//div set
		//for data polygins
		vector<tWall*> dataLeftPlanesSet;//data set
		vector<tWall*> dataRightPlanesSet;//data set
		vector<tWall*> dataMidPlanesSet;//data set

		findMidLeftRightSets(copyWalls, *mainDivPlane, leftPlanesSet, rightPlanesSet, midPlanesSet);
		findMidLeftRightSets(copyDataPolys, *mainDivPlane, dataLeftPlanesSet, dataRightPlanesSet, dataMidPlanesSet);

		//pass mid planes (planes which collinear with the main divider plane (mainDivPlane)) to the node
		for(int indMidPlane = 0; indMidPlane < midPlanesSet.size(); indMidPlane++)
		{
			//add current plane to the collection with dividers
			node.addDivPoly(midPlanesSet[indMidPlane]);
		}

		//for leftPlanesSet and rightPlanesSet we creating nodes and link it to the input node (node in the parameters)

		if(leftPlanesSet.size() > 0)
		{
			/*for(int indLeftPlane = 0; indLeftPlane < leftPlanesSet.size(); indLeftPlane++)
			{
				cout << "left div plane = " << leftPlanesSet[indLeftPlane]->getId() << endl;
			}*/

			CNode* leftNode = new CNode();
			leftNode->type = NORMAL_NODE;
			this->numNodes++;
			leftNode->id = this->numNodes;
			//cout << numNodes << endl;
			node.left = leftNode;

			//add middle data planes set to the left data planes set
			for(int indPoly = 0; indPoly < dataMidPlanesSet.size(); indPoly++)
			{
				dataLeftPlanesSet.push_back(dataMidPlanesSet[indPoly]);
			}

			processNodes(*leftNode, leftPlanesSet, dataLeftPlanesSet);
		}
		else//when left child is leaf node
		{
			//create node and it will have only dataPolygons

			CNode* leftNode = new CNode();
			leftNode->type = LEAF_NODE;
			this->numNodes++;
			leftNode->id = this->numNodes;
			node.left = leftNode;

			//add middle data planes set to the left data planes set
			for(int indPoly = 0; indPoly < dataMidPlanesSet.size(); indPoly++)
			{
				dataLeftPlanesSet.push_back(dataMidPlanesSet[indPoly]);
			}

			leftNode->addDataPolys(dataLeftPlanesSet);
		}

		if(rightPlanesSet.size() > 0)
		{
			/*for(int indRightPlane = 0; indRightPlane < rightPlanesSet.size(); indRightPlane++)
			{
				cout << "right div plane = " << rightPlanesSet[indRightPlane]->getId() << endl;
			}*/

			CNode* rightNode = new CNode();
			rightNode->type = NORMAL_NODE;
			this->numNodes++;
			rightNode->id = this->numNodes;
			//cout << numNodes << endl;
			node.right = rightNode;

			//add middle data planes set to the left data planes set
			for(int indPoly = 0; indPoly < dataMidPlanesSet.size(); indPoly++)
			{
				dataRightPlanesSet.push_back(dataMidPlanesSet[indPoly]);
			}

			processNodes(*rightNode, rightPlanesSet, dataRightPlanesSet);
		}
		else//when right child is leaf node
		{
			//create node and it will have only dataPolygons

			CNode* rightNode = new CNode();
			rightNode->type = LEAF_NODE;
			this->numNodes++;
			rightNode->id = this->numNodes;
			node.right = rightNode;

			//add middle data planes set to the left data planes set
			for(int indPoly = 0; indPoly < dataMidPlanesSet.size(); indPoly++)
			{
				dataRightPlanesSet.push_back(dataMidPlanesSet[indPoly]);
			}

			rightNode->addDataPolys(dataRightPlanesSet);
		}
	}
}

void CBinaryTree::createBinaryTree(vector<tWall*>& walls_)
{
	if(walls_.size() > 0)
	{
		//Init root node
		this->root = new CNode();
		this->root->type = NORMAL_NODE;
		this->numNodes++;

		vector<tWall*> dataWalls;
		dataWalls = walls_;

		processNodes(*root, walls_, dataWalls);//create all other nodes
	}
}

void CNode::addObjects(vector<Object3D*>* objects_)
{
	for(int indObject = 0; indObject < objects_->size(); indObject++)
	{
		m_vObjects.push_back((*objects_)[indObject]);
	}
}

void CNode::addObject(Object3D* object_)
{
	m_vObjects.push_back(object_);
}

vector<Object3D*>* CNode::getObjects()
{
	return &m_vObjects;
}

Object3D* CNode::getObject(int index)
{
	return m_vObjects[index];
}