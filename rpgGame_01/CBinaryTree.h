#pragma once
#include "GameEngine.h"
#include "Object3D.h"

#define NUM_VERT_IN_PORTAL 4
#define NUM_VERT_IN_PLANE 4

class tPlane
{
private:
public:
	//		VARIABLES

	Vector3d vertices[NUM_VERT_IN_PLANE];
	Vector3d normal;

	//		FUNCTIONS

	tPlane()
	{
		for(int i = 0; i < NUM_VERT_IN_PLANE; i++)
		{
			vertices[i] = Vector3d(0,0,0);
		}

		normal = Vector3d(0,0,0);
	}

	//copy constryctor
	tPlane(const tPlane& plane_)
	{
		for(int i = 0; i < NUM_VERT_IN_PLANE; i++)
		{
			vertices[i] = Vector3d(plane_.vertices[i]);
		}

		normal = Vector3d(plane_.normal);
	}

	Vector3d getNormal()
	{
		return normal;
	}

	Vector3d* getVertices()
	{
		return vertices;
	}

	void setNormal(Vector3d normal_)
	{
		normal = normal_;
	}

	void setVertices(Vector3d* vertices_)
	{
		for(int indVertice = 0; indVertice < NUM_VERT_IN_PLANE; indVertice++)
		{
			vertices[indVertice] = Vector3d(vertices_[indVertice]);
		}
	}
};

class tPortal
{
public:
	tPlane plain;
	int idPortal;

	tPortal()
	{
		idPortal = -1;
	}

	//copy constryctor
	tPortal(const tPortal& portal_)
	{
		plain = portal_.plain;
		idPortal = portal_.idPortal;
	}
};

//if type = 1 // common wall
//if type = 2 // wall with portals
class tWall
{
private:
	int id;
	int type;
	tPlane plain;
public:

	tWall()
	{
		id = -1;
		type = 1;
	}

	virtual ~tWall()
	{
	}

	//copy constryctor
	tWall(const tWall& wall_)
	{
		this->id = wall_.id;
		this->type = wall_.type;
		this->plain = wall_.plain;
	}

	void setId(int id_)
	{
		id = id_;
	}
	void setType(int type_)
	{
		type = type_;
	}
	int getId()
	{
		return id;
	}
	int getType()
	{
		return type;
	}
	void setPlain(tPlane plain_)
	{
		plain = plain_;
	}
	tPlane getPlain()
	{
		return plain;
	}

	virtual vector<tPortal*>* getPortals()
	{
		return NULL;
	}

	virtual void addPortal(tPortal* portal_)
	{
	}

	virtual int getNumPortals()
	{
		return 0;
	}
};

class tPortalWall: public tWall
{
private:
	vector<tPortal*> m_portals;
public:
	tPortalWall() : tWall()
	{
	}

	//copy constryctor
	tPortalWall(const tPortalWall& portalWall_) : tWall(portalWall_)
	{
		m_portals = portalWall_.m_portals;
	}

	~tPortalWall() 
	{
	}

	vector<tPortal*>* getPortals()
	{
		return &m_portals;
	}

	void addPortal(tPortal* portal_)
	{
		tPortal* copyPortal = new tPortal(*portal_);

		m_portals.push_back(copyPortal);
	}

	int getNumPortals()
	{
		return m_portals.size();
	}
};

//node type
enum e_nodeType {NORMAL_NODE, LEAF_NODE};

class CNode
{
private:
	vector<tWall*> m_vDivPolygons;//divider polygons
	vector<tWall*> m_vDataPolygons;//bounding polygons
	vector<Object3D*> m_vObjects;
public:
	CNode* left;//back
	CNode* right;//front
	int id;
	e_nodeType type;

	CNode();
	~CNode();

	//add one div polygon
	void addDivPoly(tWall* divPoly);
	//add one div polygon
	void addDivPoly(tWall& divPoly);
	//get div poly with particular index
	tWall* getDivPoly(int index);
	//return all divider polygons
	vector<tWall*>* getDivPolys();

	//add one data polygon
	void addDataPoly(tWall* dataPoly_);
	//add one data polygon
	void addDataPoly(tWall& dataPoly_);
	//add more than one data polygon
	void addDataPolys(vector<tWall*>& dataPolys_);
	//get vector (array) of data polygons
	vector<tWall*>* getDataPolys();

	void addObjects(vector<Object3D*>* objects_);
	void addObject(Object3D* object_);
	vector<Object3D*>* getObjects();
	Object3D* getObject(int index);
};

class CBinaryTree
{
private:
	void createBinaryTree(vector<tWall*>& walls_);
	void createBinaryTree(vector<tPlane>& planes_);
	tWall* findDivPlane(vector<tWall*>& walls_);
	void processNodes(CNode& node, vector<tWall*>& divPlanes, vector<tWall*>& dataPlanes);//used in the createBinaryTree function
	void findMidLeftRightSets(vector<tWall*>& divPlanes_, tWall& mainDivPlane_,
							vector<tWall*>& return_leftPlanes, vector<tWall*>& return_rightPlanes,
							vector<tWall*>& return_midPlanes);
public:
	int numNodes;
	CNode* root;

	CBinaryTree();
	CBinaryTree(vector<tWall*>& walls_);
	CBinaryTree(vector<tPlane>& planes_);
	~CBinaryTree();
};