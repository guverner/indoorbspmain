#include "CCar.h"

CCar::CCar(char* file_, float posX, float posY, float posZ)
{
	load3DModel(file_, m_model);

	m_speed = 0.0f;
	m_spin = 0.0f;

	m_dirVect[0]=0;//x		
	m_dirVect[1]=0;//y		
	m_dirVect[2]=-1;//z		
	m_dirVect[3]=0;//w

	m_center[0] = m_model.theBBox.centrePoint.x + posX;
	m_center[1] = m_model.theBBox.centrePoint.y + posY;
	m_center[2] = m_model.theBBox.centrePoint.z + posZ;
	m_center[3] = 1;

	//initialise vertices array--------------------------
	//	3----2
	//	|	 |
	//	0----1

	//Bottom plane of the car
	//0
	vertices[0].x = (m_center.x - m_model.theBBox.boxWidthX/2);
	vertices[0].y = (m_center.y - m_model.theBBox.boxWidthY/2);
	vertices[0].z = (m_center.z + m_model.theBBox.boxWidthZ/2);
	vertices[0].w = 1;
	//1
	vertices[1].x = (m_center.x + m_model.theBBox.boxWidthX/2);
	vertices[1].y = (m_center.y - m_model.theBBox.boxWidthY/2);
	vertices[1].z = (m_center.z + m_model.theBBox.boxWidthZ/2);
	vertices[1].w = 1;
	//2
	vertices[2].x = (m_center.x + m_model.theBBox.boxWidthX/2);
	vertices[2].y = (m_center.y - m_model.theBBox.boxWidthY/2);
	vertices[2].z = (m_center.z - m_model.theBBox.boxWidthZ/2);
	vertices[2].w = 1;
	//3
	vertices[3].x = m_center.x - m_model.theBBox.boxWidthX/2;
	vertices[3].y = m_center.y - m_model.theBBox.boxWidthY/2;
	vertices[3].z = m_center.z - m_model.theBBox.boxWidthZ/2;
	vertices[3].w = 1;
	//Top plane of car
	//4
	vertices[4].x = (m_center.x - m_model.theBBox.boxWidthX/2);
	vertices[4].y = (m_center.y + m_model.theBBox.boxWidthY/2);
	vertices[4].z = (m_center.z + m_model.theBBox.boxWidthZ/2);
	vertices[4].w = 1;
	//5
	vertices[5].x = (m_center.x + m_model.theBBox.boxWidthX/2);
	vertices[5].y = (m_center.y + m_model.theBBox.boxWidthY/2);
	vertices[5].z = (m_center.z + m_model.theBBox.boxWidthZ/2);
	vertices[5].w = 1;
	//6
	vertices[6].x = (m_center.x + m_model.theBBox.boxWidthX/2);
	vertices[6].y = (m_center.y + m_model.theBBox.boxWidthY/2);
	vertices[6].z = (m_center.z - m_model.theBBox.boxWidthZ/2);
	vertices[6].w = 1;
	//7
	vertices[7].x = m_center.x - m_model.theBBox.boxWidthX/2;
	vertices[7].y = m_center.y + m_model.theBBox.boxWidthY/2;
	vertices[7].z = m_center.z - m_model.theBBox.boxWidthZ/2;
	vertices[7].w = 1;
	//---------------------------------------------------
}

void CCar::draw(TypeDraw type)
{
	switch(type)
	{
	case NORMAL:
		m_model.speedDisplayFaceNormals();
		break;
	case BOUNDBOX:
		m_model.drawBoundingBox();
		break;
	case OCTREE:
		m_model.drawOctreeLeaves();
		break;
	case ALL:
		m_model.speedDisplayFaceNormals();
		m_model.drawBoundingBox();
		m_model.drawOctreeLeaves();
		break;
	}
}

float CCar::getSpeed()
{
	return m_speed;
}

void CCar::setSpeed(float speed_)
{
	m_speed = speed_;
}

void CCar::setSpin(float spin_)
{
	if(spin_ < 0)
	{
		if((m_spin + spin_) < 0)
		{
			m_spin = 360 + (m_spin + spin_);
		}
		else
		{
			m_spin += spin_;
		}
	}
	else
	{
		if(abs(m_spin + spin_) > 360)
		{
			m_spin = (m_spin + spin_) - 360;
		}
		else
		{
			m_spin += spin_;
		}
	}
}

Vector4D* CCar::getDirVect()
{
	return &m_dirVect;
}

void CCar::setDirVect(float value_)
{
	float multMatrix[16] = {cos(value_*DegToRad),	0, -sin(value_*DegToRad),	0,
							0,						1,	0,						0,
							sin(value_*DegToRad),	0,	cos(value_*DegToRad),	0,
							0,						0,	0,						1};

	float x2;
	float y2;
	float z2;
	float w2;

	x2 = multMatrix[0] * m_dirVect[0] + multMatrix[4] * m_dirVect[1] + multMatrix[8] * m_dirVect[2] + 
				multMatrix[12] * m_dirVect[3];
	y2 = multMatrix[1] * m_dirVect[0] + multMatrix[5] * m_dirVect[1] + multMatrix[9] * m_dirVect[2] + 
				multMatrix[13] * m_dirVect[3];
	z2 = multMatrix[2] * m_dirVect[0] + multMatrix[6] * m_dirVect[1] + multMatrix[10] * m_dirVect[2] + 
				multMatrix[14] * m_dirVect[3];
	w2 = multMatrix[3] * m_dirVect[0] + multMatrix[7] * m_dirVect[1] + multMatrix[11] * m_dirVect[2] + 
				multMatrix[15] * m_dirVect[3];

	m_dirVect.x = x2;
	m_dirVect.y = y2;
	m_dirVect.z = z2;
	m_dirVect.w = w2;
	m_dirVect.normalize();
}

void CCar::updateVertices()
{
	//if we will look from 3rd person camera, then

	//	3----2
	//	|	 |
	//	0----1

	//Bottom plan eof the 3d Object
	vertices[0].x = m_transfMatrix[0] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[4] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[8] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[12];
	vertices[0].y = m_transfMatrix[1] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[5] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[9] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[13];
	vertices[0].z = m_transfMatrix[2] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[6] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[10] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[14];

	vertices[1].x = m_transfMatrix[0] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[4] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[8] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[12];
	vertices[1].y = m_transfMatrix[1] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[5] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[9] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[13];
	vertices[1].z = m_transfMatrix[2] * this->m_model.theBBox.boxWidthX/2 + 
					m_transfMatrix[6] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[10] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[14];
	
	vertices[2].x = m_transfMatrix[0] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[4] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[8] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[12];
	vertices[2].y = m_transfMatrix[1] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[5] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[9] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[13];
	vertices[2].z = m_transfMatrix[2] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[6] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[10] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[14];
	
	vertices[3].x = m_transfMatrix[0] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[4] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[8] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[12];
	vertices[3].y = m_transfMatrix[1] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[5] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[9] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[13];
	vertices[3].z = m_transfMatrix[2] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[6] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[10] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[14];

	//Top plane of the 3d Object
	vertices[4].x = m_transfMatrix[0] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[4] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[8] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[12];
	vertices[4].y = m_transfMatrix[1] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[5] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[9] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[13];
	vertices[4].z = m_transfMatrix[2] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[6] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[10] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[14];

	vertices[5].x = m_transfMatrix[0] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[4] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[8] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[12];
	vertices[5].y = m_transfMatrix[1] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[5] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[9] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[13];
	vertices[5].z = m_transfMatrix[2] * this->m_model.theBBox.boxWidthX/2 + 
					m_transfMatrix[6] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[10] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[14];

	vertices[6].x = m_transfMatrix[0] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[4] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[8] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[12];
	vertices[6].y = m_transfMatrix[1] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[5] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[9] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[13];
	vertices[6].z = m_transfMatrix[2] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[6] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[10] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[14];

	vertices[7].x = m_transfMatrix[0] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[4] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[8] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[12];
	vertices[7].y = m_transfMatrix[1] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[5] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[9] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[13];
	vertices[7].z = m_transfMatrix[2] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[6] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[10] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[14];

	for(int i = 0; i < 8; i++)
	{
		if(vertices[i].isINF())
			cout << "Car inf " << i << endl;
	}
}

void CCar::collisionCar3dObj(Object3D* obj_)
{
	if(collisionPointPoly(this->vertices[3], obj_->vertices) || collisionPointPoly(this->vertices[2], obj_->vertices))
	{
		//cout << "INSIDE" << endl;
		this->setSpeed(-0.8);
	}
	else
	{
		//cout << "OUTSIDE" << endl;
	}
	if(collisionPointPoly(this->vertices[0], obj_->vertices) || collisionPointPoly(this->vertices[1], obj_->vertices))
	{
		//cout << "INSIDE" << endl;
		this->setSpeed(0.8);
	}
	else
	{
		//cout << "OUTSIDE" << endl;
	}
}

void CCar::collisionCarBoundMap(Vector4D* vertArray_, font_data& font_)
{
	static bool collideFirstTime = false;//variable for collision of car and boundaries of the map only once. If its true, collision was happened.
	static int iFrame = 0;//variable to keep text warning for longer time

	if(!collisionPointPoly(this->vertices[3], vertArray_) && !collisionPointPoly(this->vertices[2], vertArray_))//check car's forward
		//if car is outside of the map
	{
		if(collideFirstTime == false)//if there was not collision before
		{
			iFrame = 60;//warning lable will be on the screen this ammount of the frames

			//cout << "spinP " << car->m_spin << endl;
			//cout << "speed " << car->getSpeed() << endl;
			//cout << "OUTSIDE" << endl;

			this->setSpeed(5.0);//to avoid errors (shaking of hte car)

			//Rotate car by 180 degree---
			this->m_dirVect = -this->m_dirVect;
			this->setSpin(180);
			//---------------------------

			collideFirstTime = true;//so there was first collision of the car and map's boundaries
		}
	}
	else if(!collisionPointPoly(this->vertices[0], vertArray_) && !collisionPointPoly(this->vertices[1], vertArray_))//check car's back
		//if car is outside of the map
	{
		if(collideFirstTime == false)
		{
			iFrame = 60;

			//cout << "spinZ " << car->m_spin << endl;
			//cout << "speed " << car->getSpeed() << endl;
			//cout << "OUTSIDE" << endl;

			this->setSpeed(-5.0);

			this->m_dirVect = -this->m_dirVect;
			this->setSpin(-180);

			collideFirstTime = true;
		}
	}
	else if(collisionPointPoly(this->vertices[3], vertArray_) && collisionPointPoly(this->vertices[2], vertArray_) &&
			collisionPointPoly(this->vertices[0], vertArray_) && collisionPointPoly(this->vertices[1], vertArray_))
			//if car is inside the map
	{
		collideFirstTime = false;//to make able car collise with map's boundaried again

		if(iFrame > 0)
		{
			iFrame--;//decrease frames 
		}
	}

	static float iTransparent = 1;//variable to change transparency of the text
	if(iFrame)//if text is still active after after collision of the car and map boundaries
	{
		glLoadIdentity();
		glColor4f(1, 0, 0, iTransparent);
		print(font_, screenWidth/2 - 240, screenHeight/2, "Out of the map!");

		if(iTransparent > 0)//to keep variable positive
		{
			iTransparent = iTransparent - 0.02;//change transparency of hte text
		}
	}
	else
	{
		iTransparent = 1;//assign text transparency to deffault 1
	}
}

void CCar::drawSpeed(font_data& font_)
{
	glLoadIdentity();
	glColor3f(0,0,0);
	//change integer to string--------
	stringstream ss;//create a stringstream
	const float MULT_SPEED = 30;
	ss << static_cast<int>(abs(this->getSpeed() * MULT_SPEED));//add number to the stream
	//string a =  ss.str();//return a string with the contents of the stream
	//--------------------------------
	print(font_, 20, screenHeight - 70, ss.str().c_str());
	//------------------------------------------
}

double** CCar::calculateNewRotatAxis(BYTE* heighMap_)
{
	float static oldAnglesAr[4] = {0, 0, 0, 0};//old angles between car and terrain vectors

	//----------------------------------------------------------------------
		float heightMap4pAr[4];//height of map in 4 bottom point of the car
		Vector4D heightMap4ArVect[4];//vectors array of height map 4 points
		Vector4D unitVectCar[4];
		Vector4D unitVectTer[4];

		this->m_center.y = Height(heighMap_, this->m_center.x, this->m_center.z) + this->m_model.theBBox.boxWidthY/2;
		
		//Car Vertices (top view)
		//	      2
		//  	-----
		//    3	|	| 1
		//	    -----
		//	      0
		unitVectCar[0] = this->vertices[1] - this->vertices[0];
		unitVectCar[0].y = 0;
		unitVectCar[0] = unitVectCar[0].normalize();
		unitVectCar[1] = this->vertices[2] - this->vertices[1];
		unitVectCar[1].y = 0;
		unitVectCar[1] = unitVectCar[1].normalize();
		unitVectCar[2] = this->vertices[3] - this->vertices[2];//changed
		unitVectCar[2].y = 0;
		unitVectCar[2] = unitVectCar[2].normalize();
		unitVectCar[3] = this->vertices[0] - this->vertices[3];//changed
		unitVectCar[3].y = 0;
		unitVectCar[3] = unitVectCar[3].normalize();

		//4 vertices of heigh map---
		heightMap4pAr[0] = Height(heighMap_, this->vertices[0].x, this->vertices[0].z);
		heightMap4ArVect[0].x = this->vertices[0].x;	heightMap4ArVect[0].z = this->vertices[0].z;
		heightMap4ArVect[0].y = heightMap4pAr[0];		heightMap4ArVect[0].w = this->vertices[0].w;

		heightMap4pAr[1] = Height(heighMap_, this->vertices[1].x, this->vertices[1].z);
		heightMap4ArVect[1].x = this->vertices[1].x;	heightMap4ArVect[1].z = this->vertices[1].z;
		heightMap4ArVect[1].y = heightMap4pAr[1];		heightMap4ArVect[1].w = this->vertices[1].w;

		heightMap4pAr[2] = Height(heighMap_, this->vertices[2].x, this->vertices[2].z);
		heightMap4ArVect[2].x = this->vertices[2].x;	heightMap4ArVect[2].z = this->vertices[2].z;
		heightMap4ArVect[2].y = heightMap4pAr[2];		heightMap4ArVect[2].w = this->vertices[2].w;

		heightMap4pAr[3] = Height(heighMap_, this->vertices[3].x, this->vertices[3].z);
		heightMap4ArVect[3].x = this->vertices[3].x;	heightMap4ArVect[3].z = this->vertices[3].z;
		heightMap4ArVect[3].y = heightMap4pAr[3];		heightMap4ArVect[3].w = this->vertices[3].w;
		//---------------------------

		//	      2
		//  	-----
		//    3	|	| 1
		//	    -----
		//	      0	
		unitVectTer[0] = heightMap4ArVect[1] - heightMap4ArVect[0];
		unitVectTer[0] = unitVectTer[0].normalize();
		unitVectTer[1] = heightMap4ArVect[2] - heightMap4ArVect[1];
		unitVectTer[1] = unitVectTer[1].normalize();
		unitVectTer[2] = heightMap4ArVect[3] - heightMap4ArVect[2];
		unitVectTer[2] = unitVectTer[2].normalize();
		unitVectTer[3] = heightMap4ArVect[0] - heightMap4ArVect[3];
		unitVectTer[3] = unitVectTer[3].normalize();

		//Normal of the car's bottom plane (up)
		Vector4D normalVertical((unitVectCar[2].y * unitVectCar[3].z) - (unitVectCar[2].z * unitVectCar[3].y),
								(unitVectCar[2].z * unitVectCar[3].x) - (unitVectCar[2].x * unitVectCar[3].z),
								(unitVectCar[2].x * unitVectCar[3].y) - (unitVectCar[2].y * unitVectCar[3].x),
								1);

		normalVertical = normalVertical.normalize();

		//Normal of the car's back plane
		Vector4D normalBackFace((unitVectCar[0].z * -normalVertical.y) - (unitVectCar[0].y * -normalVertical.z),
								(unitVectCar[0].x * -normalVertical.z) - (unitVectCar[0].z * -normalVertical.x),
								(unitVectCar[0].y * -normalVertical.x) - (unitVectCar[0].x * -normalVertical.y),
								1);

		normalBackFace = normalBackFace.normalize();

		//Normal of the car's front plane
		Vector4D normalFrontFace((unitVectCar[2].z * -normalVertical.y) - (unitVectCar[2].y * -normalVertical.z),
								(unitVectCar[2].x * -normalVertical.z) - (unitVectCar[2].z * -normalVertical.x),
								(unitVectCar[2].y * -normalVertical.x) - (unitVectCar[2].x * -normalVertical.y),
								1);

		normalFrontFace = normalFrontFace.normalize();

		//Assign points of th eterrain to the front and back face of the car
			//q' = q + (d - q . n) * n------------------------------
			Vector4D newTerPointsArray[4];

			//back face
			for(int i = 0; i < 2; i++)
			{
				float d = normalBackFace.x * this->vertices[0].x + normalBackFace.y * this->vertices[0].y + normalBackFace.z * this->vertices[0].z;
				float dotQN = heightMap4ArVect[i].x * normalBackFace.x + heightMap4ArVect[i].y * normalBackFace.y + heightMap4ArVect[i].z * normalBackFace.z;
				Vector4D tempMult(normalBackFace * (d - dotQN));
				Vector4D newTerPoint(heightMap4ArVect[i] + tempMult);
				newTerPointsArray[i] = newTerPoint;
			}

			//front face
			for(int i = 2; i < 4; i++)
			{
				float d = normalFrontFace.x * this->vertices[2].x + normalFrontFace.y * this->vertices[2].y + normalFrontFace.z * this->vertices[2].z;
				float dotQN = heightMap4ArVect[i].x * normalFrontFace.x + heightMap4ArVect[i].y * normalFrontFace.y + heightMap4ArVect[i].z * normalFrontFace.z;
				Vector4D tempMult(normalFrontFace * (d - dotQN));
				Vector4D newTerPoint(heightMap4ArVect[i] + tempMult);
				newTerPointsArray[i] = newTerPoint;
			}
			//------------------------------------------------------

			unitVectTer[0] = newTerPointsArray[1] - newTerPointsArray[0];
			unitVectTer[0] = unitVectTer[0].normalize();
			unitVectTer[1] = newTerPointsArray[2] - newTerPointsArray[1];
			unitVectTer[1] = unitVectTer[1].normalize();
			unitVectTer[2] = newTerPointsArray[3] - newTerPointsArray[2];//changed
			unitVectTer[2] = unitVectTer[2].normalize();
			unitVectTer[3] = newTerPointsArray[0] - newTerPointsArray[3];//changed
			unitVectTer[3] = unitVectTer[3].normalize();

			this->m_dirVect.y = -((unitVectTer[2].y - unitVectTer[1].y) + (unitVectTer[3].y - unitVectTer[0].y))/2;
		//---------------------------------------------------------------------

		float anglesAr[4];//new angles between car and terrain vectors

		for(int i = 0; i < 4; i++)
		{
			float dp = dot(unitVectCar[i], unitVectTer[i]);
			if(dp > 1.0f)
				dp = 1.0f;
			if(dp < -1.0f)
				dp = -1.0f;
			anglesAr[i] = acos(dp);
		}

		//Cross Products-----------------
		Vector4D newRotatVectorHor1((unitVectTer[0].y * unitVectCar[0].z) - (unitVectTer[0].z * unitVectCar[0].y),
								(unitVectTer[0].z * unitVectCar[0].x) - (unitVectTer[0].x * unitVectCar[0].z),
								(unitVectTer[0].x * unitVectCar[0].y) - (unitVectTer[0].y * unitVectCar[0].x),
								1);

		Vector4D newRotatVectorHor2((unitVectTer[2].y * unitVectCar[2].z) - (unitVectTer[2].z * unitVectCar[2].y),
								(unitVectTer[2].z * unitVectCar[2].x) - (unitVectTer[2].x * unitVectCar[2].z),
								(unitVectTer[2].x * unitVectCar[2].y) - (unitVectTer[2].y * unitVectCar[2].x),
								1);

		Vector4D newRotatVectorHor3((unitVectTer[1].y * unitVectCar[1].z) - (unitVectTer[1].z * unitVectCar[1].y),
								(unitVectTer[1].z * unitVectCar[1].x) - (unitVectTer[1].x * unitVectCar[1].z),
								(unitVectTer[1].x * unitVectCar[1].y) - (unitVectTer[1].y * unitVectCar[1].x),
								1);

		Vector4D newRotatVectorHor4((unitVectTer[3].y * unitVectCar[3].z) - (unitVectTer[3].z * unitVectCar[3].y),
								(unitVectTer[3].z * unitVectCar[3].x) - (unitVectTer[3].x * unitVectCar[3].z),
								(unitVectTer[3].x * unitVectCar[3].y) - (unitVectTer[3].y * unitVectCar[3].x),
								1);

		Vector4D newRotatVectRight((unitVectCar[1].y * normalVertical.z) - (unitVectCar[1].z * normalVertical.y),
									(unitVectCar[1].z * normalVertical.x) - (unitVectCar[1].x * normalVertical.z),
									(unitVectCar[1].x * normalVertical.y) - (unitVectCar[1].y * normalVertical.x),
									1);
		newRotatVectRight.normalize();
		//--------------------------------
		newRotatVectorHor1.normalize();
		newRotatVectorHor2.normalize();
		newRotatVectorHor3.normalize();
		newRotatVectorHor4.normalize();

		Quaternion q1;
		Quaternion q2;

		//Print out y values of heigh map
		for(int i = 0; i < 4; i++)
		{
			cout << i << " = " << heightMap4pAr[i] << endl;
		}

		//calculate difference between angles--------
		float diffAngles[4];

		for(int i = 0; i < 4; i++)
		{

			diffAngles[i] = fabs(anglesAr[i] - oldAnglesAr[i]);
		}
		//-------------------------------------------

		const float mult = 0.1f;

		//Rotation X
		if(fabs(oldAnglesAr[1]) > fabs(oldAnglesAr[3]))
		{
			if(heightMap4pAr[0] < heightMap4pAr[3])
			//if(Height(heighMap_, this->vertices[0].x, this->vertices[0].z) <
			//	Height(heighMap_, this->vertices[3].x, this->vertices[3].z))
			{
				//cout << "+++" << endl;

				/*q1.CreateFromAxisAngle(1, 0, 0, anglesAr[1] * RadToDeg);*/
				q1.CreateFromAxisAngle(1, 0, 0, (oldAnglesAr[1] + (diffAngles[1] * mult))*RadToDeg);

				cout << "X " << (oldAnglesAr[1] + (diffAngles[1] * mult))*RadToDeg << endl;
				cout << "X 1 +" << endl;
			}
			else //if (heightMap4pAr[0] > heightMap4pAr[3])
			{
				//cout << "---" << endl;

				/*q1.CreateFromAxisAngle(1, 0, 0, anglesAr[1] * -RadToDeg);*/
				q1.CreateFromAxisAngle(1, 0, 0, (oldAnglesAr[1] + (diffAngles[1] * mult))*-RadToDeg);

				cout << "X " << (oldAnglesAr[1] + (diffAngles[1] * mult))*-RadToDeg << endl;
				cout << "X 1 -" << endl;
			}
		}
		else
		{
			if(heightMap4pAr[0] < heightMap4pAr[3])
			//if(Height(heighMap_, this->vertices[0].x, this->vertices[0].z) <
			//	Height(heighMap_, this->vertices[3].x, this->vertices[3].z))
			{
				//cout << "+++" << endl;

				/*q1.CreateFromAxisAngle(1, 0, 0, anglesAr[3] * RadToDeg);*/
				q1.CreateFromAxisAngle(1, 0, 0, (oldAnglesAr[3] + (diffAngles[3] * mult))*RadToDeg);

				cout << "X " << (oldAnglesAr[3] + (diffAngles[3] * mult))*RadToDeg << endl;
				cout << "X 3 +" << endl;
			}
			else
			{
				//cout << "---" << endl;

				/*q1.CreateFromAxisAngle(1, 0, 0, anglesAr[3] * -RadToDeg);*/
				q1.CreateFromAxisAngle(1, 0, 0, (oldAnglesAr[3] + (diffAngles[3] * mult))*-RadToDeg);

				cout << "X " << (oldAnglesAr[3] + (diffAngles[3] * mult))*-RadToDeg << endl;
				cout << "X 3 -" << endl;
			}
		}

		//Rotation Z
		if(fabs(oldAnglesAr[0]) > fabs(oldAnglesAr[2]))
		{
			if(heightMap4pAr[0] < heightMap4pAr[1])
			//if(Height(heighMap_, this->vertices[0].x, this->vertices[0].z) <
			//	Height(heighMap_, this->vertices[1].x, this->vertices[1].z))
			{
				/*q2.CreateFromAxisAngle(0, 0, -1, anglesAr[0] * -RadToDeg);*/

				/*q2.CreateFromAxisAngle(0, 0, -1, (oldAnglesAr[0] + (diffAngles[0] * mult))*-RadToDeg);*/

				q2.CreateFromAxisAngle(0, 0, 1, (oldAnglesAr[0] + (diffAngles[0] * mult))*RadToDeg);

				cout << "Z " << (oldAnglesAr[0] + (diffAngles[0] * mult))*-RadToDeg << endl;
				cout << "Z 0 -" << endl;
			}
			else //if(heightMap4pAr[0] >= heightMap4pAr[1])

				//if(Height(heighMap_, this->vertices[0].x, this->vertices[0].z) >=
				//Height(heighMap_, this->vertices[1].x, this->vertices[1].z))
			{
				/*q2.CreateFromAxisAngle(0, 0,-1, anglesAr[0] * RadToDeg);*/

				//q2.CreateFromAxisAngle(0, 0, -1, (oldAnglesAr[0] + (diffAngles[0] * mult))*RadToDeg);

				q2.CreateFromAxisAngle(0, 0, 1, (oldAnglesAr[0] + (diffAngles[0] * mult))*-RadToDeg);

				cout << "Z " <<(oldAnglesAr[0] + (diffAngles[0] * mult))*RadToDeg << endl;
				cout << "Z 0 +" << endl;
			}
		}
		else
		{
			if(heightMap4pAr[0] < heightMap4pAr[1])
			//if(Height(heighMap_, this->vertices[0].x, this->vertices[0].z) <
			//	Height(heighMap_, this->vertices[1].x, this->vertices[1].z))
			{
				/*q2.CreateFromAxisAngle(0, 0, -1, anglesAr[2] * -RadToDeg);*/

				/*q2.CreateFromAxisAngle(0, 0, -1, (oldAnglesAr[2] + (diffAngles[2] * mult))*-RadToDeg);*/

				q2.CreateFromAxisAngle(0, 0, 1, (oldAnglesAr[2] + (diffAngles[2] * mult))*RadToDeg);

				cout << "Z " << (oldAnglesAr[2] + (diffAngles[2] * mult))*-RadToDeg << endl;
				cout << "Z 2 -" << endl;
			}
			else //if(heightMap4pAr[0] >= heightMap4pAr[1])

				//if(Height(heighMap_, this->vertices[0].x, this->vertices[0].z) >=
				//Height(heighMap_, this->vertices[1].x, this->vertices[1].z))
			{
				/*q2.CreateFromAxisAngle(0, 0, -1, anglesAr[2] * RadToDeg);*/

				/*q2.CreateFromAxisAngle(0, 0, -1, (oldAnglesAr[2] + (diffAngles[2] * mult))*RadToDeg);*/

				q2.CreateFromAxisAngle(0, 0, 1, (oldAnglesAr[2] + (diffAngles[2] * mult))*-RadToDeg);

				cout << "Z " << (oldAnglesAr[2] + (diffAngles[2] * mult))*RadToDeg << endl;
				cout << "Z 2 +" << endl;
			}
		}

		cout << endl;

		double* newRotatMatrixArray[2];

		double* newRotatMatrix1;
		newRotatMatrix1 = q1.getMatrix();

		double* newRotatMatrix2;
		newRotatMatrix2 = q2.getMatrix();

		newRotatMatrixArray[0] = newRotatMatrix1;
		newRotatMatrixArray[1] = newRotatMatrix2;
	//----------------------------------------------------------------------

		//assign new angles to old angles------------
		for(int i = 0; i < 4; i++)
		{
			if(fabs(diffAngles[i]) < 1)
				oldAnglesAr[i] = anglesAr[i];
		}
		//-------------------------------------------

		//for(int i = 0; i < 4; i++)
		//{
		//	cout << "angle" << i << "=" << oldAnglesAr[i] << endl;
		//}

		return newRotatMatrixArray;
}

//RETURN:
//-1 - no collision
//0 - back edge collided
//1 - forward edge collided
//2 - right
//3 - left
int CCar::collideCarCircle(Vector4D& point_, float radius_)
{
	if(isCollideLineSegCircle(point_, radius_, this->vertices[0], this->vertices[1]))
	{
		return 0;
		//cout << "collision" << endl;
	}

	if(isCollideLineSegCircle(point_, radius_, this->vertices[3], this->vertices[2]))
	{
		return 1;
		//cout << "collision" << endl;
	}

	if(isCollideLineSegCircle(point_, radius_, this->vertices[1], this->vertices[2]))
	{
		return 2;
		//cout << "collision" << endl;
	}

	if(isCollideLineSegCircle(point_, radius_, this->vertices[0], this->vertices[3]))
	{
		return 3;
		//cout << "collision" << endl;
	}

	return -1;
}

void CCar::collisionCarTree(Vector4D& point_, float radius_)
{
	const float DISPLACEMENT = 0.6;

	if(collideCarCircle(point_, radius_) == 0)
	{
		this->setSpeed(DISPLACEMENT);
	}
	else if(collideCarCircle(point_, radius_) == 1)
	{
		this->setSpeed(-DISPLACEMENT);
	}
}