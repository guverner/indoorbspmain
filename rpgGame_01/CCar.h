#pragma once

#include "GameEngine.h"
class Object3D;

class CCar
{
private:
	float m_speed;
	
public:
	ThreeDModel m_model;
	float m_spin;
	Vector4D m_center;
	Vector4D m_dirVect;
	Vector4D vertices[8];
	float m_transfMatrix[16];

	CCar(char* file_, float posX, float posY, float posZ);
	void draw(TypeDraw type);
	float getSpeed();
	void setSpeed(float speed_);
	void setSpin(float spin_);
	Vector4D* getDirVect();
	void setDirVect(float value_);
	void updateVertices();
	void drawSpeed(font_data& font_);
	double** calculateNewRotatAxis(BYTE* heighMap_);

	void collisionCar3dObj(Object3D* obj_);
	void collisionCarBoundMap(Vector4D* vertArray_, font_data& font_);
	int collideCarCircle(Vector4D& point_, float radius_);
	void collisionCarTree(Vector4D& point_, float radius_);
};