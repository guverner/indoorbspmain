#include "CFrustum.h"
#include <cmath>
#include "gl\glew.h"

enum FrustumSide
{
	RIGHT	= 0,		// The RIGHT side of the frustum
	LEFT	= 1,		// The LEFT	 side of the frustum
	BOTTOM	= 2,		// The BOTTOM side of the frustum
	TOP		= 3,		// The TOP side of the frustum
	BACK	= 4,		// The BACK	side of the frustum
	FRONT	= 5			// The FRONT side of the frustum
};

enum PlaneData
{
	A = 0,				// The X value of the plane's normal
	B = 1,				// The Y value of the plane's normal
	C = 2,				// The Z value of the plane's normal
	D = 3				// The distance the plane is from the origin
};

//This normalizes a plane from a given frustum.
void NormalizePlane(float frustum[6][4], int side)
{
	// Here we calculate the magnitude of the normal to the plane
	// To calculate magnitude you use the equation:  magnitude = sqrt( x^2 + y^2 + z^2)
	float magnitude = (float)sqrt( frustum[side][A] * frustum[side][A] + 
								   frustum[side][B] * frustum[side][B] + 
								   frustum[side][C] * frustum[side][C] );

	// Then we divide the plane's values by it's magnitude.
	frustum[side][A] /= magnitude;
	frustum[side][B] /= magnitude;
	frustum[side][C] /= magnitude;
	frustum[side][D] /= magnitude; 
}

//This extracts our frustum from the projection and modelview matrix.
void CFrustum::CalculateFrustum()
{    
	float   proj[16];								// This will hold our projection matrix
	float   modl[16];								// This will hold our modelview matrix
	float   clip[16];								// This will hold the clipping planes

	glGetFloatv( GL_PROJECTION_MATRIX, proj );
	glGetFloatv( GL_MODELVIEW_MATRIX, modl );

	// Now that we have our modelview and projection matrix, if we combine these 2 matrices,
	// it will give us our clipping planes.  To combine 2 matrices, we multiply them.

	clip[ 0] = modl[ 0] * proj[ 0] + modl[ 1] * proj[ 4] + modl[ 2] * proj[ 8] + modl[ 3] * proj[12];
	clip[ 1] = modl[ 0] * proj[ 1] + modl[ 1] * proj[ 5] + modl[ 2] * proj[ 9] + modl[ 3] * proj[13];
	clip[ 2] = modl[ 0] * proj[ 2] + modl[ 1] * proj[ 6] + modl[ 2] * proj[10] + modl[ 3] * proj[14];
	clip[ 3] = modl[ 0] * proj[ 3] + modl[ 1] * proj[ 7] + modl[ 2] * proj[11] + modl[ 3] * proj[15];

	clip[ 4] = modl[ 4] * proj[ 0] + modl[ 5] * proj[ 4] + modl[ 6] * proj[ 8] + modl[ 7] * proj[12];
	clip[ 5] = modl[ 4] * proj[ 1] + modl[ 5] * proj[ 5] + modl[ 6] * proj[ 9] + modl[ 7] * proj[13];
	clip[ 6] = modl[ 4] * proj[ 2] + modl[ 5] * proj[ 6] + modl[ 6] * proj[10] + modl[ 7] * proj[14];
	clip[ 7] = modl[ 4] * proj[ 3] + modl[ 5] * proj[ 7] + modl[ 6] * proj[11] + modl[ 7] * proj[15];

	clip[ 8] = modl[ 8] * proj[ 0] + modl[ 9] * proj[ 4] + modl[10] * proj[ 8] + modl[11] * proj[12];
	clip[ 9] = modl[ 8] * proj[ 1] + modl[ 9] * proj[ 5] + modl[10] * proj[ 9] + modl[11] * proj[13];
	clip[10] = modl[ 8] * proj[ 2] + modl[ 9] * proj[ 6] + modl[10] * proj[10] + modl[11] * proj[14];
	clip[11] = modl[ 8] * proj[ 3] + modl[ 9] * proj[ 7] + modl[10] * proj[11] + modl[11] * proj[15];

	clip[12] = modl[12] * proj[ 0] + modl[13] * proj[ 4] + modl[14] * proj[ 8] + modl[15] * proj[12];
	clip[13] = modl[12] * proj[ 1] + modl[13] * proj[ 5] + modl[14] * proj[ 9] + modl[15] * proj[13];
	clip[14] = modl[12] * proj[ 2] + modl[13] * proj[ 6] + modl[14] * proj[10] + modl[15] * proj[14];
	clip[15] = modl[12] * proj[ 3] + modl[13] * proj[ 7] + modl[14] * proj[11] + modl[15] * proj[15];
	
	// Now we actually want to get the sides of the frustum.  To do this we take
	// the clipping planes we received above and extract the sides from them.

	// This will extract the RIGHT side of the frustum
	m_Frustum[RIGHT][A] = clip[ 3] - clip[ 0];
	m_Frustum[RIGHT][B] = clip[ 7] - clip[ 4];
	m_Frustum[RIGHT][C] = clip[11] - clip[ 8];
	m_Frustum[RIGHT][D] = clip[15] - clip[12];

	// Normalize the RIGHT side
	NormalizePlane(m_Frustum, RIGHT);

	// This will extract the LEFT side of the frustum
	m_Frustum[LEFT][A] = clip[ 3] + clip[ 0];
	m_Frustum[LEFT][B] = clip[ 7] + clip[ 4];
	m_Frustum[LEFT][C] = clip[11] + clip[ 8];
	m_Frustum[LEFT][D] = clip[15] + clip[12];

	// Normalize the LEFT side
	NormalizePlane(m_Frustum, LEFT);

	// This will extract the BOTTOM side of the frustum
	m_Frustum[BOTTOM][A] = clip[ 3] + clip[ 1];
	m_Frustum[BOTTOM][B] = clip[ 7] + clip[ 5];
	m_Frustum[BOTTOM][C] = clip[11] + clip[ 9];
	m_Frustum[BOTTOM][D] = clip[15] + clip[13];

	// Normalize the BOTTOM side
	NormalizePlane(m_Frustum, BOTTOM);

	// This will extract the TOP side of the frustum
	m_Frustum[TOP][A] = clip[ 3] - clip[ 1];
	m_Frustum[TOP][B] = clip[ 7] - clip[ 5];
	m_Frustum[TOP][C] = clip[11] - clip[ 9];
	m_Frustum[TOP][D] = clip[15] - clip[13];

	// Normalize the TOP side
	NormalizePlane(m_Frustum, TOP);

	// This will extract the BACK side of the frustum
	m_Frustum[BACK][A] = clip[ 3] - clip[ 2];
	m_Frustum[BACK][B] = clip[ 7] - clip[ 6];
	m_Frustum[BACK][C] = clip[11] - clip[10];
	m_Frustum[BACK][D] = clip[15] - clip[14];

	// Normalize the BACK side
	NormalizePlane(m_Frustum, BACK);

	// This will extract the FRONT side of the frustum
	m_Frustum[FRONT][A] = clip[ 3] + clip[ 2];
	m_Frustum[FRONT][B] = clip[ 7] + clip[ 6];
	m_Frustum[FRONT][C] = clip[11] + clip[10];
	m_Frustum[FRONT][D] = clip[15] + clip[14];

	// Normalize the FRONT side
	NormalizePlane(m_Frustum, FRONT);
}

bool CFrustum::PortalInFrustum( tPortal& portal )
{
	tPlane plane = portal.plain;

	for(int indVertex = 0; indVertex < NUM_VERT_IN_PORTAL; indVertex++)
	{
		if(this->PointInFrustum(plane.vertices[indVertex].x,
							plane.vertices[indVertex].y,
							plane.vertices[indVertex].z))
		{
			return true;
		}
	}

	//if there is no portal's point which is inside the view-frustum,
	//need to check check plane-plane intersection
	//Some code should be here!!!

	//get information about left and right plane of the view frustym
	Vector3d normalLeft(m_Frustum[LEFT][A],m_Frustum[LEFT][B],m_Frustum[LEFT][C]);
	normalLeft = Vector3d::normalize(normalLeft);
	float leftD = m_Frustum[LEFT][D];
	Vector3d normalRight(m_Frustum[RIGHT][A],m_Frustum[RIGHT][B],m_Frustum[RIGHT][C]);
	normalRight = Vector3d::normalize(normalRight);
	float rightD = m_Frustum[RIGHT][D];
	Vector3d normalFront(m_Frustum[FRONT][A],m_Frustum[FRONT][B],m_Frustum[FRONT][C]);
	normalFront = Vector3d::normalize(normalFront);
	float frontD = m_Frustum[FRONT][D];
	Vector3d normalBack(m_Frustum[BACK][A],m_Frustum[BACK][B],m_Frustum[BACK][C]);
	normalBack = Vector3d::normalize(normalBack);
	float backD = m_Frustum[BACK][D];

	//get bottom boints of the portal
	Vector3d* portalVertices = plane.getVertices();
	int indsPortalBottomVerts[2];//indices of the portal's bottom vertices
	indsPortalBottomVerts[0] = 0;
	indsPortalBottomVerts[1] = -1;

	for(int indPortalBottomVert = 1; indPortalBottomVert < NUM_VERT_IN_PORTAL; indPortalBottomVert++)
	{
		if(portalVertices[indPortalBottomVert].y < portalVertices[indsPortalBottomVerts[0]].y)
		{
			indsPortalBottomVerts[0] = indPortalBottomVert;
			indsPortalBottomVerts[1] = indsPortalBottomVerts[0];
		}
	}
	//if second index == -1 it means that first index was first vertex of the portal
	if(indsPortalBottomVerts[1] == -1)
	{
		indsPortalBottomVerts[1] = 1;

		for(int indPortalBottomVert = 2; indPortalBottomVert < NUM_VERT_IN_PORTAL; indPortalBottomVert++)
		{
			if(portalVertices[indPortalBottomVert].y < portalVertices[indsPortalBottomVerts[1]].y)
			{
				indsPortalBottomVerts[1] = indPortalBottomVert;
			}
		}
	}

	////ckeck bottom points of the portals to the fornt and back planes of the view frustum
	//float distFront1 = (Vector3d::dotProduct(normalFront, portalVertices[indsPortalBottomVerts[0]]) + frontD) /
	//					(normalFront.size());
	//float distFront2 = (Vector3d::dotProduct(normalFront, portalVertices[indsPortalBottomVerts[1]]) + frontD) /
	//					(normalFront.size());
	float distBack1 = (Vector3d::dotProduct(normalBack, portalVertices[indsPortalBottomVerts[0]]) + backD) /
						(normalBack.size());
	float distBack2 = (Vector3d::dotProduct(normalBack, portalVertices[indsPortalBottomVerts[1]]) + backD) /
						(normalBack.size());

	//now need to check left plane of the view frustym with two bottom vertices of the portal and then right plane
	float distLeft1 = (Vector3d::dotProduct(normalLeft, portalVertices[indsPortalBottomVerts[0]]) + leftD) /
						(normalLeft.size());
	float distLeft2 = (Vector3d::dotProduct(normalLeft, portalVertices[indsPortalBottomVerts[1]]) + leftD) /
						(normalLeft.size());
	/*cout << "Left:" << endl;
	cout << "distLeft1 = " << distLeft1 << endl;
	cout << "distLeft2 = " << distLeft2 << endl;*/
	if(((distLeft1 >= 0 && distLeft2 < 0) || (distLeft1 < 0 && distLeft2 >= 0))/* ||
		((onLeftForWall(this->getCamera()->View(), this->getCamera()->Position(), portalVertices[indsPortalBottomVerts[0]]) == 1 &&
		onLeftForWall(this->getCamera()->View(), this->getCamera()->Position(), portalVertices[indsPortalBottomVerts[1]]) == 2)
		&& (Vector3d::dotProduct(normalBack, portalVertices[indsPortalBottomVerts[0]]) + backD) / (normalBack.size()) < 0 &&
		(Vector3d::dotProduct(normalBack, portalVertices[indsPortalBottomVerts[1]]) + backD) / (normalBack.size()) < 0
		||
		(onLeftForWall(this->getCamera()->View(), this->getCamera()->Position(), portalVertices[indsPortalBottomVerts[0]]) == 2 &&
		onLeftForWall(this->getCamera()->View(), this->getCamera()->Position(), portalVertices[indsPortalBottomVerts[1]]) == 1)
		&& (Vector3d::dotProduct(normalBack, portalVertices[indsPortalBottomVerts[0]]) + backD) / (normalBack.size()) < 0 &&
		(Vector3d::dotProduct(normalBack, portalVertices[indsPortalBottomVerts[1]]) + backD) / (normalBack.size()) < 0)*/)
	{
		return true;
	}
	
	float distRight1 = (Vector3d::dotProduct(normalRight, portalVertices[indsPortalBottomVerts[0]]) + rightD) /
						(normalRight.size());
	float distRight2 = (Vector3d::dotProduct(normalRight, portalVertices[indsPortalBottomVerts[1]]) + rightD) /
						(normalRight.size());
	/*cout << "Right:" << endl;
	cout << "distRight1 = " << distRight1 << endl;
	cout << "distRight2 = " << distRight2 << endl;*/
	if((distRight1 > 0 && distRight2 < 0) || (distRight1 < 0 && distRight2 > 0)/* ||
		((onLeftForWall(this->getCamera()->View(), this->getCamera()->Position(), portalVertices[indsPortalBottomVerts[0]]) == 1 &&
		onLeftForWall(this->getCamera()->View(), this->getCamera()->Position(), portalVertices[indsPortalBottomVerts[1]]) == 2) 
		&& (Vector3d::dotProduct(normalBack, portalVertices[indsPortalBottomVerts[0]]) + backD) / (normalBack.size()) < 0 &&
		(Vector3d::dotProduct(normalBack, portalVertices[indsPortalBottomVerts[1]]) + backD) / (normalBack.size()) < 0
		||
		(onLeftForWall(this->getCamera()->View(), this->getCamera()->Position(), portalVertices[indsPortalBottomVerts[0]]) == 2 &&
		onLeftForWall(this->getCamera()->View(), this->getCamera()->Position(), portalVertices[indsPortalBottomVerts[1]]) == 1)
		&& (Vector3d::dotProduct(normalBack, portalVertices[indsPortalBottomVerts[0]]) + backD) / (normalBack.size()) < 0 &&
		(Vector3d::dotProduct(normalBack, portalVertices[indsPortalBottomVerts[1]]) + backD) / (normalBack.size()) < 0)*/)
	{
		return true;
	}
	
	return false;
}

bool CFrustum::PointInFrustum( float x, float y, float z )
{
	// Go through all the sides of the frustum
	for(int i = 0; i < 6; i++ )
	{
		// Calculate the plane equation and check if the point is behind a side of the frustum
		if(m_Frustum[i][A] * x + m_Frustum[i][B] * y + m_Frustum[i][C] * z + m_Frustum[i][D] <= 0)
		{
			// The point was behind a side, so it ISN'T in the frustum
			return false;
		}
	}

	// The point was inside of the frustum (In front of ALL the sides of the frustum)
	return true;
}

bool CFrustum::SphereInFrustum( float x, float y, float z, float radius )
{
	// Go through all the sides of the frustum
	for(int i = 0; i < 6; i++ )	
	{
		// If the center of the sphere is farther away from the plane than the radius
		if( m_Frustum[i][A] * x + m_Frustum[i][B] * y + m_Frustum[i][C] * z + m_Frustum[i][D] <= -radius )
		{
			// The distance was greater than the radius so the sphere is outside of the frustum
			return false;
		}
	}
	
	// The sphere was inside of the frustum!
	return true;
}

bool CFrustum::CubeInFrustum( float x, float y, float z, float size )
{
	for(int i = 0; i < 6; i++ )
	{
		if(m_Frustum[i][A] * (x - size) + m_Frustum[i][B] * (y - size) + m_Frustum[i][C] * (z - size) + m_Frustum[i][D] > 0)
		   continue;
		if(m_Frustum[i][A] * (x + size) + m_Frustum[i][B] * (y - size) + m_Frustum[i][C] * (z - size) + m_Frustum[i][D] > 0)
		   continue;
		if(m_Frustum[i][A] * (x - size) + m_Frustum[i][B] * (y + size) + m_Frustum[i][C] * (z - size) + m_Frustum[i][D] > 0)
		   continue;
		if(m_Frustum[i][A] * (x + size) + m_Frustum[i][B] * (y + size) + m_Frustum[i][C] * (z - size) + m_Frustum[i][D] > 0)
		   continue;
		if(m_Frustum[i][A] * (x - size) + m_Frustum[i][B] * (y - size) + m_Frustum[i][C] * (z + size) + m_Frustum[i][D] > 0)
		   continue;
		if(m_Frustum[i][A] * (x + size) + m_Frustum[i][B] * (y - size) + m_Frustum[i][C] * (z + size) + m_Frustum[i][D] > 0)
		   continue;
		if(m_Frustum[i][A] * (x - size) + m_Frustum[i][B] * (y + size) + m_Frustum[i][C] * (z + size) + m_Frustum[i][D] > 0)
		   continue;
		if(m_Frustum[i][A] * (x + size) + m_Frustum[i][B] * (y + size) + m_Frustum[i][C] * (z + size) + m_Frustum[i][D] > 0)
		   continue;

		// If we get here, it isn't in the frustum
		return false;
	}

	return true;
}

bool CFrustum::bbInFrustum(float x, float y, float z, float halfLengthX, float halfLengthY, float halfLengthZ)
{
	for(int i = 0; i < 6; i++ )
	{
		if(m_Frustum[i][A] * (x - halfLengthX) + m_Frustum[i][B] * (y - halfLengthY) + m_Frustum[i][C] * (z - halfLengthZ) + m_Frustum[i][D] > 0)
		   continue;
		if(m_Frustum[i][A] * (x + halfLengthX) + m_Frustum[i][B] * (y - halfLengthY) + m_Frustum[i][C] * (z - halfLengthZ) + m_Frustum[i][D] > 0)
		   continue;
		if(m_Frustum[i][A] * (x - halfLengthX) + m_Frustum[i][B] * (y + halfLengthY) + m_Frustum[i][C] * (z - halfLengthZ) + m_Frustum[i][D] > 0)
		   continue;
		if(m_Frustum[i][A] * (x + halfLengthX) + m_Frustum[i][B] * (y + halfLengthY) + m_Frustum[i][C] * (z - halfLengthZ) + m_Frustum[i][D] > 0)
		   continue;
		if(m_Frustum[i][A] * (x - halfLengthX) + m_Frustum[i][B] * (y - halfLengthY) + m_Frustum[i][C] * (z + halfLengthZ) + m_Frustum[i][D] > 0)
		   continue;
		if(m_Frustum[i][A] * (x + halfLengthX) + m_Frustum[i][B] * (y - halfLengthY) + m_Frustum[i][C] * (z + halfLengthZ) + m_Frustum[i][D] > 0)
		   continue;
		if(m_Frustum[i][A] * (x - halfLengthX) + m_Frustum[i][B] * (y + halfLengthY) + m_Frustum[i][C] * (z + halfLengthZ) + m_Frustum[i][D] > 0)
		   continue;
		if(m_Frustum[i][A] * (x + halfLengthX) + m_Frustum[i][B] * (y + halfLengthY) + m_Frustum[i][C] * (z + halfLengthZ) + m_Frustum[i][D] > 0)
		   continue;

		// If we get here, it isn't in the frustum
		return false;
	}

	return true;
}

void CFrustum::rec_findRoomsToDraw(int roomID)
{
	//data of this room-----------------------------------------
	CRoom* roomWithCam = m_sceneManager->getRoom(roomID);
	vector<tPortal>* roomPortals = (*roomWithCam).getPortals();
	//-----------------------------------------------------------
	m_idRoomsNeedDraw.push_back(roomID);//draw this room

	vector<tPortal> portalsInsideViewFrust;

	//go throw all portals and find portals which are inside view-frustym
	for(int indPortal = 0; indPortal < (*roomPortals).size(); indPortal++)
	{
		//if the portal inside the view-frustum
		if(m_isIgnoreFrustum || this->PortalInFrustum((*roomPortals)[indPortal]))
		{
			portalsInsideViewFrust.push_back((*roomPortals)[indPortal]);
		}
	}

	vector<int> idRoomsWithThisPortals;
	//go throw portals which are inside the view-frustym
	for(int indPortal = 0; indPortal < portalsInsideViewFrust.size(); indPortal++)
	{
		findRoomsWithThisPortal(portalsInsideViewFrust[indPortal], idRoomsWithThisPortals);
	}

	for(int indIdRoom = 0; indIdRoom < idRoomsWithThisPortals.size(); indIdRoom++)
	{
		//only ncontinue recursion function if there is no rooms with this id
		if(idRoomsWithThisPortals[indIdRoom] != roomID)
		{
			bool continueRec = true;

			for(int indRoomNeedDraw = 0; indRoomNeedDraw < m_idRoomsNeedDraw.size(); indRoomNeedDraw++)
			{
				if(idRoomsWithThisPortals[indIdRoom] == m_idRoomsNeedDraw[indRoomNeedDraw])
				{
					continueRec = false;
				}
			}

			if(continueRec)
			{
				rec_findRoomsToDraw(idRoomsWithThisPortals[indIdRoom]);
			}
		}
	}
}

void CFrustum::findRoomsWithThisPortal(tPortal& portal, vector<int>& idRoomsWithThisPortal)
{
	vector<CRoom*>* rooms = m_sceneManager->getRooms();

	for(int indRoom = 0; indRoom < rooms->size(); indRoom++)
	{
		vector<tPortal>* roomPortals = (*rooms)[indRoom]->getPortals();

		for(int indPortal = 0; indPortal < (*roomPortals).size(); indPortal++)
		{
			if(portal.idPortal == (*roomPortals)[indPortal].idPortal)
			{
				idRoomsWithThisPortal.push_back((*rooms)[indRoom]->getId());
			}
		}
	}
}

void CFrustum::setSceneManager(cSceneManager* sceneManager_)
{
	m_sceneManager = sceneManager_;
}

void CFrustum::setIsIgnoreFrustum(bool value)
{
	m_isIgnoreFrustum = value;
}

void CFrustum::eraseIdRoomNeedDraw()
{
	//delete all rooms from it
	if(m_idRoomsNeedDraw.size() > 0)
	{
		m_idRoomsNeedDraw.erase(m_idRoomsNeedDraw.begin(), m_idRoomsNeedDraw.end());
	}
}

vector<int>* CFrustum::getIdRoomsNeedDraw()
{
	return &m_idRoomsNeedDraw;
}

void CFrustum::setCamera(CCamera* camera_)
{
	this->m_camera = camera_;
}

CCamera* CFrustum::getCamera()
{
	return m_camera; 
}