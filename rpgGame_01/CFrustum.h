#pragma once
#include "CBinaryTree.h"
#include "cSceneManager.h"

class CFrustum
{
public:

	// Call this every time the camera moves to update the frustum
	void CalculateFrustum();

	// This takes a 3D point and returns TRUE if it's inside of the frustum
	bool PointInFrustum(float x, float y, float z);

	// This takes a 3D point and a radius and returns TRUE if the sphere is inside of the frustum
	bool SphereInFrustum(float x, float y, float z, float radius);

	// This takes the center and half the length of the cube.
	bool CubeInFrustum( float x, float y, float z, float size );

	//bounding box in frustum
	bool bbInFrustum( float x, float y, float z, float halfLengthX, float halfLengthY, float halfLengthZ );

	bool PortalInFrustum( tPortal& portal );

	//find set of room which we need to draw
	//(according to the portals of the camera's room inside the frustum)
	void rec_findRoomsToDraw(int roomID);

	void setSceneManager(cSceneManager* sceneManager_);
	void setIsIgnoreFrustum(bool value);
	void eraseIdRoomNeedDraw();
	vector<int>* getIdRoomsNeedDraw();

	void setCamera(CCamera* camera_);
	CCamera* getCamera();

private:
	// This holds the A B C and D values for each side of our frustum.
	float m_Frustum[6][4];

	bool m_isIgnoreFrustum;
	cSceneManager* m_sceneManager;
	vector<int> m_idRoomsNeedDraw;
	CCamera* m_camera;

	//FUNCTIONS:
	void findRoomsWithThisPortal(tPortal& portal, vector<int>& idRoomsWithThisPortal);
};