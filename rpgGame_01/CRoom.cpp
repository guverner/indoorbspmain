#include "CRoom.h"

CRoom::CRoom(int id_)
{
	m_roomID = id_;
}

CRoom::CRoom(int id_, vector<tWall*>* boundaryPolygons_)
{
	m_roomID = id_;

	//go throw boundary polygons
	for(int indBoundPoly = 0; indBoundPoly < boundaryPolygons_->size(); indBoundPoly++)
	{
		m_vBoundaryPolys.push_back((*boundaryPolygons_)[indBoundPoly]->getPlain());
	}
}

int CRoom::getId()
{
	return m_roomID;
}

void CRoom::setId(int id_)
{
	m_roomID = id_;
}

vector<Object3D*>* CRoom::getRoomObjects()
{
	return &m_vObjects;
}

void CRoom::addRoomObject(Object3D* object)
{
	m_vObjects.push_back(object);
}

vector<tPlane>* CRoom::getBoundaryPolys()
{
	return &m_vBoundaryPolys;
}

vector<tPortal>* CRoom::getPortals()
{
	return &m_vPortals;
}

void CRoom::addPortal(tPortal& portal)
{
	m_vPortals.push_back(portal);
}

void CRoom::drawBoundaries(bool isLineLoop)
{
	const int NUM_VERTS = 4;//number of vertices in the polygon

	if(!isLineLoop)
	{
		glColor3f(1,0,0);
		glBegin(GL_QUADS);
		for(int indBoundary = 0; indBoundary < m_vBoundaryPolys.size(); indBoundary++)
		{
			tPlane plane = m_vBoundaryPolys[indBoundary];

			for(int indVertex = 0; indVertex < NUM_VERTS; indVertex++)
			{
				glVertex3f(plane.vertices[indVertex].x,
							plane.vertices[indVertex].y,
							plane.vertices[indVertex].z);
			}
		}
		glEnd();
		glColor3f(1,1,1);
	}
	else
	{
		glColor3f(1,0,0);
		for(int indBoundary = 0; indBoundary < m_vBoundaryPolys.size(); indBoundary++)
		{
			tPlane plane = m_vBoundaryPolys[indBoundary];

			glBegin(GL_LINE_LOOP);
				for(int indVertex = 0; indVertex < NUM_VERTS; indVertex++)
				{
					glVertex3f(plane.vertices[indVertex].x,
								plane.vertices[indVertex].y,
								plane.vertices[indVertex].z);
				}
			glEnd();
		}
		glColor3f(1,1,1);
	}
}

void CRoom::drawObjects()
{
	glEnable(GL_LIGHTING);
		for(int indObject = 0; indObject < m_vObjects.size(); indObject++)
		{
			m_vObjects[indObject]->drawWithTransform();
		}
	glDisable(GL_LIGHTING);
}

void CRoom::drawPortals()
{
	const int NUM_VERTS = 4;//number of vertices in the polygon

	for(int indPortal = 0; indPortal < m_vPortals.size(); indPortal++)
	{
		tPlane plane = m_vPortals[indPortal].plain;

		glColor3f(0,1,0);
		glBegin(GL_LINE_LOOP);
			for(int indVertex = 0; indVertex < NUM_VERTS; indVertex++)
			{
				glVertex3f(plane.vertices[indVertex].x,
							plane.vertices[indVertex].y,
							plane.vertices[indVertex].z);
			}
		glEnd();
	}
}

void CRoom::drawRoom()
{
	const bool DRAW_LINE_LOOP = true; //draw boundaries as line loop or as polygon (quad)

	drawBoundaries(DRAW_LINE_LOOP);
	glEnable(GL_CULL_FACE);
	drawObjects();
	glDisable(GL_CULL_FACE);
	drawPortals();
}