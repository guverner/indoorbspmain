#pragma once
#include "GameEngine.h"
#include "Object3D.h"
#include "CBinaryTree.h"

class CRoom
{
public:
	// FUNCTIONS:
		CRoom(int id_ = -1);
		CRoom(int id_, vector<tWall*>* boundaryPolygons_);
		~CRoom(){}

		int getId();
		void setId(int id_);
		vector<Object3D*>* getRoomObjects();
		void addRoomObject(Object3D* object);
		//get boundary polygons
		vector<tPlane>* getBoundaryPolys();
		vector<tPortal>* getPortals();
		void addPortal(tPortal& portal);

			//Draw functions:
			void drawRoom();
private:
	// VARIABLES:
		int m_roomID;
		vector<Object3D*> m_vObjects;//objects inside the room
		vector<tPlane> m_vBoundaryPolys;//boundary polygons for the room
		vector<tPortal> m_vPortals;

	// FUNCTIONS:
		void drawBoundaries(bool isLineLoop);
		void drawObjects();
		void drawPortals();
};