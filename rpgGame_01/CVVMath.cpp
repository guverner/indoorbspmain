#include "Settings.h"
#include "CVVMath.h"

float findSlope(const CPoint3D& point1, const CPoint3D& point2)
{
	return (point2.y - point1.y)/(point2.x - point1.x);
}

float findSlope(const CPoint3D& point1, float intersectWithYaxes)
{
	return (point1.y - intersectWithYaxes)/point1.x;
}

bool isParallelLines(const CPoint3D& line1P1, const CPoint3D& line1P2,
						const CPoint3D& line2P1, const CPoint3D& line2P2)
{
	if(findSlope(line1P1, line1P2) == findSlope(line2P1, line2P2))
	{
		return true;
	}
	return false;
}

bool isParallelLines(float slopeLine1, float slopeLine2)
{
	if(slopeLine1 == slopeLine2)
	{
		return true;
	}
	return false;
}

bool isPerpendicularLines(const CPoint3D& line1P1, const CPoint3D& line1P2,
						const CPoint3D& line2P1, const CPoint3D& line2P2)
{
	if((findSlope(line1P1, line1P2) * findSlope(line2P1, line2P2)) == -1)
	{
		return true;
	}
	return false;
}

bool isPerpendicularLines(float slopeLine1, float slopeLine2)
{
	if((slopeLine1 * slopeLine2) == -1)
	{
		return true;
	}
	return false;
}

float findPerpendicularSlope(float slopeLine1)
{
	return	(-1.0/slopeLine1);
}

float findPerpendicularSlope(const CPoint3D& linePoint1, const CPoint3D& linePoint2)
{
	return	(-1.0/findSlope(linePoint1, linePoint2));
}

CPoint3D findInterceptPoint2D(const CPoint3D& line1P1, const CPoint3D& line1P2,
						const CPoint3D& line2P1, const CPoint3D& line2P2)
{
	float slopeLine1 = findSlope(line1P1, line1P2);
	float slopeLine2 = findSlope(line2P1, line2P2);

	//find x by using 2 equations of lines
	float x = (slopeLine1*line1P1.x - line1P1.y - slopeLine2 * line2P1.x + line2P1.y) / (slopeLine1 - slopeLine2);
	//put x to one of the line equations
	float y = slopeLine1 * x - slopeLine1 * line1P1.x + line1P1.y;

	CPoint3D interceptPoint = {x,y,0};

	return interceptPoint;
}

float findDistance2D(const CPoint3D& line1P, const CPoint3D& line2P)
{
	return sqrt((line2P.x - line1P.x)*(line2P.x - line1P.x) + (line2P.y - line1P.y)*(line2P.y - line1P.y));
}

float findDistance3D(const CPoint3D& line1P, const CPoint3D& line2P)
{
	return sqrt((line2P.x - line1P.x)*(line2P.x - line1P.x) + (line2P.y - line1P.y)*(line2P.y - line1P.y) +
				(line2P.z - line1P.z)*(line2P.z - line1P.z));
}

bool isRightTriangle2D(const CPoint3D& point1, const CPoint3D& point2, const CPoint3D& point3)
{
	//need to find lengths of three sides of the triangle
	float lengthSide1 = findDistance2D(point1,point2);
	float lengthSide2 = findDistance2D(point2,point3);
	float lengthSide3 = findDistance2D(point3,point1);

	//put it to Pythagorean theorem
	if(lengthSide1*lengthSide1 + lengthSide2*lengthSide2 == lengthSide3*lengthSide3)
	{
		return true;
	}
	return false;
}

CPoint3D findMidPoint2D(const CPoint3D& point1, const CPoint3D& point2)
{
	CPoint3D midPoint = {(point1.x+point2.x)/2,(point1.y+point2.y)/2,0};
	return midPoint;
}

CPoint3D findMidPoint3D(const CPoint3D& point1, const CPoint3D& point2)
{
	CPoint3D midPoint = {(point1.x+point2.x)/2,(point1.y+point2.y)/2,(point1.z+point2.z)/2};
	return midPoint;
}

void drawParabola(bool isVertical, float a, const CPoint3D& vertex, float point1, float point2)
{
	if(isVertical)
	{
		float firstY;
		float y;

		glBegin(GL_LINE_STRIP);
		for(float x = vertex.x+point1; x <= vertex.x+point2; x+=0.2)
			{
				if(x == vertex.x+point1)
				{
					y = a*((x-vertex.x)*(x-vertex.x)) + vertex.y;
					firstY = y;
				}
				else
				{
					y = a*((x-vertex.x)*(x-vertex.x)) + vertex.y;
				}

				glVertex2f(x,y);
			}

			glVertex2f(vertex.x+point2,firstY);
		glEnd();
	}
	else
	{
		float x;
		float firstX;

		glBegin(GL_LINE_STRIP);
		for(float y = vertex.y+point1; y <= vertex.y+point2; y+=0.2)
			{
				if(y == vertex.y+point1)
				{
					x = a*((y-vertex.y)*(y-vertex.y)) + vertex.x;
					firstX = x;
				}
				else
				{
					x = a*((y-vertex.y)*(y-vertex.y)) + vertex.x;
				}

				glVertex2f(x,y);
			}
			
			glVertex2f(firstX,vertex.y+point2);
		glEnd();
	}
}

void CCitcle::draw()
{
	glBegin(GL_LINE_LOOP);
	for(float i=0; i<360; i+=0.005) 
	{
		float xcoord = this->center.x + this->radius * cos(i*(PI/180));
		float ycoord = this->center.y + this->radius * sin(i*(PI/180));
		glVertex2f(xcoord, ycoord);
	}
	glEnd();
}

float findDistanceTwoCircCenters(const CCitcle& circle1, const CCitcle& circle2)
{
	return sqrt((circle2.center.x-circle1.center.x)*(circle2.center.x-circle1.center.x) +
					(circle2.center.y-circle1.center.y)*(circle2.center.y-circle1.center.y));
}

float findDistanceTwoSphereCenters(const CCitcle& circle1, const CCitcle& circle2)
{
	return sqrt((circle2.center.x-circle1.center.x)*(circle2.center.x-circle1.center.x) +
					(circle2.center.y-circle1.center.y)*(circle2.center.y-circle1.center.y) +
					(circle2.center.z-circle1.center.z)*(circle2.center.z-circle1.center.z));
}

bool isCollisionCircleCircle(const CCitcle& circle1, const CCitcle& circle2)
{
	if((circle2.center.x-circle1.center.x)*(circle2.center.x-circle1.center.x) +
		(circle2.center.y-circle1.center.y)*(circle2.center.y-circle1.center.y) <=
			(circle1.radius + circle2.radius)*(circle1.radius + circle2.radius))
	{
		return true;
	}
	return false;
}

bool isCollisionSphereSphere3D(const CCitcle& circle1, const CCitcle& circle2)
{
	if((circle2.center.x-circle1.center.x)*(circle2.center.x-circle1.center.x) +
		(circle2.center.y-circle1.center.y)*(circle2.center.y-circle1.center.y) +
		(circle2.center.z-circle1.center.z)*(circle2.center.z-circle1.center.z) <=
		(circle1.radius + circle2.radius)*(circle1.radius + circle2.radius))
	{
		return true;
	}
	return false;
}

float DEGREES_TO_RAD(float deg)
{
	float radians = -1.0f;

	if(deg >= 0.0f && deg <= 360.0f)
	{
		radians = deg * DegToRad;//DegToRad = PI/180
	}

	return radians;
}

float RAD_TO_DEGREES(float rad)
{
	float degrees = -1.0f;

	if(rad >= 0 && rad <= PI*2)
	{
		degrees = rad * RadToDeg;//RadToDeg = 180/PI
	}

	return degrees;
}

Vector2D_comp polarToCortesCoord(const Vector2D_polar& vectPolar)
{
	float x = vectPolar.magnitude * cos(vectPolar.direction * DegToRad);
	float y = vectPolar.magnitude * sin(vectPolar.direction * DegToRad);
	Vector2D_comp vect = {x,y};
	return vect;
}

Vector2D_polar cortesToPolarCoord(const Vector2D_comp& vectCortes)
{
	float maginitude = sqrt(vectCortes.x*vectCortes.x + vectCortes.y*vectCortes.y);
	//if(maginitude==0)
	//	return 0;

	float direction = RadToDeg * asin(vectCortes.y/maginitude);//���� ����� � 1�� ��� 4�� ���������
	if(vectCortes.x < 0)//���� ����� � 2�� ��� 3�� ���������
	{
		direction += 180;
	}
	else if(vectCortes.x > 0 && vectCortes.y < 0)//���� ����� � 4�� ���������
	{
		direction += 360;
	}

	Vector2D_polar vect = {maginitude,direction};
	return vect;
}

int findLocPointFromLine(const CPoint3D& p, const CPoint3D& pLine1, const CPoint3D& pLine2)
{
	if(((p.y-pLine1.y)*(pLine2.x-pLine1.x) -
		(p.x-pLine1.x)*(pLine2.y-pLine1.y)) < 0)
	{
		//to the right of line segment
		return 1;
	}
	else if(((p.y-pLine1.y)*(pLine2.x-pLine1.x) -
		(p.x-pLine1.x)*(pLine2.y-pLine1.y)) > 0)
	{
		//to the left of line segment
		return 2;
	}
	else
	{
		//on the line
		return 3;
	}
}

//in XZ plane
int findLocPointFromLineXZ(Vector3D& p, Vector3D& pLine1, Vector3D& pLine2)
{
	if(((p.z-pLine1.z)*(pLine2.x-pLine1.x) -
		(p.x-pLine1.x)*(pLine2.z-pLine1.z)) < 0)//it is cross product (using signed area)
	{
		//to the right of line segment
		return 1;
	}
	else if(((p.z-pLine1.z)*(pLine2.x-pLine1.x) -
		(p.x-pLine1.x)*(pLine2.z-pLine1.z)) > 0)
	{
		//to the left of line segment
		return 2;
	}
	else
	{
		//on the line
		return 3;
	}
}

//in XZ plane
int findLocPointFromLineXZ(Vector4D& p, Vector4D& pLine1, Vector4D& pLine2)
{
	if(((p.z-pLine1.z)*(pLine2.x-pLine1.x) -
		(p.x-pLine1.x)*(pLine2.z-pLine1.z)) < 0.0)//it is cross product (using signed area)
	{
		//to the right of line segment
		return 1;
	}
	else if(((p.z-pLine1.z)*(pLine2.x-pLine1.x) -
		(p.x-pLine1.x)*(pLine2.z-pLine1.z)) > 0.0)
	{
		//to the left of line segment
		return 2;
	}
	else
	{
		//on the line
		return 3;
	}
}

int orientation(Point& v0, Point& v1, Point& v2)
{
	Point a = v1 - v0;
	Point b = v2 - v0;
	double sa = a.x * b.y - b.x * a.y;
	if(sa > 0.0)
	{
		return 1;//positive orientation
	}
	else if(sa < 0.0)
	{
		return -1;//negative orientation
	}
	else
		return 0;//collinearn
}

Vector3D Cross(Vector3D vVector1, Vector3D vVector2)
{
	Vector3D vNormal;									// The vector to hold the cross product

	// If we are given 2 vectors (the view and up vector) then we have a plane define.  
	// The cross product finds a vector that is perpendicular to that plane, 
	// which means it's point straight out of the plane at a 90 degree angle.
	// The equation for the cross product is simple, but difficult at first to memorize:
	
	// The X value for the vector is:  (V1.y * V2.z) - (V1.z * V2.y)													// Get the X value
	vNormal.x = ((vVector1.y * vVector2.z) - (vVector1.z * vVector2.y));
														
	// The Y value for the vector is:  (V1.z * V2.x) - (V1.x * V2.z)
	vNormal.y = ((vVector1.z * vVector2.x) - (vVector1.x * vVector2.z));
														
	// The Z value for the vector is:  (V1.x * V2.y) - (V1.y * V2.x)
	vNormal.z = ((vVector1.x * vVector2.y) - (vVector1.y * vVector2.x));

	// *IMPORTANT* This is not communitive. Just remember,
	// If you are trying to find the X, you don't use the X value of the 2 vectors, and
	// it's the same for the Y and Z.  You notice you use the other 2, but never that axis.
	// If you look at the camera rotation tutorial, you will notice it's the same for rotations.

	// Why doI need the cross product?  We need
	// to find the axis that our view has to rotate around.  Rotating the camera left
	// and right is simple, the axis is always (0, 1, 0).  Rotating around the camera
	// up and down is different because we are constantly going in and out of axises.
	// We need to find the axis that our camera is on, and that is why we use the cross
	// product.  By taking the cross product between our view vector and up vector,
	// we get a perpendicular vector to those 2 vectors, which is our desired axis.

	// Return the cross product
	return vNormal;
}

float Magnitude(Vector3D vNormal)
{
	// This will give us the magnitude or "Norm" as some say of, our normal.
	// The magnitude has to do with the length of the vector.  We use this
	// information to normalize a vector, which gives it a length of 1.
	// Here is the equation:  magnitude = sqrt(V.x^2 + V.y^2 + V.z^2)   Where V is the vector

	return (float)sqrt( (vNormal.x * vNormal.x) + 
						(vNormal.y * vNormal.y) + 
						(vNormal.z * vNormal.z) );
}

Vector3D Normalize(Vector3D vVector)
{
	// Why we need it? Since we are using the cross
	// product formula, we need to make sure our view vector is normalized.  
	// For a vector to be normalized, it means that it has a length of 1.
	// For instance, a vector (2, 0, 0) would be (1, 0, 0) once normalized.
	// Most equations work well with normalized vectors.  If in doubt, normalize.

	// Get the magnitude of our normal
	float magnitude = Magnitude(vVector);				

	// Now that we have the magnitude, we can divide our vector by that magnitude.
	// That will make our vector a total length of 1.  
	// This makes it easier to work with too.
	vVector = vVector / magnitude;		
	
	// Finally, return our normalized vector
	return vVector;	
}

bool collisionPointPoly(Vector4D& point_, Vector4D* poly_)
{
	for(int i = 0; i < 4; i++)
	{
		const int RIGHT_LOCAT = 1;
		const int LEFT_LOCAT = 2;
		const int ON_LINE_LOCAT = 3;

		if(i != 3)//if not last vertice
		{
			if(findLocPointFromLineXZ(point_, *(poly_ + i), *(poly_ + i + 1)) == RIGHT_LOCAT ||
				findLocPointFromLineXZ(point_, *(poly_ + i), *(poly_ + i + 1)) == ON_LINE_LOCAT)
			{
				continue;
			}
			else
			{
				return false;
			}
		}
		else
		{
			if(findLocPointFromLineXZ(point_, *(poly_ + i), *(poly_ + 0)) == LEFT_LOCAT)
			{
				return false;
			}
		}
	}

	//delete vertArray;
	return true;
}

bool collisionPointPoly(Vector3D& point_, Vector3D* poly_)
{
	//Vector3D* vertArray = rect.updateVertices();//iy is car.vertices array

	for(int i = 0; i < 4; i++)
	{
		const int RIGHT_LOCAT = 1;
		const int LEFT_LOCAT = 2;
		const int ON_LINE_LOCAT = 3;

		if(i != 3)//if not last vertice
		{
			if(findLocPointFromLineXZ(point_, *(poly_ + i), *(poly_ + i + 1)) == RIGHT_LOCAT ||
				findLocPointFromLineXZ(point_, *(poly_ + i), *(poly_ + i + 1)) == ON_LINE_LOCAT)
			{
				continue;
			}
			else
			{
				return false;
			}
		}
		else
		{
			if(findLocPointFromLineXZ(point_, *(poly_ + i), *(poly_ + 0)) == LEFT_LOCAT)
			{
				return false;
			}
		}
	}

	//delete vertArray;
	return true;
}

Vector4D clossestPointLinePoint(Vector4D& point_, Vector4D& lineA_, Vector4D& lineB_)
{
	Vector4D AP(point_ - lineA_);//lineA point
	Vector4D AB(lineB_ - lineA_);//lineA lineB


	float magnitudeAB = AB.x * AB.x + AB.z * AB.z;
	float magnitudeAP_AB = AP.x * AB.x + AP.z * AB.z;
	float t = magnitudeAP_AB / magnitudeAB;

	if(t < 0.0f)
	{
		t = 0.0f;
	}
	else if(t > 1.0f)
	{
		t = 1.0f;
	}

	Vector4D clossest(lineA_.x + AB.x * t,
						lineA_.y + AB.y * t,
						lineA_.z + AB.z * t,
						1);

	return clossest;
}

bool isCollideLineSegCircle(Vector4D& point_, float radius_, Vector4D& lineA_, Vector4D& lineB_)
{
	Vector4D closses(clossestPointLinePoint(point_, lineA_, lineB_));
	float RAD_TREE = radius_;
	Vector4D vectCarPTree(closses - point_);
	float magnitudeCarPTree = vectCarPTree.x * vectCarPTree.x + vectCarPTree.z * vectCarPTree.z;

	//cout << "magnitude = " << magnitudeCarPTree << endl;

	if(magnitudeCarPTree <= RAD_TREE * RAD_TREE)
	{
		return true;
		//cout << "collision" << endl;
	}
	else
	{
		return false;
		//cout << "not collision" << endl;
	}
}

bool isCollideRectangleCircle(Vector4D& point_, float radius_, Vector4D* rect_)
{
	int edge = 0;

	for(int i = 0; i < 4; i++)//go throw all car's boud. box edges
	{
		edge++;

		if(edge != 3)
		{
			if(isCollideLineSegCircle(point_, radius_, rect_[i], rect_[i+1])) //this->vertices[i], this->vertices[i+1]))
			{
				return true;
				//cout << "collision" << endl;
			}
			else
			{
				continue;
				//cout << "not" << endl;
			}
		}
		else
		{
			if(isCollideLineSegCircle(point_, radius_, rect_[i], rect_[0]))
			{
				return true;
				//cout << "collision" << endl;
			}
			else
			{
				continue;
				//cout << "not" << endl;
			}
		}
	}

	return false;
}

//////////////////////////////////////////////////////////////
bool isSame(float a, float b)
{
	float EPSILON = numeric_limits<float>::epsilon();
	return fabs(a - b) < EPSILON;
}

bool isYEqual(Vector3d& a, Vector3d& b)
{
	if(isSame(a.y, b.y))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool isXandZEqual(Vector3d& a, Vector3d& b)
{
	if(isSame(a.x, b.x) && isSame(a.z, b.z))
	{
		return true;
	}
	else
	{
		return false;
	}
}
///////////////////////////////////////////////////////////////

int onLeftForWall(Vector3d& a, Vector3d& b, Vector3d& c)
{
	if(((b.z - a.z)*(c.x - a.x) - (b.x - a.x)*(c.z - a.z)) > 0)
	{
		return 1;
	}
	else if(((b.z - a.z)*(c.x - a.x) - (b.x - a.x)*(c.z - a.z)) < 0)
	{
		return 2;
	}
	else
	{
		return 0;
	}
}

bool isLinesCollinear(Vector3d& line1p1, Vector3d& line1p2,
						Vector3d& line2p1, Vector3d& line2p2)
{
	if(onLeftForWall(line1p1, line1p2, line2p1) == 0 &&
		onLeftForWall(line1p1, line1p2, line2p2) == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}