#pragma once

#include "3D\Vector3D.h"
#include "3D\Vector4D.h"
#include "TypesForGame.h"

//input of functions is radians, output is decimal number between -1 and 1.

//So we need t do like this: result = sin (angle*pi/180);
//angle = asin (input)*180/pi;

#define RadToDeg 57.29577951f
#define DegToRad 0.017453293f

struct CPoint3D
{
	float x, y, z;

	//CPoint3D(float x_, float y_, float z_)
	//{
	//	x = x_;
	//	y = y_;
	//	z = z_;
	//}

	//CPoint3D()
	//{
	//	x = 0;
	//	y = 0;
	//	x = 0;
	//}
};

//Vector = magnitude + direction.

//displacement - ����������� � ��������� ��������
//Displacement = final position � initial position

// A structure for holding a vector in magnitude/direction form
//in polar coordinates
struct Vector2D_polar
{
	float magnitude;
	float direction;//in degrees
};
// A structure for holding a vector in component form
//in cortesian coordinates
struct Vector2D_comp
{
	float x, y;
};

Vector2D_comp polarToCortesCoord(const Vector2D_polar& vectPolar);

//converting from cortesian to polar coordinates
Vector2D_polar cortesToPolarCoord(const Vector2D_comp& vectCortes);

//can work as circle and sphere
struct CCitcle
{
	CPoint3D center;
	float radius;

	CCitcle(const CPoint3D& center_, float radius_)
	{
		center = center_;
		radius = radius_;
	}

	CCitcle(float centerX, float centerY, float radius_)
	{
		center.x = centerX;
		center.y = centerY;
		radius = radius_;
	}

	//Equation of a Circle:
	//(x-h)^2 + (y-k)^2 = r^2
	//where center is (h,k) and radius is r
	//(x,y) ��� ����� ����� �� ����������

	//if (h,k)=(0,0) and radius r then
	//equation of circle : x^2 + y^2  = r^2

	//Equaion of Sphere:
	//(x-h)^2 + (y-k)^2 + (z-l)^2 = r^2
	//if center is (0,0,0) and radius is r then
	//equation of sphere is: x^2 + y^2 + z^2 = r^2

	void draw();
};

//find slope when you know two points of this line
float findSlope(const CPoint3D& point1, const CPoint3D& point2);

//find slope when you know one point and interception of this line with y axes
float findSlope(const CPoint3D& point1, float intersectWithYaxes);

bool isParallelLines(const CPoint3D& line1P1, const CPoint3D& line1P2,
						const CPoint3D& line2P1, const CPoint3D& line2P2);
bool isParallelLines(float slopeLine1, float slopeLine2);

bool isPerpendicularLines(const CPoint3D& line1P1, const CPoint3D& line1P2,
						const CPoint3D& line2P1, const CPoint3D& line2P2);
bool isPerpendicularLines(float slopeLine1, float slopeLine2);

//find perpendicular slope for current line by its slope
float findPerpendicularSlope(float slopeLine1);
//find perpendicular slope for current line by its two points
float findPerpendicularSlope(const CPoint3D& linePoint1, const CPoint3D& linePoint2);

//find interception point of two lines by 2 points of one line and
//2 points of another line in 2D
CPoint3D findInterceptPoint2D(const CPoint3D& line1P1, const CPoint3D& line1P2,
						const CPoint3D& line2P1, const CPoint3D& line2P2);


//find distance between two points in 2D
float findDistance2D(const CPoint3D& line1P, const CPoint3D& line2P);
//find distance between two points in 3D
float findDistance3D(const CPoint3D& line1P, const CPoint3D& line2P);

//check if its right triangle or not in 2D
bool isRightTriangle2D(const CPoint3D& point1, const CPoint3D& point2, const CPoint3D& point3);

//find middle point between two points (line segment) in 2D
CPoint3D findMidPoint2D(const CPoint3D& point1, const CPoint3D& point2);
//find middle point between two points (line segment) in 3D
CPoint3D findMidPoint3D(const CPoint3D& point1, const CPoint3D& point2);

//======================
//Line equations:
//(1) Ax + By = C  =>  y = (C - Ax) / B
//(2) Slope-intercept form: y = mx + b	P.S: b is y-intercept (point where the line crosses the y-axis (P.P.S: x = 0 !))
//(3) Point-slope form: (y - y1) = m * (x - x1) where (x1, y1) its point on the line
//======================

//Parabola equations:
//with vertical axis: y=a*(x-h)^2+k where (h,k) is vertex and axis of summetry is x=h
//with horizontal axis: x=a*(y-k)^2+h where (h,k) is vertex and axis of summetry is y=k
//P.S:where a determines in which direction the parabola opens and how wide or skinny the opening is. If a is
//a positive number, the parabola opens up if it looks like y=a(x�h)2+k or opens to the right if it's x=a(y�k)2+h.
//If a is negative, the parabola opens downward or to the left.

//draw parabola
//Input: isVertical -	if true, parabola is vertical. if false, parabola is horizontal
//				a -		direction of parabola
//			vertex -	vertex of porabola (where change direction of porabola)
//			point1 -	first point of parabola
//			point2 -	last point of parabola
void drawParabola(bool isVertical, float a, const CPoint3D& vertex, float point1, float point2);

//find distance between two circle's centers
float findDistanceTwoCircCenters(const CCitcle& circle1, const CCitcle& circle2);
float findDistanceTwoSphereCenters(const CCitcle& circle1, const CCitcle& circle2);

//find there is collision bettween circles or not
bool isCollisionCircleCircle(const CCitcle& circle1, const CCitcle& circle2);
bool isCollisionSphereSphere3D(const CCitcle& circle1, const CCitcle& circle2);

float DEGREES_TO_RAD(float deg);
float RAD_TO_DEGREES(float rad);

struct CTriangle2D
{
	//point1 has right angle
	//point2 has alpha angle
	CPoint3D point1, point2, point3;
	float hypotenyse, opposite, adjacent;

	CTriangle2D(const CPoint3D& point1_, const CPoint3D& point2_,
				const CPoint3D& point3_)
	{
		point1 = point1_;
		point2 = point2_;
		point3 = point3_;
		
		//if it's right triangle
		//if()
		adjacent = point1.x - point2.x;
		opposite = point3.y - point1.y;
	}

	CTriangle2D(float point1X, float point1Y,
				float point2X, float point2Y,
				float point3X, float point3Y)
	{
		point1.x = point1X; point1.y = point1Y;
		point2.x = point2X; point2.y = point2Y;
		point3.x = point3X; point3.y = point3Y;
	}

	void draw()
	{
		glBegin(GL_LINE_LOOP);
			glVertex2f(point1.x,point1.y);
			glVertex2f(point2.x,point2.y);
			glVertex2f(point3.x,point3.y);
		glEnd();
	}
	//Trigonometric Functions:
	//There is hypotenyse - ����� �������� ���� � 90 ��������
	//			opposite - ����� �������� ���� �����
	//			adjacent - ����� ����� ����� � 90 �������� � ����� �����
	//P.S: Functions work only with right triangle
	//sina = opp/hyp
	//cosa = adj/hyp
	//tana = opp/adj
	//csca = hyp/opp
	//seca = hyp/adj
	//cota = adj/opp
};

int findLocPointFromLine(const CPoint3D& p,
						const CPoint3D& pLine1,
						const CPoint3D& pLine2);

int findLocPointFromLineXZ(Vector3D& p, Vector3D& pLine1, Vector3D& pLine2);
int findLocPointFromLineXZ(Vector4D& p, Vector4D& pLine1, Vector4D& pLine2);

enum {P_LEFT, P_RIGHT, P_BEYOND, P_BEHIND, P_BETWEEN, P_ORIGIN, P_DESTIONATION};
//	����� ������� ������� ������	�����	������	�����

class Point
{
public:
	double x;
	double y;
	Point(double x_ = 0.0, double y_ = 0.0) : x(x_), y(y_)
	{
	}

	Point operator+ (Point& v)
	{
		return Point(x + v.x, y + v.y);
	}

	Point operator- (Point& v)
	{
		return Point(x - v.x, y - v.y);
	}

	Point operator* (double s)
	{
		return Point(s * x, s * y);
	}

	double operator[] (int i)
	{
		return (i == 0) ? x : y;
	}

	int operator== (Point& v)
	{
		return (x == v.x) && (y == v.y);
	}

	int operator!= (Point& v)
	{
		return !(*this == v);
	}

	int operator< (Point& v)
	{
		return ((x < v.x) || (x == v.x) && (y < v.y));
	}

	int operator> (Point& v)
	{
		return ((x > v.x) || (x == v.x) && (y > v.y));
	}

	//Location of point ������������ ������
	int classify(Point& v0, Point& v1)
	{
		Point v2 = *this;
		Point a = v1 - v0;
		Point b = v2 - v0;
		double sa = a.x * b.y - b.x * a.y;
		if(sa > 0.0)
		{
			return P_LEFT;
		}
		else if(sa < 0.0)
		{
			return P_RIGHT;
		}
		else if((a.x * b.x < 0.0) || (a.y * b.y < 0.0))
		{
			return P_BEHIND;
		}
		else if(a.length() < b.length())
		{
			return P_BEYOND;
		}
		else if(v0 == v2)
		{
			return P_ORIGIN;
		}
		else if(v1 == v2)
		{
			return P_DESTIONATION;
		}
		else
		{
			return P_BETWEEN;
		}
	}

	double polarAngle()
	{
		if((x == 0.0) && (y == 0.0))
		{
			return -1.0;
		}
		else if(x == 0.0)
		{
			return ((y > 0.0) ? 90 : 270);
		}
		double theta = atan(y/x);//in radians
		theta *= 360 / (2 * 3.1415926);//in degrees
		if(x > 0.0)
		{
			return ((y >= 0.0) ? theta : 360 + theta);
		}
		else 
		{
			return (180 + theta);
		}
	}

	double length()
	{
		return sqrt(x*x + y*y);
	}
};

int orientation(Point& v0, Point& v1, Point& v2);

//class Vertex : public Point
//{
//public:
//	Vertex(double x, double y) : Point(x, y)
//	{
//	}
//
//	Vertex(Point& p) : Point(p)
//	{
//	}
//
//	Vertex* cw()
//	{
//		return (Vertex*)_next;
//	}
//}

//////////////////////////////////////////////////////////////////////////////////////////

//This returns a perpendicular vector from 2 given vectors by taking the cross product.
Vector3D Cross(Vector3D vVector1, Vector3D vVector2);
//This returns the magnitude of a vector
float Magnitude(Vector3D vNormal);
//This returns a normalize vector (A vector exactly of length 1)
Vector3D Normalize(Vector3D vVector);
//////////////////////////////////////////////////////////////////////////////////////////

enum TypeDraw{NORMAL, BOUNDBOX, OCTREE, ALL};

bool collisionPointPoly(Vector4D& point_, Vector4D* poly_);
bool collisionPointPoly(Vector3D& point_, Vector3D* poly_);

//closest point of the point to the line
Vector4D clossestPointLinePoint(Vector4D& point_, Vector4D& lineA_, Vector4D& lineB_);

bool isCollideLineSegCircle(Vector4D& point_, float radius_, Vector4D& lineA_, Vector4D& lineB_);
bool isCollideRectangleCircle(Vector4D& point_, float radius_, Vector4D* rect_);

class CPlane
{
	//three points not on the same line determine a
	//unique plane

	//Equations of th eplane:
	//	As 3 points: T(a,b) = P + b(1-a)(Q-P) + (1-b)(R-P) where (Q-P) and (R-P) are vectors,
	//	so equation can be made by one point (P0) and and two nonparallel vectors (u and v), as
	//	T(a,b) = P0 + au + bv
	//	If P lies in the plane, then P - P0 = au + bv
	//	n = u x v
	//	then equation of the plane become n . (P - P0) = 0
	//	where n is normal
	//
	//  p * n = d	where n is unit vector, n is normal and p is any point in the plane
	//
	//	Ax + By + Cz + D = 0 where (Ax + By + Cz) are components of the plane's normal.
	//	D = -N . P where N is a normal.
	//	if N is normaliset, then d = N.Q + D given the signed distance from the plane to an
	//	arbitrary point Q. If d = 0 then the point Q lies in the plane. If d > 0 then the point Q
	//	lies on the positive side of the plane (side where is the normal vector points). Otherwise,
	//	id d < 0 then Q lies pon the negative side of the plane.
	//
	//	If there is w coordinate of 1, then equation can be rewrited as d = L.Q where L = <N,D>.
	//	A point Q lies in the plane if L.Q = 0.

public:
	Vector3d n;
	float dist;//signed distance

	//compute signed distance from the point to the plane
	//It is negative if it is on the back side of the plane and positive if its on the front
	//of hte plane.
	float    signedDistanceTo (Vector3d& v ) 
    {
		return (v.dotProduct(v,n)) + dist;
    }
};

//////////////////////////////////////////////
bool isSame(float a, float b);
//check is Y value of the vertices is equal.
bool isYEqual(Vector3d& a, Vector3d& b);
//check is X and Z values of the vertices is equal.
bool isXandZEqual(Vector3d& a, Vector3d& b);
//////////////////////////////////////////////

//---------------------------------------------------------

struct tLine
{
public:
	Vector3d p1;//point 1
	Vector3d p2;//point 2
};

//Parameters:
//a - first point of the line
//b - second point of the line
//c - point to test against the line
//Return values:
//0 - on the line
//1 - left
//2 - right
int onLeftForWall(Vector3d& a, Vector3d& b, Vector3d& c);

//check is two lines collinear
bool isLinesCollinear(Vector3d& line1p1, Vector3d& line1p2,
						Vector3d& line2p1, Vector3d& line2p2);