#include "Camera.h"
//#include <Windows.h>
#include "GameEngine.h"
//#include "gl\glew.h"

// This is how fast our camera moves (Sped up a bit due to normalizing our vectors)
#define kSpeed	50.0f									

//This returns a perpendicular vector from 2 given vectors by taking the cross product.											
Vector3d Cross(Vector3d vVector1, Vector3d vVector2)
{
	Vector3d vNormal;	

	// Calculate the cross product with the non communitive equation
	vNormal.x = ((vVector1.y * vVector2.z) - (vVector1.z * vVector2.y));
	vNormal.y = ((vVector1.z * vVector2.x) - (vVector1.x * vVector2.z));
	vNormal.z = ((vVector1.x * vVector2.y) - (vVector1.y * vVector2.x));

	// Return the cross product
	return vNormal;										 
}

//This returns the magnitude of a vector
float Magnitude(Vector3d vNormal)
{
	// Here is the equation:  magnitude = sqrt(V.x^2 + V.y^2 + V.z^2) : Where V is the vector
	return (float)sqrt( (vNormal.x * vNormal.x) + 
						(vNormal.y * vNormal.y) + 
						(vNormal.z * vNormal.z) );
}

//This returns a normalize vector (A vector exactly of length 1)
Vector3d Normalize(Vector3d vVector)
{
	// Get the magnitude of our normal
	float magnitude = Magnitude(vVector);				

	// Now that we have the magnitude, we can divide our vector by that magnitude.
	// That will make our vector a total length of 1.  
	vVector = vVector / magnitude;		
	
	// Finally, return our normalized vector
	return vVector;										
}

CCamera::CCamera()
{
	Vector3d vZero = Vector3d(0.0, 0.0, 0.0);
	Vector3d vView = Vector3d(0.0, 1.0, 0.5);
	Vector3d vUp   = Vector3d(0.0, 0.0, 1.0);

	m_vPosition	= vZero;
	m_vView		= vView;
	m_vUpVector	= vUp;
}

//This function sets the camera's position and view and up vVector.
void CCamera::PositionCamera(float positionX, float positionY, float positionZ,
				  		     float viewX,     float viewY,     float viewZ,
							 float upVectorX, float upVectorY, float upVectorZ)
{
	Vector3d vPosition	= Vector3d(positionX, positionY, positionZ);
	Vector3d vView		= Vector3d(viewX, viewY, viewZ);
	Vector3d vUpVector	= Vector3d(upVectorX, upVectorY, upVectorZ);

	m_vPosition = vPosition;
	m_vView     = vView;
	m_vUpVector = vUpVector;
}

//This allows us to look around using the mouse, like in most first person games.
void CCamera::SetViewByMouse()
{
	POINT mousePos;									// This is a window structure that holds an X and Y
	int middleX = screenWidth  >> 1;				// This is a binary shift to get half the width
	int middleY = screenHeight >> 1;				// This is a binary shift to get half the height
	float angleY = 0.0f;							// This is the direction for looking up or down
	float angleZ = 0.0f;							// This will be the value we need to rotate around the Y axis (Left and Right)
	static float currentRotX = 0.0f;
	
	// Get the mouse's current X,Y position
	GetCursorPos(&mousePos);						

	// If our cursor is still in the middle, we never moved... so don't update the screen
	if( (mousePos.x == middleX) && (mousePos.y == middleY) ) return;

	// Set the mouse position to the middle of our window
	SetCursorPos(middleX, middleY);							

	// Get the direction the mouse moved in, but bring the number down to a reasonable amount
	angleY = (float)( (middleX - mousePos.x) ) / 500.0f;		
	angleZ = (float)( (middleY - mousePos.y) ) / 500.0f;
	
	static float lastRotX = 0.0f; 
 	lastRotX = currentRotX; // We store off the currentRotX and will use it in when the angle is capped
	
	// Here we keep track of the current rotation (for up and down) so that
	// we can restrict the camera from doing a full 360 loop.
	currentRotX += angleZ;
 
	// If the current rotation (in radians) is greater than 1.0, we want to cap it.
	if(currentRotX > 1.0f)     
	{
		currentRotX = 1.0f;
		
		// Rotate by remaining angle if there is any
		if(lastRotX != 1.0f) 
		{
			// To find the axis we need to rotate around for up and down
			// movements, we need to get a perpendicular vector from the
			// camera's view vector and up vector.  This will be the axis.
			// Before using the axis, it's a good idea to normalize it first.
			Vector3d vAxis = Cross(m_vView - m_vPosition, m_vUpVector);
			vAxis = Normalize(vAxis);
				
			// rotate the camera by the remaining angle (1.0f - lastRotX)
			RotateView( 1.0f - lastRotX, vAxis.x, vAxis.y, vAxis.z);
		}
	}
	// Check if the rotation is below -1.0, if so we want to make sure it doesn't continue
	else if(currentRotX < -1.0f)
	{
		currentRotX = -1.0f;
		
		// Rotate by the remaining angle if there is any
		if(lastRotX != -1.0f)
		{
			// To find the axis we need to rotate around for up and down
			// movements, we need to get a perpendicular vector from the
			// camera's view vector and up vector.  This will be the axis.
			// Before using the axis, it's a good idea to normalize it first.
			Vector3d vAxis = Cross(m_vView - m_vPosition, m_vUpVector);
			vAxis = Normalize(vAxis);
			
			// rotate the camera by ( -1.0f - lastRotX)
			RotateView( -1.0f - lastRotX, vAxis.x, vAxis.y, vAxis.z);
		}
	}
	// Otherwise, we can rotate the view around our position
	else 
	{	
		// To find the axis we need to rotate around for up and down
		// movements, we need to get a perpendicular vector from the
		// camera's view vector and up vector.  This will be the axis.
		// Before using the axis, it's a good idea to normalize it first.
		Vector3d vAxis = Cross(m_vView - m_vPosition, m_vUpVector);
		vAxis = Normalize(vAxis);
	
		// Rotate around our perpendicular axis
		RotateView(angleZ, vAxis.x, vAxis.y, vAxis.z);
	}

	// Always rotate the camera around the y-axis
	RotateView(angleY, 0, 1, 0);
}

//This rotates the view around the position using an axis-angle rotation
void CCamera::RotateView(float angle, float x, float y, float z)
{
	Vector3d vNewView;

	// Get the view vector (The direction we are facing)
	Vector3d vView = m_vView - m_vPosition;		

	// Calculate the sine and cosine of the angle once
	float cosTheta = (float)cos(angle);
	float sinTheta = (float)sin(angle);

	// Find the new x position for the new rotated point
	vNewView.x  = (cosTheta + (1 - cosTheta) * x * x)		* vView.x;
	vNewView.x += ((1 - cosTheta) * x * y - z * sinTheta)	* vView.y;
	vNewView.x += ((1 - cosTheta) * x * z + y * sinTheta)	* vView.z;

	// Find the new y position for the new rotated point
	vNewView.y  = ((1 - cosTheta) * x * y + z * sinTheta)	* vView.x;
	vNewView.y += (cosTheta + (1 - cosTheta) * y * y)		* vView.y;
	vNewView.y += ((1 - cosTheta) * y * z - x * sinTheta)	* vView.z;

	// Find the new z position for the new rotated point
	vNewView.z  = ((1 - cosTheta) * x * z - y * sinTheta)	* vView.x;
	vNewView.z += ((1 - cosTheta) * y * z + x * sinTheta)	* vView.y;
	vNewView.z += (cosTheta + (1 - cosTheta) * z * z)		* vView.z;

	// Now we just add the newly rotated vector to our position to set
	// our new rotated view of our camera.
	m_vView = m_vPosition + vNewView;
}

//This strafes the camera left and right depending on the speed (-/+)
void CCamera::StrafeCamera(float speed)
{	
	// Add the strafe vector to our position
	m_vPosition.x += m_vStrafe.x * speed;
	m_vPosition.z += m_vStrafe.z * speed;

	// Add the strafe vector to our view
	m_vView.x += m_vStrafe.x * speed;
	m_vView.z += m_vStrafe.z * speed;
}

/////// * /////////// * /////////// * NEW * /////// * /////////// * /////////// *


///////////////////////////////// MOVE CAMERA \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*
/////
/////	This will move the camera forward or backward depending on the speed
/////
///////////////////////////////// MOVE CAMERA \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*

void CCamera::MoveCamera(float speed)
{
	// Get the current view vector (the direction we are looking)
	Vector3d vVector = m_vView - m_vPosition;

	vVector = Normalize(vVector);

	if(USE_FRUSTUM == 1)
	{
		m_vPosition.x += vVector.x * speed;		// Add our acceleration to our position's X
		//m_vPosition.y += vVector.y * speed;		// Add our acceleration to our position's Y
		m_vPosition.z += vVector.z * speed;		// Add our acceleration to our position's Z
		m_vView.x += vVector.x * speed;			// Add our acceleration to our view's X
		//m_vView.y += vVector.y * speed;			// Add our acceleration to our view's Y
		m_vView.z += vVector.z * speed;			// Add our acceleration to our view's Z
	}
	else if(USE_FRUSTUM == 2)
	{
		m_vPosition.x += vVector.x * speed;		// Add our acceleration to our position's X
		m_vPosition.y += vVector.y * speed;		// Add our acceleration to our position's Y
		m_vPosition.z += vVector.z * speed;		// Add our acceleration to our position's Z
		m_vView.x += vVector.x * speed;			// Add our acceleration to our view's X
		m_vView.y += vVector.y * speed;			// Add our acceleration to our view's Y
		m_vView.z += vVector.z * speed;			// Add our acceleration to our view's Z
	}
}


/////// * /////////// * /////////// * NEW * /////// * /////////// * /////////// *

// The next 3 functions were added to our camera class.  The less code in 
// Main.cpp the better.

//////////////////////////// CHECK FOR MOVEMENT \\\\\\\\\\\\\\\\\\\\\\\\\\\\*
/////
/////	This function handles the input faster than in the WinProc()
/////
//////////////////////////// CHECK FOR MOVEMENT \\\\\\\\\\\\\\\\\\\\\\\\\\\\*

void CCamera::CheckForMovement()
{
	//if(keys[VK_UP])
	//{
	//	g_Camera.MoveCamera(kSpeed);
	//}
	//else if(keys[VK_DOWN])
	//{
	//	g_Camera.MoveCamera(-kSpeed);
	//}
	//if(keys[VK_LEFT])
	//{
	//	// We want to rotate around the Y axis so we pass in (0, 1, 0) for the axis
	//	//g_Camera.RotateView(kSpeed, 0, 1, 0);
	//	g_Camera.RotateAroundPoint(g_Camera.m_vView, kSpeed, 0, 1, 0);
	//}
	//else if(keys[VK_RIGHT])
	//{
	//	//g_Camera.RotateView(-kSpeed, 0, 1, 0);
	//	g_Camera.RotateAroundPoint(g_Camera.m_vView, -kSpeed, 0, 1, 0);
	//}

	float speed = kSpeed * g_FrameInterval;

	if(/*keys[VK_UP] ||*/ keys['W'])
	{
		MoveCamera(speed);
	}
	if(/*keys[VK_DOWN] ||*/ keys['S'])
	{
		MoveCamera(-speed);
	}
	if(/*keys[VK_LEFT] ||*/ keys['A'])
	{
		StrafeCamera(-speed);
		//this->RotateView(speed*0.03, 0, 1, 0);
	}
	if(/*keys[VK_RIGHT] ||*/ keys['D'])
	{
		StrafeCamera(speed);
		//this->RotateView(-speed*0.03, 0, 1, 0);
	}
}


///////////////////////////////// UPDATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*
/////
/////	This updates the camera's view and strafe vector
/////
///////////////////////////////// UPDATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*

void CCamera::Update() 
{
	// Below we calculate the strafe vector every time we update
	// the camera.  This is because many functions use it so we might
	// as well calculate it only once.  

	// Initialize a variable for the cross product result
	Vector3d vCross = Cross(m_vView - m_vPosition, m_vUpVector);

	// Normalize the strafe vector
	m_vStrafe = Normalize(vCross);

	// Move the camera's view by the mouse
	SetViewByMouse();

	// This checks to see if the keyboard was pressed
	CheckForMovement();
}


///////////////////////////////// LOOK \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*
/////
/////	This updates the camera according to the 
/////
///////////////////////////////// LOOK \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*

void CCamera::Look()
{
	// Give openGL our camera position, then camera view, then camera up vector
	gluLookAt(m_vPosition.x, m_vPosition.y, m_vPosition.z,	
			  m_vView.x,	 m_vView.y,     m_vView.z,	
			  m_vUpVector.x, m_vUpVector.y, m_vUpVector.z);
}