#include "DrawFunctions.h"
#include "GameEngine.h"

void drawFloor()
{
	const int size = 50;

	for(int z = -200; z < 2*100; z+=100)
	{
		for(int x = -200; x < 2*100; x+=100)
		{
			glPushMatrix();
				glTranslatef(x,0,z);

				//glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D, g_Texture[ID_TEX_FLOOR]);
				//glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

				glBegin(GL_POLYGON);
					glNormal3f(0,1,0);
					glTexCoord2f(1.0, 0.0); glVertex3f(size, 0, size);
					glTexCoord2f(1.0, 1.0); glVertex3f(size, 0, -size);
					glTexCoord2f(0.0, 1.0); glVertex3f(-size, 0, -size);
					glTexCoord2f(0.0, 0.0); glVertex3f(-size, 0, size);
				glEnd();

				//glDisable(GL_BLEND);
				//glDisable(GL_TEXTURE_2D);
				
			glPopMatrix();
		}
	}
}

void drawRoom(vector<CRoom*>* rooms, int roomId)
{
	for(int indRoom = 0; indRoom < rooms->size(); indRoom++)
	{
		if(roomId == (*rooms)[indRoom]->getId())
		{
			(*rooms)[indRoom]->drawRoom();
		}
	}
}

void drawLeafNodeWithId(vector<CNode*>& vLeafNodes_, int id_)
{
	for(int indLeafNode = 0; indLeafNode < vLeafNodes_.size(); indLeafNode++)
	{
		if(id_ == vLeafNodes_[indLeafNode]->id)
		{
			//DRAW BOUNDING POLYGONS----------------------------------------------
			vector<tWall*>* polygons = vLeafNodes_[indLeafNode]->getDataPolys();

			for(int indPolygon = 0; indPolygon < polygons->size(); indPolygon++)
			{
				tPlane plane = (*polygons)[indPolygon]->getPlain();

				glBegin(GL_LINE_LOOP/*GL_QUADS*//*GL_POINTS*/);
					for(int indVertex = 0; indVertex < 4; indVertex++)
					{
						glVertex3f(plane.vertices[indVertex].x,
									plane.vertices[indVertex].y,
									plane.vertices[indVertex].z);
					}
				glEnd();
			}
			//-----------------------------------------------------------------------

			//DRAW OBJECTS-----------------------------------------------------------
			vector<Object3D*>* objects = vLeafNodes_[indLeafNode]->getObjects();
			for(int indObject = 0; indObject < (*objects).size(); indObject++)
			{
				(*objects)[indObject]->drawWithTransform();
			}
			//-----------------------------------------------------------------------
		}
	}
}

void drawRoomNode(vector<CNode*>& vRoomNodes_, int indRoomNode_)
{
	//given index of the room's node should be between 0 and number of rooms (rooms nodes) in scene
	if(indRoomNode_ >= 0 && indRoomNode_ < vRoomNodes_.size())
	{
		//get all polygons in the room node
		vector<tWall*>* polygons = vRoomNodes_[indRoomNode_]->getDataPolys();
		int numPolugons = polygons->size();

		//draw polygons of the room's node
		glBegin(GL_QUADS);
			for(int indPolygon = 0; indPolygon < numPolugons; indPolygon++)
			{
				tPlane plane = (*polygons)[indPolygon]->getPlain();

				for(int indVertex = 0; indVertex < 4; indVertex++)
				{
					glVertex3f(plane.vertices[indVertex].x,
								plane.vertices[indVertex].y,
								plane.vertices[indVertex].z);
				}
			}
		glEnd();
	}
}

//void drawSceneBB(tWall* wholeSceneBBplanes_)
//{
//	const int DIVIDER_FOR_NORMAL = 150;//to make normal shorter
//
//	glDisable(GL_LIGHTING);
//	glPointSize(9);
//	glLineWidth(6);
//	glColor3f(0,0,1);
//
//	for(int indPlane = 0; indPlane < 4; indPlane++)
//	{
//			glBegin(GL_LINE_LOOP/*GL_POINTS*/);
//			for(int indVertice = 0; indVertice < 4; indVertice++)
//			{
//				glVertex3f(wholeSceneBBplanes_[indPlane].getPlain().vertices[indVertice].x,
//							wholeSceneBBplanes_[indPlane].getPlain().vertices[indVertice].y,
//							wholeSceneBBplanes_[indPlane].getPlain().vertices[indVertice].z);
//			}
//			glEnd();
//
//			//Vector3d center((wholeSceneBBplanes_[indPlane].getPlain().vertices[0].x + wholeSceneBBplanes_[indPlane].getPlain().vertices[3].x) / 2,
//			//			(wholeSceneBBplanes_[indPlane].getPlain().vertices[0].y + wholeSceneBBplanes_[indPlane].getPlain().vertices[3].y) / 4,
//			//			(wholeSceneBBplanes_[indPlane].getPlain().vertices[0].z + wholeSceneBBplanes_[indPlane].getPlain().vertices[3].z) / 2);
//			//Vector3d normal(wholeSceneBBplanes_[indPlane].getPlain().normal / DIVIDER_FOR_NORMAL);
//
//			//glBegin(GL_LINES/*GL_POINTS*/);
//			//	glVertex3f(center.x, center.y, center.z);
//			//	glVertex3f(normal.x + center.x, normal.y + center.y, normal.z + center.z);
//			//glEnd();
//	}
//
//	glColor3f(1,1,1);
//	glLineWidth(1);
//	glPointSize(1);
//	glEnable(GL_LIGHTING);
//}
//
//void drawPortals(vector<CPortal>& portals, bool drawNormals)
//{
//	const int DIVIDER_FOR_NORMAL = 150;//to make normal shorter
//
//	glDisable(GL_LIGHTING);
//	glPointSize(9);
//	glLineWidth(6);
//	glColor3f(1,0,0);
//	//glEnable(GL_CULL_FACE);
//
//	for(int indPortal = 0; indPortal < portals.size(); indPortal++)
//	{
//		glBegin(GL_LINE_LOOP/*GL_QUADS*/);
//			for(int indVertice = 0; indVertice < 4; indVertice++)
//			{
//				glVertex3f(portals[indPortal].vertices[indVertice].x,
//							portals[indPortal].vertices[indVertice].y,
//							portals[indPortal].vertices[indVertice].z);
//			}
//		glEnd();
//		
//		if(drawNormals)
//		{
//			Vector3d center((portals[indPortal].vertices[0].x + portals[indPortal].vertices[3].x) / 2,
//						(portals[indPortal].vertices[0].y + portals[indPortal].vertices[3].y) / 4,
//						(portals[indPortal].vertices[0].z + portals[indPortal].vertices[3].z) / 2);
//			Vector3d normal(portals[indPortal].normal / DIVIDER_FOR_NORMAL);
//
//			glColor3f(1,0,0);
//			glBegin(GL_LINES);
//				glVertex3f(center.x, center.y, center.z);
//				glVertex3f(normal.x + center.x, normal.y + center.y, normal.z + center.z);
//			glEnd();
//		}
//	}
//	//glDisable(GL_CULL_FACE);
//	glColor3f(1,1,1);
//	glLineWidth(1);
//	glPointSize(1);
//	glEnable(GL_LIGHTING);
//}
//
//void drawAllWallsVertices(Vector3d* wallsVertices, int numVertices)
//{
//	for(int indWallVertice = 0; indWallVertice < numVertices; indWallVertice++)
//	{
//		glDisable(GL_LIGHTING);
//		glPointSize(9);
//		glLineWidth(6);
//		glColor3f(1,0,0);
//		glBegin(GL_POINTS);
//			glVertex3f(wallsVertices[indWallVertice].x,
//						wallsVertices[indWallVertice].y,
//						wallsVertices[indWallVertice].z);
//		glEnd();
//		glColor3f(1,1,1);
//		glLineWidth(1);
//		glPointSize(1);
//		glEnable(GL_LIGHTING);
//	}
//}
//
//void drawSceneQuads(vector<tPlane>& g_quads_)
//{
//	glDisable(GL_LIGHTING);
//	glPointSize(16);
//	glLineWidth(6);
//	glColor3f(1,0,0);
//
//	for(int indQuad = 0; indQuad < g_quads_.size(); indQuad++)
//	{
//		glBegin(GL_LINE_LOOP);
//			for(int indVertex = 0; indVertex < 4; indVertex++)
//			{
//				glVertex3f(g_quads_[indQuad].vertices[indVertex].x,
//							g_quads_[indQuad].vertices[indVertex].y,
//							g_quads_[indQuad].vertices[indVertex].z);
//			}
//		glEnd();
//	}
//
//	glColor3f(1,1,1);
//	glLineWidth(1);
//	glPointSize(1);
//	glEnable(GL_LIGHTING);
//}