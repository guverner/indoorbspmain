#pragma once
#include "cSceneManager.h"

void drawFloor();

//draw room with given id
void drawRoom(vector<CRoom*>* rooms, int roomId);

//draw data of the leaf node with given id
void drawLeafNodeWithId(vector<CNode*>& vLeafNodes_, int id_);

//draw room node with given index
void drawRoomNode(vector<CNode*>& vRoomNodes_, int indRoomNode_);

////draw bounding box of the whole scene
//void drawSceneBB(tWall* wholeSceneBBplanes_);
//
//void drawPortals(vector<CPortal>& portals, bool drawNormals);
//
//void drawAllWallsVertices(Vector3d* wallsVertices, int numVertices);