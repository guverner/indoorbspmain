#include "GameEngine.h"
#include "Image.h"

#pragma warning(disable:4244)
#pragma warning(disable:4305)

//GLOBAL VARIABLES//////////////////////////////////////
//win32 global variabless---------------
HDC			hDC=NULL;
HGLRC		hRC=NULL;
HWND		hWnd=NULL;
HINSTANCE	hInstance;
//--------------------------------------

char*		titleWindow = "";

ConsoleWindow	console;

bool	keys[256];
bool	active = true;
bool	isFullScreen = false;

int		mouse_x = 0, mouse_y = 0;
bool	LeftPressed = false;
int		screenWidth = 480, screenHeight = 480;

MSG		msg;			
bool	done=false;

bool g_RenderMode = true;
bool g_bTextures  = true;

CCamera g_Camera;
//CFrustum g_Frustum;

void (*pf_init)(void);
void (*pf_display)(void);
void (*pf_processKeys)(void);
void (*pf_update)(void);

CPoint3D centerScreenP = {0,0,0};

UINT g_Texture[MAX_TEXTURES] = {0};

//stores the elapsed time between the current and last frame
float g_FrameInterval = 0.0f;

OBJLoader objLoader;

//FOG-------------------
GLuint filter;                      // Which Filter To Use
GLuint fogMode[]= { GL_EXP, GL_EXP2, GL_LINEAR };   // Storage For Three Types Of Fog
GLuint fogfilter= 2;                    // Which Fog To Use
GLfloat fogColor[4]= {0.5f, 0.5f, 0.5f, 1.0f};      // Fog Color
//----------------------

//settings for reshape function and view frustum--
float g_fov;
float g_aspectRatio;
float g_zNearDist;
float g_zFarDist;
//------------------------------------------------

/////////////////////////////////////////////////////////

void regUpdateFunc(void (*pf)(void))
{
	pf_update = pf;
}

void regProcessKeysFunc(void (*pf)(void))
{
	pf_processKeys = pf;
}

void regDisplayFunc(void (*pf)(void))
{
	pf_display = pf;
}

void regInitFunc(void (*pf)(void))
{
	pf_init = pf;
}

bool initEngineWindow(char* title, int width, int height, int bits, bool isfullScreen_)
{
	console.Open();

	// Create Our OpenGL Window
	if (!CreateGLWindow(title, width, height, bits, isfullScreen_))
	{
		return false;									// Quit If Window Was Not Created
	}

	return true;
}

bool LockFrameRate(int frame_rate)
{
    static float lastTime = 0.0f;		// This will hold the time from the last frame
	static float frameTime = 0.0f;				// This stores the last frame's time

	// Get current time in seconds (milliseconds * .001 = seconds)
	float currentTime = timeGetTime() * 0.001f; 

	// Here we store the elapsed time between the current and last frame,
	// then keep the current frame in our static variable for the next frame.
 	g_FrameInterval = currentTime - frameTime;
	frameTime = currentTime;

	// Get the elapsed time by subtracting the current time from the last time
	// If the desired frame rate amount of seconds has passed -- return true (ie Blit())
	if((currentTime - lastTime) > (1.0f / frame_rate))
	{
		// Reset the last time
		lastTime = currentTime;

		return true;
	}

	return false;
}

int InitGL()
{
	glewInit();

	glClearColor(0.0f,0.0f,0.0f,1.0f);

	glShadeModel(GL_SMOOTH);

	////Fog Init--------------------------------------------
	////glFogi(GL_FOG_MODE, fogMode[fogfilter]);				// Fog Mode  //fogMode[fogfilter]
	////glFogfv(GL_FOG_COLOR, fogColor);            // Set Fog Color
	////glFogf(GL_FOG_DENSITY, 0.1f);              // How Dense Will The Fog Be
	////glHint(GL_FOG_HINT, GL_DONT_CARE);          // Fog Hint Value
	////glFogf(GL_FOG_START, 0.0f);					// Fog Start Depth
	////glFogf(GL_FOG_END, 700.0f);				// Fog End Depth
	////glEnable(GL_FOG);							// Enables GL_FOG
	////-----------------------------------------------------

	//Light 1----------------------------------------------
	//light position and colour
	GLfloat light_position[] = {5, 2.0, 0.0, 0.0};
	GLfloat white_light[] = {1.0,1.0,1.0,0.0};
	GLfloat no_light[] = {0.0,0.0,0.0,0.0};
	GLfloat diff_light[] = {1.0,1.0,1.0,0.0};//���������� ���������
	GLfloat spec_light[] = {1.0,1.0,1.0,0.0};
	glLightfv(GL_LIGHT0, GL_AMBIENT, no_light);//set color
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diff_light);//���������� ���������
	glLightfv(GL_LIGHT0, GL_SPECULAR, spec_light);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);//set position

	/*glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 2.0);
	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 1.0);
	glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.5);*/
	
	//set the reflectance factors for the material
	//ambient light
	GLfloat ambient[] = {0.3,0.3,0.3};
	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);

	//set the diffuse reflectance factors
	//diffuse material component (���������� ���������)
	GLfloat diff[] = {0.6,0.6,0.6};
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diff);

	//specular material component
	GLfloat WhiteSpec[] = {1,1,1};
	glMaterialfv(GL_FRONT, GL_SPECULAR, WhiteSpec);

	GLfloat shininess = 30;
	glMaterialf(GL_FRONT, GL_SHININESS, shininess);

	//ENABLE LIGHTING AND DEPTH TEST
	glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
	//-----------------------------------------------------

	glEnable(GL_DEPTH_TEST);

	//glEnable(GL_CULL_FACE);
	
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);	//mix texture and lighting

	return true;
}

void mainLoop()
{
	srand(GetTickCount());

	pf_init();

	while(!done)									// Loop That Runs While done=FALSE
	{
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	// Is There A Message Waiting?
		{
			if (msg.message==WM_QUIT)				// Have We Received A Quit Message?
			{
				done=true;							// If So done=TRUE
			}
			else										// If Not, Deal With Window Messages
			{
				TranslateMessage(&msg);				// Translate The Message
				DispatchMessage(&msg);				// Dispatch The Message
			}
		}
		else if(LockFrameRate(1000))					// If There Are No Messages and frame rate is ok
		{
			if (active)
			{
				if(keys[VK_ESCAPE])
				{
					done = true;
				}
				else
				{
					pf_processKeys();
					pf_update();
					pf_display();
					SwapBuffers(hDC);
				}

				LeftPressed = false;
			}

			if (keys[VK_F1])						// Is F1 Being Pressed?
			{
				keys[VK_F1]=FALSE;					// If So Make Key FALSE
				KillGLWindow();						// Kill Our Current Window
				isFullScreen=!isFullScreen;				// Toggle Fullscreen / Windowed Mode
				// Recreate Our OpenGL Window
				CreateGLWindow(titleWindow,1366,768,32,isFullScreen);
			}
		}
	}

	console.Close();
	KillGLWindow();
}

void reshape(int width, int height)		// Resize the OpenGL window
{
	if(height == 0)// Prevent A Divide By Zero By
	{
		height = 1;
	}

	screenWidth=width; screenHeight = height;           // to ensure the mouse coordinates match 
														// we will use these values to set the coordinate system

	glViewport(0,0,width,height);						// Reset the current viewport

	glMatrixMode(GL_PROJECTION);						// select the projection matrix stack
	glLoadIdentity();									// reset the top of the projection matrix to an identity matrix

	g_fov = FOV;
	g_aspectRatio = (GLfloat)width/(GLfloat)height;
	g_zNearDist = Z_NEAR_DIST;
	g_zFarDist = Z_FAR_DIST;
	gluPerspective(g_fov, g_aspectRatio, g_zNearDist, g_zFarDist);

	glMatrixMode(GL_MODELVIEW);							// Select the modelview matrix stack
	glLoadIdentity();									// Reset the top of the modelview matrix to an identity matrix
}

LRESULT CALLBACK WndProc(	HWND	hWnd,			// Handle For This Window
							UINT	uMsg,			// Message For This Window
							WPARAM	wParam,			// Additional Message Information
							LPARAM	lParam)			// Additional Message Information
{
	switch (uMsg)									// Check For Windows Messages
	{
		case WM_ACTIVATE:							// Watch For Window Activate Message
		{
			if (!HIWORD(wParam))					// Check Minimization State
			{
				active=TRUE;						// Program Is Active
			}
			else
			{
				active=FALSE;						// Program Is No Longer Active
			}

			return 0;								// Return To The Message Loop
		}

		case WM_SYSCOMMAND:							// Intercept System Commands
		{
			switch (wParam)							// Check System Calls
			{
				case SC_SCREENSAVE:					// Screensaver Trying To Start?
				case SC_MONITORPOWER:				// Monitor Trying To Enter Powersave?
				return 0;							// Prevent From Happening
			}
			break;									// Exit
		}

		case WM_CLOSE:								// Did We Receive A Close Message?
		{
			PostQuitMessage(0);						// Send A Quit Message
			return 0;								// Jump Back
		}
		break;

		case WM_SIZE:								// Resize The OpenGL Window
		{
			reshape(LOWORD(lParam),HIWORD(lParam));  // LoWord=Width, HiWord=Height
			return 0;								// Jump Back
		}
		break;

		case WM_LBUTTONDOWN:
			{
	            mouse_x = LOWORD(lParam);          
				mouse_y = screenHeight - HIWORD(lParam);

				////change mouse position to opengl coordinates
				//mouse_x = mouse_x - (screenWidth/2 - centerScreenP.x);
				//mouse_y = mouse_y - (screenHeight/2 - centerScreenP.y);

				LeftPressed = true;
			}
		break;

		case WM_LBUTTONUP:
			{
	            LeftPressed = false;
			}
		break;

		case WM_MOUSEMOVE:
			{
	            mouse_x = LOWORD(lParam);          
				mouse_y = screenHeight  - HIWORD(lParam);

				////change mouse position to opengl coordinates
				//mouse_x = mouse_x - (screenWidth/2 - centerScreenP.x);
				//mouse_y = mouse_y - (screenHeight/2 - centerScreenP.y);
			}
		break;
		case WM_KEYDOWN:							// Is A Key Being Held Down?
		{
			keys[wParam] = true;					// If So, Mark It As TRUE
			return 0;								// Jump Back
		}
		break;
		case WM_KEYUP:								// Has A Key Been Released?
		{
			keys[wParam] = false;					// If So, Mark It As FALSE
			return 0;								// Jump Back
		}
		break;
	}

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

bool CreateGLWindow(char* title, int width, int height, int bits, bool isfullScreen_)
{
	titleWindow = title;

	GLuint		PixelFormat;			// Holds The Results After Searching For A Match
	WNDCLASS	wc;						// Windows Class Structure
	DWORD		dwExStyle;				// Window Extended Style
	DWORD		dwStyle;				// Window Style
	RECT		WindowRect;				// Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left=(long)0;			// Set Left Value To 0
	WindowRect.right=(long)width;		// Set Right Value To Requested Width
	WindowRect.top=(long)0;				// Set Top Value To 0
	WindowRect.bottom=(long)height;		// Set Bottom Value To Requested Height

	isFullScreen = isfullScreen_;

	hInstance			= GetModuleHandle(NULL);				// Grab An Instance For Our Window
	wc.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraw On Size, And Own DC For Window.
	wc.lpfnWndProc		= (WNDPROC) WndProc;					// WndProc Handles Messages
	wc.cbClsExtra		= 0;									// No Extra Window Data
	wc.cbWndExtra		= 0;									// No Extra Window Data
	wc.hInstance		= hInstance;							// Set The Instance
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);			// Load The Default Icon
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	wc.hbrBackground	= NULL;									// No Background Required For GL
	wc.lpszMenuName		= NULL;									// We Don't Want A Menu
	wc.lpszClassName	= "OpenGL";								// Set The Class Name

	if (!RegisterClass(&wc))									// Attempt To Register The Window Class
	{
		MessageBox(NULL,"Failed To Register The Window Class.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;											// Return FALSE
	}
	
	const int MY_WIDTH_WINDOW = 1366;
	const int MY_WIDTH_HEIGHT = 768;

	if(isFullScreen)// Attempt Fullscreen Mode?
	{
		WindowRect.left=(long)0;			// Set Left Value To 0
		WindowRect.right=(long)MY_WIDTH_WINDOW;		// Set Right Value To Requested Width
		WindowRect.top=(long)0;				// Set Top Value To 0
		WindowRect.bottom=(long)MY_WIDTH_HEIGHT;		// Set Bottom Value To Requested Height

		DEVMODE dmScreenSettings;								// Device Mode
		memset(&dmScreenSettings,0,sizeof(dmScreenSettings));	// Makes Sure Memory's Cleared
		dmScreenSettings.dmSize=sizeof(dmScreenSettings);		// Size Of The Devmode Structure
		dmScreenSettings.dmPelsWidth	= MY_WIDTH_WINDOW;				// Selected Screen Width
		dmScreenSettings.dmPelsHeight	= MY_WIDTH_HEIGHT;				// Selected Screen Height
		dmScreenSettings.dmBitsPerPel	= bits;					// Selected Bits Per Pixel
		dmScreenSettings.dmFields=DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT;

		// Try To Set Selected Mode And Get Results.  NOTE: CDS_FULLSCREEN Gets Rid Of Start Bar.
		if (ChangeDisplaySettings(&dmScreenSettings,CDS_FULLSCREEN)!=DISP_CHANGE_SUCCESSFUL)
		{
			// If The Mode Fails, Offer Two Options.  Quit Or Use Windowed Mode.
			if (MessageBox(NULL,"The Requested Fullscreen Mode Is Not Supported By\nYour Video Card. Use Windowed Mode Instead?","NeHe GL",MB_YESNO|MB_ICONEXCLAMATION)==IDYES)
			{
				isFullScreen=FALSE;		// Windowed Mode Selected.  Fullscreen = FALSE
			}
			else
			{
				// Pop Up A Message Box Letting User Know The Program Is Closing.
				MessageBox(NULL,"Program Will Now Close.","ERROR",MB_OK|MB_ICONSTOP);
				return FALSE;									// Return FALSE
			}
		}
	}

	if (isFullScreen)												// Are We Still In Fullscreen Mode?
	{
		dwExStyle=WS_EX_APPWINDOW;								// Window Extended Style
		dwStyle=WS_POPUP;										// Windows Style
		ShowCursor(FALSE);										// Hide Mouse Pointer
	}
	else
	{
		dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// Window Extended Style
		dwStyle=WS_OVERLAPPEDWINDOW;							// Windows Style
	}
	
	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);		// Adjust Window To True Requested Size

	// Create The Window
	if (!(hWnd=CreateWindowEx(	dwExStyle,							// Extended Style For The Window
								"OpenGL",							// Class Name
								titleWindow,						// Window Title
								dwStyle |							// Defined Window Style
								WS_CLIPSIBLINGS |					// Required Window Style
								WS_CLIPCHILDREN,					// Required Window Style
								0, 0,								// Window Position
								WindowRect.right-WindowRect.left,	// Calculate Window Width
								WindowRect.bottom-WindowRect.top,	// Calculate Window Height
								NULL,								// No Parent Window
								NULL,								// No Menu
								hInstance,							// Instance
								NULL)))								// Dont Pass Anything To WM_CREATE
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Window Creation Error.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	static	PIXELFORMATDESCRIPTOR pfd=				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		bits,										// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		16,											// 16Bit Z-Buffer (Depth Buffer)  
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};
	
	//������� �������� ����������
	if (!(hDC=GetDC(hWnd)))							// Did We Get A Device Context?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Device Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	//������������ �� ������� ���� ������ ��������
	//(����� �������� �������� ������� ��������)
	if (!(PixelFormat=ChoosePixelFormat(hDC,&pfd)))	// Did Windows Find A Matching Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Find A Suitable PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	//���������� ������ �������� � �������� ����������
	if(!SetPixelFormat(hDC,PixelFormat,&pfd))		// Are We Able To Set The Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Set The PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	//�������� ��������� ���������������
	if (!(hRC=wglCreateContext(hDC)))				// Are We Able To Get A Rendering Context?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Rendering Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	//���������� ������� ����� �������� ��������������� ������
	if(!wglMakeCurrent(hDC,hRC))					// Try To Activate The Rendering Context
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Activate The GL Rendering Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	ShowWindow(hWnd,SW_SHOW);						// Show The Window
	SetForegroundWindow(hWnd);						// Slightly Higher Priority
	SetFocus(hWnd);									// Sets Keyboard Focus To The Window

	if(!isFullScreen)
		reshape(width, height);					// Set Up Our Perspective GL Screen
	else
		reshape(MY_WIDTH_WINDOW, MY_WIDTH_HEIGHT);
	
	if (!InitGL())									// Initialize Our Newly Created GL Window
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Initialization Failed.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}

	return true;
}

void KillGLWindow()								// Properly Kill The Window
{
	if (isFullScreen)										// Are We In Fullscreen Mode?
	{
		ChangeDisplaySettings(NULL,0);					// If So Switch Back To The Desktop
		ShowCursor(TRUE);								// Show Mouse Pointer
	}

	if (hRC)											// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL,NULL))					// Are We Able To Release The DC And RC Contexts?
		{
			MessageBox(NULL,"Release Of DC And RC Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(hRC))						// Are We Able To Delete The RC?
		{
			MessageBox(NULL,"Release Rendering Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}
		hRC=NULL;										// Set RC To NULL
	}

	if (hDC && !ReleaseDC(hWnd,hDC))					// Are We Able To Release The DC
	{
		MessageBox(NULL,"Release Device Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hDC=NULL;										// Set DC To NULL
	}

	if (hWnd && !DestroyWindow(hWnd))					// Are We Able To Destroy The Window?
	{
		MessageBox(NULL,"Could Not Release hWnd.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hWnd=NULL;										// Set hWnd To NULL
	}

	if (!UnregisterClass("OpenGL",hInstance))			// Are We Able To Unregister Class
	{
		MessageBox(NULL,"Could Not Unregister Class.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hInstance=NULL;									// Set hInstance To NULL
	}
}

void engine_LoadImage(char* file, GLuint* texture_)
{
	// Texture loading object
	nv::Image imgObj;

	if(file == NULL)
	{
		file = "noSprite.png";
	}

	// Return true on success
	if(imgObj.loadImageFromFile(file))
	{
		glGenTextures(1, texture_);
		glBindTexture(GL_TEXTURE_2D, *texture_);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
		glTexImage2D(GL_TEXTURE_2D, 0, imgObj.getInternalFormat(), imgObj.getWidth(), imgObj.getHeight(), 0, imgObj.getFormat(), imgObj.getType(), imgObj.getLevel(0));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
	}
	else
	{
		MessageBox(NULL, "Failed to load texture", "End of the world", MB_OK | MB_ICONINFORMATION);
	}
}

void load3DModel(char* file, ThreeDModel& theResult)
{
	if(file == NULL)
	{
		//cout << "file = NULL" << endl;
		return;
	}

	cout << " loading model " << endl;
	if(objLoader.loadModel(file, theResult))//returns true if the model is loaded, puts the model in the model parameter
	{
		cout << " model loaded " << endl;		

		//if you want to translate the object to the origin of the screen,
		//first calculate the centre of the object, then move all the vertices
		//back so that the centre is on the origin.
		theResult.calcCentrePoint();
		theResult.centreOnZero();
	
		theResult.calcVertNormalsUsingOctree();  //the method will construct the octree if it hasn't already been created.
				

		//turn on VBO by setting useVBO to true in 3dmodel.cpp default constructor - only permitted on 8 series cards and higher
		if(!theResult.useImmediateMode || theResult.useVBO)
		{
			theResult.initDrawElements();
		}
		if(theResult.useVBO)
		{
			theResult.initVBO();
			theResult.deleteVertexFaceData();
		}
	}
	else
	{
		cout << " model failed to load " << endl;
	}
}

//void load3DModel(char* file, t3DModel& theResult)
//{
//
//}

bool CreateTexture(UINT &texture, LPSTR strFileName)
{
	AUX_RGBImageRec *pImage = NULL;
	FILE *pFile = NULL;

	if(!strFileName) 
		return false;

	// Open a file pointer to the BMP file and check if it was found and opened 
	if((pFile = fopen(strFileName, "rb")) == NULL) 
	{
		// Display an error message saying the file was not found, then return NULL
		MessageBox(hWnd, "Unable to load BMP File!", "Error", MB_OK);
		return NULL;
	}

	// Load the bitmap using the aux function stored in glaux.lib
	pImage = auxDIBImageLoad(strFileName);				

	// Make sure valid image data was given to pImage, otherwise return false
	if(pImage == NULL)								
		return false;

	// Generate a texture with the associative texture ID stored in the array
	glGenTextures(1, &texture);

	// This sets the alignment requirements for the start of each pixel row in memory.
	glPixelStorei (GL_UNPACK_ALIGNMENT, 1);

	// Bind the texture to the texture arrays index and init the texture
	glBindTexture(GL_TEXTURE_2D, texture);
	
	// Build Mipmaps (builds different versions of the picture for distances - looks better)
	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, pImage->sizeX, 
					  pImage->sizeY, GL_RGB, GL_UNSIGNED_BYTE, pImage->data);

	//Assign the mip map levels and texture info
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	// Now we need to free the image data that we loaded since openGL stored it as a texture

	if (pImage)										// If we loaded the image
	{
		if (pImage->data)							// If there is texture data
		{
			free(pImage->data);						// Free the texture data, we don't need it anymore
		}

		free(pImage);								// Free the image structure
	}

	// Return a success
	return true;
}

//bool CreateTexture(UINT &texture, LPSTR strFileName)
//{
//	if(!strFileName) 
//		return false;
//
//	// Define a pointer to a tImage
//	tImage *pImage = NULL;
//
//	// If the file is a jpeg, load the jpeg and store the data in pImage
//	if(strstr(strFileName, ".jpg"))
//	{
//		pImage = LoadJPG(strFileName);
//	}
//	// If the file is a tga, load the tga and store the data in pImage
//	else if(strstr(strFileName, ".tga"))
//	{
//		pImage = LoadTGA(strFileName);
//	}
//	// If the file is a bitmap, load the bitmap and store the data in pImage
//	else if(strstr(strFileName, ".bmp"))
//	{
//		pImage = LoadBMP(strFileName);
//	}
//	// Else we don't support the file format that is trying to be loaded
//	else
//		return false;
//
//	// Make sure valid image data was given to pImage, otherwise return false
//	if(pImage == NULL)								
//		return false;
//
//	// Generate a texture with the associative texture ID stored in the array
//	glGenTextures(1, &texture);
//
//	// This sets the alignment requirements for the start of each pixel row in memory.
//	glPixelStorei (GL_UNPACK_ALIGNMENT, 1);
//
//	// Bind the texture to the texture arrays index and init the texture
//	glBindTexture(GL_TEXTURE_2D, texture);
//
//	// Assume that the texture is a 24 bit RGB texture (We convert 16-bit ones to 24-bit)
//	int textureType = GL_RGB;
//
//	// If the image is 32-bit (4 channels), then we need to specify GL_RGBA for an alpha
//	if(pImage->channels == 4)
//		textureType = GL_RGBA;
//		
//	// Build Mipmaps (builds different versions of the picture for distances - looks better)
//	gluBuild2DMipmaps(GL_TEXTURE_2D, pImage->channels, pImage->sizeX, 
//					  pImage->sizeY, textureType, GL_UNSIGNED_BYTE, pImage->data);
//
//	//Assign the mip map levels and texture info
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
//	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
//
//	// Now we need to free the image data that we loaded since openGL stored it as a texture
//
//	if (pImage)										// If we loaded the image
//	{
//		if (pImage->data)							// If there is texture data
//		{
//			free(pImage->data);						// Free the texture data, we don't need it anymore
//		}
//
//		free(pImage);								// Free the image structure
//	}
//
//	return true;
//}

void CreateTexture(UINT textureArray[], LPSTR strFileName, int textureID)
{
	AUX_RGBImageRec *pBitmap = NULL;
	
	if(!strFileName)									// Return from the function if no file name was passed in
		return;

	pBitmap = auxDIBImageLoad(strFileName);				// Load the bitmap and store the data
	
	if(pBitmap == NULL)									// If we can't load the file, quit!
		exit(0);

	// Generate a texture with the associative texture ID stored in the array
	glGenTextures(1, &textureArray[textureID]);

	// This sets the alignment requirements for the start of each pixel row in memory.
	glPixelStorei (GL_UNPACK_ALIGNMENT, 1);

	// Bind the texture to the texture arrays index and init the texture
	glBindTexture(GL_TEXTURE_2D, textureArray[textureID]);

	// Build Mipmaps (builds different versions of the picture for distances - looks better)
	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, pBitmap->sizeX, pBitmap->sizeY, GL_RGB, GL_UNSIGNED_BYTE, pBitmap->data);

	// Lastly, we need to tell OpenGL the quality of our texture map.  GL_LINEAR_MIPMAP_LINEAR
	// is the smoothest.  GL_LINEAR_MIPMAP_NEAREST is faster than GL_LINEAR_MIPMAP_LINEAR, 
	// but looks blochy and pixilated.  Good for slower computers though.  
		
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

	// Now we need to free the bitmap data that we loaded since openGL stored it as a texture

	if (pBitmap)										// If we loaded the bitmap
	{
		if (pBitmap->data)								// If there is texture data
		{
			free(pBitmap->data);						// Free the texture data, we don't need it anymore
		}

		free(pBitmap);									// Free the bitmap structure
	}
}