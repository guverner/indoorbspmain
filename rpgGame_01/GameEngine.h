#pragma once

#include "Settings.h"
#include "CVVMath.h"
#include "my3Dstruct.h"
using namespace std;

//win32 global variabless
extern HDC			hDC;		// Private GDI Device Context
extern HGLRC		hRC;		// Permanent Rendering Context
extern HWND			hWnd;		// Holds Our Window Handle
extern HINSTANCE	hInstance;		// Holds The Instance Of The Application

extern char*		titleWindow;

extern ConsoleWindow	console;//console window variable

extern bool				keys[256];
extern bool				active;
extern bool				isFullScreen;

extern int				mouse_x, mouse_y;
extern bool				LeftPressed;
extern int				screenWidth, screenHeight;

extern MSG		msg;				// Windows Message Structure
extern bool		done;				// Bool Variable To Exit Loop

extern CPoint3D centerScreenP;

extern float g_FrameInterval;

// This tells us if we want to render the textures (used later for just lightmaps)
extern bool g_bTextures;
extern bool g_RenderMode;

extern OBJLoader objLoader;

extern CCamera	g_Camera;
//extern CFrustum g_Frustum;

extern GLuint filter;                      // Which Filter To Use
extern GLuint fogMode[];   // Storage For Three Types Of Fog
extern GLuint fogfilter;                    // Which Fog To Use
extern GLfloat fogColor[4];      // Fog Color

//settings for reshape function and view frustum
extern float g_fov;
extern float g_aspectRatio;
extern float g_zNearDist;
extern float g_zFarDist;

extern void (*pf_init)(void);
extern void (*pf_display)(void);
extern void (*pf_processKeys)(void);
extern void (*pf_update)(void);

//Engine Abstraction functions---------------------------------
bool initEngineWindow(char* title, int width, int height, int bits, bool isfullScreen_);
int InitGL();
void mainLoop();
void regInitFunc(void (*pf)(void));
void regDisplayFunc(void (*pf)(void));
void regProcessKeysFunc(void (*pf)(void));
void regUpdateFunc(void (*pf)(void));

bool LockFrameRate(int frame_rate);
//-------------------------------------------------------------

//for terrain textures ans skybox------------------------------
#define MAX_TEXTURES 1000								// The maximum amount of textures to load

extern UINT g_Texture[MAX_TEXTURES];					// This stores the texture IDs

// We need to define this for glTexParameteri()
#define GL_CLAMP_TO_EDGE	0x812F						// This is for our skybox textures
//-------------------------------------------------------------

//WIN32 Processes function - useful for responding to user inputs or other events.
LRESULT CALLBACK WndProc(	HWND	hWnd,			// Handle For This Window
							UINT	uMsg,			// Message For This Window
							WPARAM	wParam,			// Additional Message Information
							LPARAM	lParam);		// Additional Message Information
void KillGLWindow();// Properly Kill The Window
/*	This Code Creates Our OpenGL Window.  Parameters Are:					*
 *	title			- Title To Appear At The Top Of The Window				*
 *	width			- Width Of The GL Window Or Fullscreen Mode				*
 *	height			- Height Of The GL Window Or Fullscreen Mode			*/
bool CreateGLWindow(char* title, int width, int height, int bits, bool isfullScreen_);
void reshape(int width, int height);//called when the window is resized

void engine_LoadImage(char* file, GLuint* texture_);

struct CColor3
{
	float r, g, b;
};

void load3DModel(char* file, ThreeDModel& theResult);
void load3DModel(char* file, t3DModel& theResult);

//This creates a texture in OpenGL that we can texture map
bool CreateTexture(UINT &texture, LPSTR strFileName);

void CreateTexture(UINT textureArray[], LPSTR strFileName, int textureID);