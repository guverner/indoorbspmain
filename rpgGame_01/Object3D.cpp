#include "Object3D.h"

Object3D::Object3D(char* file_, float posX, float posY, float posZ)
{
	load3DModel(file_, m_model);

	m_center.x = m_model.theBBox.centrePoint.x + posX;
	m_center.y = m_model.theBBox.centrePoint.y + posY;
	m_center.z = m_model.theBBox.centrePoint.z + posZ;

	m_spin = 0;

	//initialise vertices array--------------------------
	//	3----2
	//	|	 |
	//	0----1

	//Bottom plane of the 3d Object
	//0
	vertices[0].x = (m_center.x - m_model.theBBox.boxWidthX/2);
	vertices[0].y = (m_center.y - m_model.theBBox.boxWidthY/2);
	vertices[0].z = (m_center.z + m_model.theBBox.boxWidthZ/2);
	//1
	vertices[1].x = (m_center.x + m_model.theBBox.boxWidthX/2);
	vertices[1].y = (m_center.y - m_model.theBBox.boxWidthY/2);
	vertices[1].z = (m_center.z + m_model.theBBox.boxWidthZ/2);
	//2
	vertices[2].x = (m_center.x + m_model.theBBox.boxWidthX/2);
	vertices[2].y = (m_center.y - m_model.theBBox.boxWidthY/2);
	vertices[2].z = (m_center.z - m_model.theBBox.boxWidthZ/2);
	//3
	vertices[3].x = m_center.x - m_model.theBBox.boxWidthX/2;
	vertices[3].y = m_center.y - m_model.theBBox.boxWidthY/2;
	vertices[3].z = m_center.z - m_model.theBBox.boxWidthZ/2;
	//Top plane of the 3d Object
	//4
	vertices[4].x = (m_center.x - m_model.theBBox.boxWidthX/2);
	vertices[4].y = (m_center.y + m_model.theBBox.boxWidthY/2);
	vertices[4].z = (m_center.z + m_model.theBBox.boxWidthZ/2);
	//5
	vertices[5].x = (m_center.x + m_model.theBBox.boxWidthX/2);
	vertices[5].y = (m_center.y + m_model.theBBox.boxWidthY/2);
	vertices[5].z = (m_center.z + m_model.theBBox.boxWidthZ/2);
	//6
	vertices[6].x = (m_center.x + m_model.theBBox.boxWidthX/2);
	vertices[6].y = (m_center.y + m_model.theBBox.boxWidthY/2);
	vertices[6].z = (m_center.z - m_model.theBBox.boxWidthZ/2);
	//7
	vertices[7].x = m_center.x - m_model.theBBox.boxWidthX/2;
	vertices[7].y = m_center.y + m_model.theBBox.boxWidthY/2;
	vertices[7].z = m_center.z - m_model.theBBox.boxWidthZ/2;
	//---------------------------------------------------

	m_minP.x = this->m_center.x - this->m_model.theBBox.boxWidthX/2;
	m_minP.y = this->m_center.y - this->m_model.theBBox.boxWidthY/2;
	m_minP.z = this->m_center.z - this->m_model.theBBox.boxWidthZ/2;
	m_maxP.x = this->m_center.x + this->m_model.theBBox.boxWidthX/2;
	m_maxP.y = this->m_center.y + this->m_model.theBBox.boxWidthY/2;
	m_maxP.z = this->m_center.z + this->m_model.theBBox.boxWidthZ/2;
}

Object3D::Object3D(ThreeDModel& model_, float posX, float posY, float posZ)
{
	m_model = model_;

	m_center.x = m_model.theBBox.centrePoint.x + posX;
	m_center.y = m_model.theBBox.centrePoint.y + posY;
	m_center.z = m_model.theBBox.centrePoint.z + posZ;

	m_spin = 0;

	//initialise vertices array--------------------------
	//	3----2
	//	|	 |
	//	0----1

	//Bottom plane of the 3d Object
	//0
	vertices[0].x = (m_center.x - m_model.theBBox.boxWidthX/2);
	vertices[0].y = (m_center.y - m_model.theBBox.boxWidthY/2);
	vertices[0].z = (m_center.z + m_model.theBBox.boxWidthZ/2);
	//1
	vertices[1].x = (m_center.x + m_model.theBBox.boxWidthX/2);
	vertices[1].y = (m_center.y - m_model.theBBox.boxWidthY/2);
	vertices[1].z = (m_center.z + m_model.theBBox.boxWidthZ/2);
	//2
	vertices[2].x = (m_center.x + m_model.theBBox.boxWidthX/2);
	vertices[2].y = (m_center.y - m_model.theBBox.boxWidthY/2);
	vertices[2].z = (m_center.z - m_model.theBBox.boxWidthZ/2);
	//3
	vertices[3].x = m_center.x - m_model.theBBox.boxWidthX/2;
	vertices[3].y = m_center.y - m_model.theBBox.boxWidthY/2;
	vertices[3].z = m_center.z - m_model.theBBox.boxWidthZ/2;
	//Top plane of the 3d Object
	//4
	vertices[4].x = (m_center.x - m_model.theBBox.boxWidthX/2);
	vertices[4].y = (m_center.y + m_model.theBBox.boxWidthY/2);
	vertices[4].z = (m_center.z + m_model.theBBox.boxWidthZ/2);
	//5
	vertices[5].x = (m_center.x + m_model.theBBox.boxWidthX/2);
	vertices[5].y = (m_center.y + m_model.theBBox.boxWidthY/2);
	vertices[5].z = (m_center.z + m_model.theBBox.boxWidthZ/2);
	//6
	vertices[6].x = (m_center.x + m_model.theBBox.boxWidthX/2);
	vertices[6].y = (m_center.y + m_model.theBBox.boxWidthY/2);
	vertices[6].z = (m_center.z - m_model.theBBox.boxWidthZ/2);
	//7
	vertices[7].x = m_center.x - m_model.theBBox.boxWidthX/2;
	vertices[7].y = m_center.y + m_model.theBBox.boxWidthY/2;
	vertices[7].z = m_center.z - m_model.theBBox.boxWidthZ/2;
	//---------------------------------------------------

	m_minP.x = this->m_center.x - this->m_model.theBBox.boxWidthX/2;
	m_minP.y = this->m_center.y - this->m_model.theBBox.boxWidthY/2;
	m_minP.z = this->m_center.z - this->m_model.theBBox.boxWidthZ/2;
	m_maxP.x = this->m_center.x + this->m_model.theBBox.boxWidthX/2;
	m_maxP.y = this->m_center.y + this->m_model.theBBox.boxWidthY/2;
	m_maxP.z = this->m_center.z + this->m_model.theBBox.boxWidthZ/2;
}

void Object3D::draw(TypeDraw type_)
{
	switch(type_)
	{
	case NORMAL:
		m_model.speedDisplayFaceNormals();
		break;
	case BOUNDBOX:
		m_model.drawBoundingBox();
		break;
	case OCTREE:
		m_model.drawOctreeLeaves();
		break;
	case ALL:
		m_model.speedDisplayFaceNormals();
		m_model.drawBoundingBox();
		m_model.drawOctreeLeaves();
		break;
	}
}

void Object3D::updateVertices()
{
	//m_center.x = m_transfMatrix[12];
	//m_center.y = m_transfMatrix[13];
	//m_center.z = m_transfMatrix[14];

	//if we will look from 3rd person camera, then

	//	3----2
	//	|	 |
	//	0----1

		//Bottom plan eof the car
	vertices[0].x = m_transfMatrix[0] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[4] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[8] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[12];
	vertices[0].y = m_transfMatrix[1] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[5] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[9] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[13];
	vertices[0].z = m_transfMatrix[2] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[6] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[10] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[14];

	vertices[1].x = m_transfMatrix[0] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[4] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[8] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[12];
	vertices[1].y = m_transfMatrix[1] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[5] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[9] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[13];
	vertices[1].z = m_transfMatrix[2] * this->m_model.theBBox.boxWidthX/2 + 
					m_transfMatrix[6] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[10] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[14];
	
	vertices[2].x = m_transfMatrix[0] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[4] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[8] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[12];
	vertices[2].y = m_transfMatrix[1] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[5] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[9] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[13];
	vertices[2].z = m_transfMatrix[2] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[6] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[10] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[14];
	
	vertices[3].x = m_transfMatrix[0] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[4] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[8] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[12];
	vertices[3].y = m_transfMatrix[1] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[5] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[9] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[13];
	vertices[3].z = m_transfMatrix[2] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[6] * -this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[10] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[14];

	//Top plane of the car
	vertices[4].x = m_transfMatrix[0] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[4] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[8] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[12];
	vertices[4].y = m_transfMatrix[1] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[5] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[9] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[13];
	vertices[4].z = m_transfMatrix[2] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[6] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[10] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[14];

	vertices[5].x = m_transfMatrix[0] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[4] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[8] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[12];
	vertices[5].y = m_transfMatrix[1] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[5] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[9] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[13];
	vertices[5].z = m_transfMatrix[2] * this->m_model.theBBox.boxWidthX/2 + 
					m_transfMatrix[6] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[10] * this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[14];

	vertices[6].x = m_transfMatrix[0] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[4] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[8] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[12];
	vertices[6].y = m_transfMatrix[1] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[5] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[9] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[13];
	vertices[6].z = m_transfMatrix[2] * this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[6] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[10] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[14];

	vertices[7].x = m_transfMatrix[0] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[4] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[8] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[12];
	vertices[7].y = m_transfMatrix[1] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[5] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[9] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[13];
	vertices[7].z = m_transfMatrix[2] * -this->m_model.theBBox.boxWidthX/2 +
					m_transfMatrix[6] * this->m_model.theBBox.boxWidthY/2 +
					m_transfMatrix[10] * -this->m_model.theBBox.boxWidthZ/2 + m_transfMatrix[14];

	//for(int i = 0; i < 8; i++)
	//{
	//	if(vertices[i].isINF())
	//		cout << "Object3D inf " << i << endl;
	//}
}

void Object3D::drawWithTransform()
{
	//Transformation of the object
	glPushMatrix();
		glLoadIdentity();
		glTranslatef(this->m_center.x, this->m_center.y, this->m_center.z);
		glRotatef(m_spin, 0, 1, 0);

		glGetFloatv(GL_MODELVIEW_MATRIX, this->m_transfMatrix);

		this->updateVertices();
	glPopMatrix();

	//draw object
	glPushMatrix();
		glTranslatef(this->m_center.x, this->m_center.y, this->m_center.z);
		glRotatef(m_spin, 0, 1, 0);
		this->draw(NORMAL);
		//this->draw(BOUNDBOX);
		//this->draw(ALL);
	glPopMatrix();
}

void Object3D::setCenter(float x_, float y_, float z_)
{
	this->m_center.x = x_;
	this->m_center.y = y_;
	this->m_center.z = z_;

	m_minP.x = this->m_center.x - this->m_model.theBBox.boxWidthX/2;
	m_minP.y = this->m_center.y - this->m_model.theBBox.boxWidthY/2;
	m_minP.z = this->m_center.z - this->m_model.theBBox.boxWidthZ/2;
	m_maxP.x = this->m_center.x + this->m_model.theBBox.boxWidthX/2;
	m_maxP.y = this->m_center.y + this->m_model.theBBox.boxWidthY/2;
	m_maxP.z = this->m_center.z + this->m_model.theBBox.boxWidthZ/2;
}

bool Object3D::isCollideBoundBox(Object3D* obj_)
{
	if(this->m_maxP.y >= obj_->m_minP.y && obj_->m_maxP.y >= this->m_minP.y &&
		this->m_maxP.x >= obj_->m_minP.x && obj_->m_maxP.x >= this->m_minP.x &&
		this->m_maxP.z >= obj_->m_minP.z && obj_->m_maxP.z >= this->m_minP.z)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Object3D::isCollideBBandP(Vector3d& point_)
{
	if(point_.x >= this->m_minP.x && point_.x <= this->m_maxP.x &&
		point_.y >= this->m_minP.y && point_.y <= this->m_maxP.y &&
		point_.z >= this->m_minP.z && point_.z <= this->m_maxP.z)
	{
		return true;
	}
	else
	{
		return false;
	}
}