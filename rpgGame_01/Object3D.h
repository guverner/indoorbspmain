#pragma once
#include "GameEngine.h"

class Object3D
{
public:
	ThreeDModel m_model;
	Vector3d m_center;
	Vector3d vertices[8];//vertices of the bounding box
	float m_transfMatrix[16];
	Vector3d m_minP;//min point ob the bounding box
	Vector3d m_maxP;//max point ob the bounding box
	float m_spin;

	Object3D(char* file_ = NULL, float posX = 0, float posY = 0, float posZ = 0);
	Object3D(ThreeDModel& model_, float posX, float posY, float posZ);
	void draw(TypeDraw type_);
	void drawWithTransform();
	void updateVertices();
	void setCenter(float x_, float y_, float z_);
	bool isCollideBoundBox(Object3D* obj_);
	bool isCollideBBandP(Vector3d& point_);
};