#ifndef _QUAKE3BSP_H
#define _QUAKE3BSP_H

#include "Structures\Vector3d.h"
#include "Structures\Vector2d.h"

// This is the number that is associated with a face that is of type "polygon"
#define FACE_POLYGON	1

struct tVector3i
{
	int x, y, z;
};


//BSP header structure
struct tBSPHeader
{
    char strID[4];				// This should always be 'IBSP'
    int version;				// This should be 0x2e for Quake 3 files
}; 


//BSP lump structure
struct tBSPLump
{
	int offset;					// The offset into the file for the start of this lump
	int length;					// The length in bytes for this lump
};


//BSP vertex structure
struct tBSPVertex
{
    Vector3d vPosition;			// (x, y, z) position. 
    Vector2d vTextureCoord;		// (u, v) texture coordinate
    Vector2d vLightmapCoord;	// (u, v) lightmap coordinate
    Vector3d vNormal;			// (x, y, z) normal vector
    byte color[4];				// RGBA color for the vertex 
};


//BSP face structure
struct tBSPFace
{
    int textureID;				// The index into the texture array 
    int effect;					// The index for the effects (or -1 = n/a) 
    int type;					// 1=polygon, 2=patch, 3=mesh, 4=billboard 
    int startVertIndex;			// The starting index into this face's first vertex 
    int numOfVerts;				// The number of vertices for this face 
    int startIndex;				// The starting index into the indices array for this face
    int numOfIndices;			// The number of indices for this face
    int lightmapID;				// The texture index for the lightmap 
    int lMapCorner[2];			// The face's lightmap corner in the image 
    int lMapSize[2];			// The size of the lightmap section 
    Vector3d lMapPos;			// The 3D origin of lightmap. 
    Vector3d lMapVecs[2];		// The 3D space for s and t unit vectors. 
    Vector3d vNormal;			// The face normal. 
    int size[2];				// The bezier patch dimensions. 
};


//BSP texture structure
struct tBSPTexture
{
    char strName[64];			// The name of the texture w/o the extension 
    int flags;					// The surface flags (unknown) 
    int contents;				// The content flags (unknown)
};


//lumps enumeration
enum eLumps
{
    kEntities = 0,				// Stores player/object positions, etc...
    kTextures,					// Stores texture information
    kPlanes,				    // Stores the splitting planes
    kNodes,						// Stores the BSP nodes
    kLeafs,						// Stores the leafs of the nodes
    kLeafFaces,					// Stores the leaf's indices into the faces
    kLeafBrushes,				// Stores the leaf's indices into the brushes
    kModels,					// Stores the info of world models
    kBrushes,					// Stores the brushes info (for collision)
    kBrushSides,				// Stores the brush surfaces info
    kVertices,					// Stores the level vertices
    kIndices,					// Stores the level indices
    kShaders,					// Stores the shader files (blending, anims..)
    kFaces,						// Stores the faces for the level
    kLightmaps,					// Stores the lightmaps for the level
    kLightVolumes,				// Stores extra world lighting information
    kVisData,					// Stores PVS and cluster info (visibility)
    kMaxLumps					// A constant to store the number of lumps
};


// This is our bitset class for storing which face has already been drawn.
// The bitset functionality isn't really taken advantage of in this version
// since we aren't rendering by leafs and nodes.
class CBitset 
{

public:

	// Initialize all the data members
    CBitset() : m_bits(0), m_size(0) {}

	// This is our deconstructor
	~CBitset() 
	{
		// If we have valid memory, get rid of it
		if(m_bits) 
		{
			delete m_bits;
			m_bits = NULL;
		}
	}

	// This resizes our bitset to a size so each face has a bit associated with it
	void Resize(int count) 
	{ 
		// Get the size of integers we need
		m_size = count/32 + 1;

		// Make sure we haven't already allocated memory for the bits
        if(m_bits) 
		{
			delete m_bits;
			m_bits = 0;
		}

		// Allocate the bits and initialize them
		m_bits = new unsigned int[m_size];
		ClearAll();
	}

	// This does the binary math to set the desired bit
	void Set(int i) 
	{
		m_bits[i >> 5] |= (1 << (i & 31));
	}

	// This returns if the desired bit slot is a 1 or a 0
	int On(int i) 
	{
		return m_bits[i >> 5] & (1 << (i & 31 ));
	}

	// This clears a bit to 0
	void Clear(int i) 
	{
		m_bits[i >> 5] &= ~(1 << (i & 31));
	}

	// This initializes the bits to 0
	void ClearAll() 
	{
		memset(m_bits, 0, sizeof(unsigned int) * m_size);
	}

private:

	// Our private bit data that holds the bits and size
	unsigned int *m_bits;
	int m_size;
};

class CQuake3BSP
{

public:
	CQuake3BSP();
	~CQuake3BSP();

	bool LoadBSP(const char *strFileName);

	void RenderLevel(const Vector3d &vPos);

	void Destroy();

private:
	// This attaches the correct extension to the file name, if found
	void FindTextureExtension(char *strFileName);

	// This renders a single face to the screen
	void RenderFace(int faceIndex);

	int  m_numOfVerts;			// The number of verts in the model
	int  m_numOfFaces;			// The number of faces in the model
	int  m_numOfIndices;		// The number of indices for the model
	int  m_numOfTextures;		// The number of texture maps

	int			*m_pIndices;	// The object's indices for rendering
	tBSPVertex  *m_pVerts;		// The object's vertices
	tBSPFace	*m_pFaces;		// The faces information of the object
								// The texture and lightmap array for the level
	UINT m_textures[MAX_TEXTURES];	
								
	CBitset m_FacesDrawn;		// The bitset for the faces that have/haven't been drawn
};
#endif