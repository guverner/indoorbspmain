#pragma once

//----------------------------------------
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "glaux.lib")
#pragma comment(lib, "libfreetype.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "nvImage.lib")
#pragma comment(lib, "jpegbuild.lib")
#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "jpeg.lib")
//----------------------------------------

//----------------------------------------
#define SCREEN_WIDTH			800
#define SCREEN_HEIGHT			600
#define IS_FULL_SCREEN			true
#define DRAW_PORTALS_NORMALS	true

//for reshaping and main view-frustum
#define FOV						70.0f
#define Z_NEAR_DIST				0.1f
#define Z_FAR_DIST				1000.0f

//Textures
#define ID_TEX_FLOOR			0

//which frustum to use
//it can be 1 or 2
//if 1, then standard view frustum will be used
//if 2, then NOT standard view frustum will be used
#define USE_FRUSTUM				2
//----------------------------------------

//----------------------------------------
#include <windows.h>		// Header file for Windows
#include "Image_Loading/nvImage.h"
//#include <gl\gl.h>			// Header file for the OpenGL32 Library
//#include <gl\glu.h>			// Header file for the GLu32 Library
#include "gl\glew.h"
#include <gl\glaux.h>
#include <vector>
#include "console.h"		// Header file for Console
#include <cmath>
#include "freetype.h"		// Header for font library.
using namespace freetype;
#include "TypesForGame.h"
#include "3D\Vector3D.h"
#include "3D\Vector4D.h"
//MODEL LOADING--
#include "3DStruct\3dModel.h"
#include "Obj\OBJLoader.h"
//---------------
#include "MatrixRoutines.h"
#include "Quaternion.h"
#include <string>
#include <sstream>
#include "Camera.h"
#include "Obj.h"
//----------------------------------------