#include "GameEngine.h"

int Height(BYTE *pHeightMap, int X, int Y)
{
	int x = X % MAP_SIZE;					// Error check our x value
	int y = Y % MAP_SIZE;					// Error check our y value

	if(!pHeightMap) return 0;				// Make sure our data is valid

	return pHeightMap[x + (y * MAP_SIZE)];	// Index into our height array and return the height
}

float Height(BYTE *pHeightMap, float X, float Z)
{
	float dx = X - (int)X;
	float dz = Z - (int)Z;

	// Q12	  R2	Q22
	//	|-----------|
	//	|			|
	//	|	  .P	|
	//	|			|
	//	------------
	// Q11	  R1	Q21

	Vector3d Q11((int)X, Height(pHeightMap, (int)X, (int)Z), (int)Z);
	Vector3d Q21((int)X + 1, Height(pHeightMap, (int)X + 1, (int)Z), (int)Z);
	Vector3d Q22((int)X + 1, Height(pHeightMap, (int)X + 1, (int)Z + 1), (int)Z + 1);
	Vector3d Q12((int)X, Height(pHeightMap, (int)X, (int)Z + 1), (int)Z + 1);

	Vector3d R1(X, 0, Q11.z);
	R1.y = ((Q21.x - R1.x) / (Q21.x - Q11.x)) * Q11.y +
		((R1.x - Q11.x) / (Q21.x - Q11.x)) * Q21.y;

	Vector3d R2(X, 0, Q12.z);
	R2.y = ((Q21.x - R1.x) / (Q21.x - Q11.x)) * Q12.y +
		((R1.x - Q11.x) / (Q21.x - Q11.x)) * Q22.y;

	Vector3d P(X, 0, Z);
	P.y = ((Q22.z - P.z) / (Q22.z - Q11.z)) * R1.y +
		((P.z - Q11.z) / (Q22.z - Q11.z)) * R2.y;

	return P.y;
}

//This sets the current texture coordinate of the terrain, based on the X and Z
void SetTextureCoord(float x, float z)
{
	// Give OpenGL the current texture coordinate for our height map
	glTexCoord2f(   (float)x / (float)MAP_SIZE,	
				  - (float)z / (float)MAP_SIZE	);
}

void SetVertexColor(BYTE *pHeightMap, int x, int y)
{
	if(!pHeightMap) return;					// Make sure our height data is valid

	// Here we set the color for a vertex based on the height index.
	// To make it darker, I start with -0.15f.  We also get a ratio
	// of the color from 0 to 1.0 by dividing the height by 256.0f;
	float fColor = -0.15f + (Height(pHeightMap, x, y ) / 256.0f);

	// Assign this green shade to the current vertex
	glColor3f(0, fColor, 0 );
}



void RenderHeightMap(BYTE pHeightMap[])
{
	int X = 0, Y = 0;						// Create some variables to walk the array with.
	int x, y, z;							// Create some variables for readability
	bool bSwitchSides = false;

	// Make sure our height data is valid
	if(!pHeightMap) return;		

	// Bind the terrain texture to our terrain
	glBindTexture(GL_TEXTURE_2D, g_Texture[0]);

	/*glBegin(GL_TRIANGLES);*/
		for ( X = 0; X <= (MAP_SIZE-STEP_SIZE); X += STEP_SIZE )
		{
			for ( Y = 0; Y <= (MAP_SIZE-STEP_SIZE); Y += STEP_SIZE )
			{
				float	vertex1[3];
				float	vertex2[3];
				float	vertex3[3];
				float	vertex4[3];

				// get vertex data------
					// Get The (X, Y, Z) Value For The Bottom Left Vertex
					vertex1[0] = X;							
					vertex1[1] = Height(pHeightMap, X, Y ); ;	
					vertex1[2] = Y;
					// Get The (X, Y, Z) Value For The Top Left Vertex
					vertex2[0] = X;										
					vertex2[1] = Height(pHeightMap, X, Y + STEP_SIZE );  
					vertex2[2] = Y + STEP_SIZE;
					// Get The (X, Y, Z) Value For The Top Right Vertex
					vertex3[0] = X + STEP_SIZE; 
					vertex3[1] = Height(pHeightMap, X + STEP_SIZE, Y + STEP_SIZE ); 
					vertex3[2] = Y + STEP_SIZE;
					// Get The (X, Y, Z) Value For The Bottom Right Vertex
					vertex4[0] = X + STEP_SIZE; 
					vertex4[1] = Height(pHeightMap, X + STEP_SIZE, Y ); 
					vertex4[2] = Y;
				//-----------------------                      
 
				//Vectors----------------
					Vector3d leftVect(vertex2[0] - vertex1[0], vertex2[1] - vertex1[1], vertex2[2] - vertex1[2]);
					leftVect.normalize();
					Vector3d topVect(vertex3[0] - vertex2[0], vertex3[1] - vertex2[1], vertex3[2] - vertex2[2]);
					topVect.normalize();
					Vector3d rightVect(vertex4[0] - vertex3[0], vertex4[1] - vertex3[1], vertex4[2] - vertex3[2]);
					rightVect.normalize();
					Vector3d bottomVect(vertex1[0] - vertex4[0], vertex1[1] - vertex4[1], vertex1[2] - vertex4[2]);
					bottomVect.normalize();
					Vector3d middleVect(vertex3[0] - vertex1[0], vertex3[1] - vertex1[1], vertex3[2] - vertex1[2]);
					middleVect.normalize();
				//-----------------------

				////Find normals for vertices----
				//	Vector3d norm11(0,0,0);
				//	Vector3d norm12(0,0,0);

				//	if(X == 0 || X == (MAP_SIZE-STEP_SIZE))
				//	{
				//		if(Y == 0 || Y == (MAP_SIZE-STEP_SIZE))
				//		{
				//			norm11 = middleVect * leftVect;
				//			norm11.normalize();
				//			norm12 = -bottomVect * middleVect;
				//			norm12.normalize();
				//		}
				//		else
				//		{
				//		}
				//	}
				//	else
				//	{
				//	}
				////------------------------------
				//
				//Vector3d finalNorm1((norm11.x + norm12.x)/2, (norm11.y + norm12.y)/2, (norm11.z + norm12.z)/2);

				//Find normals for triangles----
					//For triangle 1----
					Vector3d normalTriangle1((-leftVect) * topVect);
					normalTriangle1.normalize();
					//------------------
					//For triangle 2----
					Vector3d normalTriangle2((-rightVect) * bottomVect);
					normalTriangle2.normalize();
					//------------------
				//------------------------------

				glBegin(GL_TRIANGLES);
				//Triangle 1-------------
				glNormal3f(-normalTriangle1.x, -normalTriangle1.y, -normalTriangle1.z);

				SetTextureCoord(vertex1[0], vertex1[2]);
				glVertex3i(vertex1[0], vertex1[1], vertex1[2]);                 
         
				SetTextureCoord(vertex2[0], vertex2[2]);
				glVertex3i(vertex2[0], vertex2[1], vertex2[2]);
 
				SetTextureCoord(vertex3[0], vertex3[2]);
				glVertex3i(vertex3[0], vertex3[1], vertex3[2]);
				//----------------------- 
 
				//Triangle 2-------------
				glNormal3f(-normalTriangle2.x, -normalTriangle2.y, -normalTriangle2.z);

				SetTextureCoord(vertex3[0], vertex3[2]);
				glVertex3i(vertex3[0], vertex3[1], vertex3[2]);

				SetTextureCoord(vertex4[0], vertex4[2]);
				glVertex3i(vertex4[0], vertex4[1], vertex4[2]);

				SetTextureCoord(vertex1[0], vertex1[2]);
				glVertex3i(vertex1[0], vertex1[1], vertex1[2]);  
				//-----------------------
				glEnd();
			}
		}
	/*glEnd();*/
}

void LoadRawFile(LPSTR strName, int nSize, BYTE *pHeightMap, Vector3d** normalsAr_)
{
	//CreateNormalsArray(pHeightMap, normalsAr_);

	FILE *pFile = NULL;

	// Let's open the file in Read/Binary mode.
	pFile = fopen( strName, "rb" );

	// Check to see if we found the file and could open it
	if ( pFile == NULL )	
	{
		// Display our error message and stop the function
		MessageBox(NULL, "Can't find the height map!", "Error", MB_OK);
		return;
	}

	// Here we load the .raw file into our pHeightMap data array.
	// We are only reading in '1', and the size is the (width * height)
	fread( pHeightMap, 1, nSize, pFile );

	// After we read the data, it's a good idea to check if everything read fine.
	int result = ferror( pFile );

	// Check if we received an error.
	if (result)
	{
		MessageBox(NULL, "Can't get data!", "Error", MB_OK);
	}

	// Close the file.
	fclose(pFile);
}

//void CreateNormalsArray(BYTE *pHeightMap, Vector3d** normalsAr_)
//{
//	int X = 0, Y = 0;						// Create some variables to walk the array with.
//	int x, y, z;							// Create some variables for readability
//
//	// Make sure our height data is valid
//	if(!pHeightMap) return;
//
//	int ind = 0;
//
//	for ( X = 0; X < (MAP_SIZE-STEP_SIZE); X += STEP_SIZE )
//	{
//		for ( Y = 0; Y < (MAP_SIZE-STEP_SIZE); Y += STEP_SIZE )
//		{
//			if(X == 0)
//			{
//				if(Y == 0)
//				{
//				}
//				else if(Y == )
//				{
//				}
//			}
//			else
//			{
//			}
//
//			//	2			3
//			//	|-----------|
//			//	|			|
//			//	|			|
//			//	|			|
//			//	|			|
//			//	-------------
//			//	1			4
//
//			float	vertex1[3];
//			float	vertex2[3];
//			float	vertex3[3];
//			float	vertex4[3];
//
//			// get vertex data------
//				// Get The (X, Y, Z) Value For The Bottom Left Vertex
//				vertex1[0] = X;							
//				vertex1[1] = Height(pHeightMap, X, Y ); ;	
//				vertex1[2] = Y;
//				// Get The (X, Y, Z) Value For The Top Left Vertex
//				vertex2[0] = X;										
//				vertex2[1] = Height(pHeightMap, X, Y + STEP_SIZE );  
//				vertex2[2] = Y + STEP_SIZE;
//				// Get The (X, Y, Z) Value For The Top Right Vertex
//				vertex3[0] = X + STEP_SIZE; 
//				vertex3[1] = Height(pHeightMap, X + STEP_SIZE, Y + STEP_SIZE ); 
//				vertex3[2] = Y + STEP_SIZE;
//				// Get The (X, Y, Z) Value For The Bottom Right Vertex
//				vertex4[0] = X + STEP_SIZE; 
//				vertex4[1] = Height(pHeightMap, X + STEP_SIZE, Y ); 
//				vertex4[2] = Y;
//			//-----------------------                      
// 
//			Vector3d vector21(vertex1[0] - vertex2[0], vertex1[1] - vertex2[1], vertex1[2] - vertex2[2]);
//			vector21.normalize();
//			Vector3d vector23(vertex3[0] - vertex2[0], vertex3[1] - vertex2[1], vertex3[2] - vertex2[2]);
//			vector23.normalize();
//			Vector3d vector43(vertex3[0] - vertex4[0], vertex3[1] - vertex4[1], vertex3[2] - vertex4[2]);
//			vector43.normalize();
//			Vector3d vector41(vertex1[0] - vertex4[0], vertex1[1] - vertex4[1], vertex1[2] - vertex4[2]);
//			vector41.normalize();
//
//			//Calculate normal of triangle 1
//			Vector3d normalTri1(vector21 * vector23);
//			normalTri1.normalize();
//
//			//Calculate normal of triangle 2
//			Vector3d normalTri2(vector43 * vector41);
//			normalTri2.normalize();
//
//			Vector3d normalsArray[2];
//			normalsArray[0] = normalTri1;
//			normalsArray[1] = normalTri2;
//
//			normalsAr_[ind] = normalsArray;
//			ind++;
//
//			////Triangle 1-------------
//			//SetTextureCoord(vertex1[0], vertex1[2]);
//			//glVertex3i(vertex1[0], vertex1[1], vertex1[2]);                 
//   //      
//			//SetTextureCoord(vertex2[0], vertex2[2]);
//			//glVertex3i(vertex2[0], vertex2[1], vertex2[2]);
// 
//			//SetTextureCoord(vertex3[0], vertex3[2]);
//			//glVertex3i(vertex3[0], vertex3[1], vertex3[2]);
//			////----------------------- 
// 
//			////Triangle 2-------------
//			//SetTextureCoord(vertex3[0], vertex3[2]);
//			//glVertex3i(vertex3[0], vertex3[1], vertex3[2]);
//
//			//SetTextureCoord(vertex4[0], vertex4[2]);
//			//glVertex3i(vertex4[0], vertex4[1], vertex4[2]);
//
//			//SetTextureCoord(vertex1[0], vertex1[2]);
//			//glVertex3i(vertex1[0], vertex1[1], vertex1[2]);  
//			////----------------------- 
//		}
//	}
//}