#ifndef _TERRAIN_H
#define _TERRAIN_H

#define MAP_SIZE		2048				// This is the size of our .raw height map
#define STEP_SIZE		16		// This is width and height of each QUAD
#define HEIGHT_RATIO	1.5f				// This is the ratio that the Y is scaled according to the X and Z

// This returns the height (0 to 255) from a heightmap given an X and Y
int Height(BYTE *pHeightMap, int X, int Y);

float Height(BYTE *pHeightMap, float X, float Z);

// This loads a .raw file of a certain size from the file
void LoadRawFile(LPSTR strName, int nSize, BYTE *pHeightMap, Vector3d** normalsAr_);

// This turns heightmap data into primitives and draws them to the screen
void RenderHeightMap(BYTE *pHeightMap);

//Create normals array for height map for each triangle
void CreateNormalsArray(BYTE *pHeightMap, Vector3d** normalsAr_);

#endif