#include "TypesForGame.h"

CRect2D::CRect2D(float posX_, float posY_, float width_, float height_)
{
	m_width = width_;
	m_height = height_;

	m_translX = posX_;
	m_translY = posY_;
	m_angle = 0;

	for(int i = 0; i < 16; i++)
	{
		m_transformMatrix[i] = 0;
	}

	m_texture = 0;
}

CRect2D::CRect2D(float posX_, float posY_, const char* file, float width_, float height_)
{
	m_translX = posX_;
	m_translY = posY_;
	m_angle = 0;

	for(int i = 0; i < 16; i++)
	{
		m_transformMatrix[i] = 0;
	}

	// Texture loading object
	nv::Image img;

	if(file != "")
	{
		// Return true on success
		if(img.loadImageFromFile(file))
		{
			glGenTextures(1, &m_texture);
			glBindTexture(GL_TEXTURE_2D, m_texture);
			glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
			glTexImage2D(GL_TEXTURE_2D, 0, img.getInternalFormat(), img.getWidth(), img.getHeight(), 0, img.getFormat(), img.getType(), img.getLevel(0));
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
		}
		else
		{
			MessageBox(NULL, "Failed to load texture", "End of the world", MB_OK | MB_ICONINFORMATION);
		}
	}
	else
	{
		m_texture = 0;
	}

	if(width_ == -1 && height_ == -1 && m_texture != 0)
	{
		m_width = img.getWidth();
		m_height = img.getHeight();
	}
	else if(m_texture != 0)
	{
		m_width = width_;
		m_height = height_;
	}
	else//input only position
	{
		const float size = 10;
		m_width = size;
		m_height = size;
	}
}

void CRect2D::draw()
{
	if(m_texture != 0)
	{
		glPushMatrix();
			glTranslated(m_translX,m_translY,0);
			glRotated(m_angle, 0, 0, 1);
			glGetFloatv(GL_MODELVIEW_MATRIX, this->m_transformMatrix);

			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, m_texture);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			glBegin(GL_POLYGON);
				glTexCoord2f(0.0, 0.0); glVertex2f(-m_width/2,-m_height/2);
				glTexCoord2f(0.0, 1.0); glVertex2f(-m_width/2,m_height/2);
				glTexCoord2f(1.0, 1.0); glVertex2f(m_width/2,m_height/2);
				glTexCoord2f(1.0, 0.0); glVertex2f(m_width/2,-m_height/2);
			glEnd();

			glDisable(GL_TEXTURE_2D);
			glDisable(GL_BLEND);
		glPopMatrix();
	}
	else
	{
		glPushMatrix();
			glTranslated(m_translX,m_translY,0);
			glGetFloatv(GL_MODELVIEW_MATRIX, this->m_transformMatrix);

			glBegin(GL_LINE_LOOP);
				glVertex2d(-m_width/2,-m_height/2);
				glVertex2d(-m_width/2,m_height/2);
				glVertex2d(m_width/2,m_height/2);
				glVertex2d(m_width/2,-m_height/2);
			glEnd();
		glPopMatrix();
	}
}

CPoint3D* CRect2D::updateVertices()
{
	CPoint3D* vertArray = new CPoint3D[4];

	//bottom left
	vertArray[0].x = m_transformMatrix[0]*-m_width/2 + m_transformMatrix[4]*-m_height/2 + m_transformMatrix[8]*0 + m_transformMatrix[12];
	vertArray[0].y = m_transformMatrix[1]*-m_width/2 + m_transformMatrix[5]*-m_height/2 + m_transformMatrix[9]*0 + m_transformMatrix[13];
	//top left
	vertArray[1].x = m_transformMatrix[0]*-m_width/2 + m_transformMatrix[4]*m_height/2 + m_transformMatrix[8]*0 + m_transformMatrix[12];
	vertArray[1].y = m_transformMatrix[1]*-m_width/2 + m_transformMatrix[5]*m_height/2 + m_transformMatrix[9]*0 + m_transformMatrix[13];
	//top right
	vertArray[2].x = m_transformMatrix[0]*m_width/2 + m_transformMatrix[4]*m_height/2 + m_transformMatrix[8]*0 + m_transformMatrix[12];
	vertArray[2].y = m_transformMatrix[1]*m_width/2 + m_transformMatrix[5]*m_height/2 + m_transformMatrix[9]*0 + m_transformMatrix[13];
	//bottom right
	vertArray[3].x = m_transformMatrix[0]*m_width/2 + m_transformMatrix[4]*-m_height/2 + m_transformMatrix[8]*0 + m_transformMatrix[12];
	vertArray[3].y = m_transformMatrix[1]*m_width/2 + m_transformMatrix[5]*-m_height/2 + m_transformMatrix[9]*0 + m_transformMatrix[13];

	return vertArray;
}

void CRect2D::engine_LoadImageToRect(const char* file)
{
	// Texture loading object
	nv::Image img;

	GLuint texture;

	// Return true on success
	if(img.loadImageFromFile(file))
	{
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
		glTexImage2D(GL_TEXTURE_2D, 0, img.getInternalFormat(), img.getWidth(), img.getHeight(), 0, img.getFormat(), img.getType(), img.getLevel(0));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
	}
	else
	{
		MessageBox(NULL, "Failed to load texture", "End of the world", MB_OK | MB_ICONINFORMATION);
	}

	this->m_texture = texture;
}

//CIRCLE///////
CCircle2D::CCircle2D(const CPoint3D& center_, const char* file, float radius_, float posX_, float posY_)
{
	m_center = center_;
	m_angle = 0;

	for(int i = 0; i < 16; i++)
	{
		m_transformMatrix[i] = 0;
	}

	// Texture loading object
	nv::Image img;

	if(file == "")
	{
		m_texture = 0;
	}
	else
	{
		// Return true on success
		if(img.loadImageFromFile(file))
		{
			glGenTextures(1, &m_texture);
			glBindTexture(GL_TEXTURE_2D, m_texture);
			glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
			glTexImage2D(GL_TEXTURE_2D, 0, img.getInternalFormat(), img.getWidth(), img.getHeight(), 0, img.getFormat(), img.getType(), img.getLevel(0));
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
		}
		else
		{
			MessageBox(NULL, "Failed to load texture", "End of the world", MB_OK | MB_ICONINFORMATION);
		}

		if(radius_ != -1 && m_texture == 0)
		{
			m_radius = radius_;
		}
		else
		{
			m_radius = img.getWidth()/2.0f;
		}
	}
}

CCircle2D::CCircle2D(float centerX_, float centerY_, const char* file, float radius_, float posX_, float posY_)
{
	m_center.x = centerX_;
	m_center.y = centerY_;
	m_angle = 0;

	m_translX = posX_;
	m_translY = posY_;

	for(int i = 0; i < 16; i++)
	{
		m_transformMatrix[i] = 0;
	}

	// Texture loading object
	nv::Image img;

	if(file == "")
	{
		m_texture = 0;
	}
	else
	{
		// Return true on success
		if(img.loadImageFromFile(file))
		{
			glGenTextures(1, &m_texture);
			glBindTexture(GL_TEXTURE_2D, m_texture);
			glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
			glTexImage2D(GL_TEXTURE_2D, 0, img.getInternalFormat(), img.getWidth(), img.getHeight(), 0, img.getFormat(), img.getType(), img.getLevel(0));
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
		}
		else
		{
			MessageBox(NULL, "Failed to load texture", "End of the world", MB_OK | MB_ICONINFORMATION);
		}
	}

	if(radius_ != -1 && m_texture == 0)
	{
		m_radius = radius_;
	}
	else
	{
		m_radius = img.getWidth()/2.0f;
	}
}

void CCircle2D::draw()
{
	if(m_texture == 0)
	{
		glPushMatrix();
			glTranslated(m_translX,m_translY,0);
			glRotated(m_angle, 0, 0, 1);
			glGetFloatv(GL_MODELVIEW_MATRIX, this->m_transformMatrix);

			glBegin(GL_LINE_LOOP);
				for(float i=0; i<360; i+=0.005) 
				{
					float xcoord = this->m_center.x + this->m_radius * cos(i*(PI/180));
					float ycoord = this->m_center.y + this->m_radius * sin(i*(PI/180));
					glVertex2f(xcoord, ycoord);
				}
			glEnd();
		glPopMatrix();
	}
	else
	{
		glPushMatrix();
			glTranslated(m_translX,m_translY,0);
			glRotated(m_angle, 0, 0, 1);
			glGetFloatv(GL_MODELVIEW_MATRIX, this->m_transformMatrix);

			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, m_texture);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			glBegin(GL_POLYGON);
				for(float i=0; i<360; i+=0.005) 
				{
					float xcoord = this->m_center.x + this->m_radius * cos(i*(PI/180));
					float ycoord = this->m_center.y + this->m_radius * sin(i*(PI/180));
					glTexCoord2f(0.5 + 0.5 * cos(i*(PI/180)), 0.5 + 0.5 * sin(i*(PI/180)));
					glVertex2f(xcoord, ycoord);
				}
			glEnd();

			glDisable(GL_TEXTURE_2D);
			glDisable(GL_BLEND);
		glPopMatrix();
	}
}

AABB::AABB()
{
	min.x = 0;	min.y = 0;	min.z = 0;
	max.x = 0;	max.y = 0;	max.z = 0;
}

AABB::AABB(Vector3D& min_, Vector3D& max_)
{
	min = min_;
	max = max_;
}