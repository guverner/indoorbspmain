#pragma once

#include "Settings.h"
#include "CVVMath.h"

struct CRect2D
{
	float m_width;
	float m_height;
	GLfloat m_transformMatrix[16];
	float m_translX, m_translY;
	float m_angle;
	GLuint m_texture;

public:
	CRect2D(float posX_, float posY_, float width_, float height_);
	CRect2D(float posX_ = 0, float posY_ = 0, const char* file = "", float width_ = -1, float height_ = -1);

	void draw();

	//multiply vertices to transformation matrix (need for checking collision)
	CPoint3D* updateVertices();

	void engine_LoadImageToRect(const char* file);
};

struct CCircle2D
{
	CPoint3D m_center;
	float m_radius;
	GLfloat m_transformMatrix[16];
	float m_translX, m_translY;
	float m_angle;
	GLuint m_texture;

public:
	CCircle2D(const CPoint3D& center_, const char* file = "", float radius_ = -1, float posX_ = 0, float posY_ = 0);
	CCircle2D(float centerX_, float centerY_, const char* file = "", float radius_ = -1, float posX_ = 0, float posY_ = 0);

	void draw();
};

class AABB
{
public:
	Vector3D min;
	Vector3D max;

	AABB();
	AABB(Vector3D& min_, Vector3D& max_);
	//void update(float* mat);
};