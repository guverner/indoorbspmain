#ifndef _MATRIX_ROUTINES_H
#define _MATRIX_ROUTINES_H

template <class T>
class MatrixRoutines
{
public:
	static void setToIdentity(T m[16])
	{
		for(int i =0; i<4; i++)
		{
			for(int j=0; j<4; j++)
			{
				if(i==j)
					m[i*4+j] = 1.0;
				else
					m[i*4+j] = 0.0;
			}
		}
	}
	static void setToIdentityWithPosition(T x, T y, T z, T m[16])
	{
		setToIdentity(m);

		m[12] = x;
		m[13] = y;
		m[14] = z;
	}
	static void multiplyPoint(T point[3], T matrix[16], T resultingPoint[4])
	{
		resultingPoint[0] = matrix[0]*point[0] + matrix[4]*point[1] + matrix[8]*point[2] + matrix[12];
		resultingPoint[1] = matrix[1]*point[0] + matrix[5]*point[1] + matrix[9]*point[2] + matrix[13];
		resultingPoint[2] = matrix[2]*point[0] + matrix[6]*point[1] + matrix[10]*point[2] + matrix[14];
		resultingPoint[3] = 1;
	}
};

#endif _MATRIX_ROUTINES_H