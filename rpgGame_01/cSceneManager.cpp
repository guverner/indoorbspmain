#include "cSceneManager.h"

cSceneManager::cSceneManager(int idScene)
{
	m_3dSceneModel = NULL;
	m_wallPolygonsBTree = NULL;
	m_idNodeWithCamera = 0;
	m_isRoomNodeWithCamera = false;

	choose3dSceneModel(idScene);//choose particular 3d model of the scene according to the given idScene
	initWallsWithPortals(m_3dSceneModel, m_vSceneWallPlygons);
	initPortals();

	//create binary tree with all polygons (walls)
	m_wallPolygonsBTree = new CBinaryTree(m_vSceneWallPlygons);

	initLeafNodesCollection(m_wallPolygonsBTree->root, m_vLeafNodes);

	initRoomNodesCollection(m_vLeafNodes, m_vRoomsNodes);

	initRooms();
}

cSceneManager::~cSceneManager()
{
	delete m_3dSceneModel;
	m_3dSceneModel = NULL;
	delete m_wallPolygonsBTree;
	m_wallPolygonsBTree = NULL;
}

void cSceneManager::set3dSceneModel(Object3D* object)
{
	m_3dSceneModel = object;
}

//Object3D* cSceneManager::get3dSceneModel()
//{
//	return m_3dSceneModel;
//}

vector<CNode*>* cSceneManager::getRoomsNodes()
{
	return &m_vRoomsNodes;
}

vector<CNode*>* cSceneManager::getLeafNodes()
{
	return &m_vLeafNodes;
}

void cSceneManager::choose3dSceneModel(int idScene)
{
	Object3D* objScene = NULL;//object which will contain 3d model of the entire scene

	switch(idScene)
	{
	case 1:
		objScene = new Object3D("room/room3.obj",0,20,0);
		objScene->m_model.translate(Vector3d(0,20,0));
		set3dSceneModel(objScene);
		break;
	case 2:
		objScene = new Object3D("room/room5.obj",0,25,0);
		objScene->m_model.translate(Vector3d(0,25,0));
		set3dSceneModel(objScene);
		break;
	case 3:
		objScene = new Object3D("room/room6.obj",0,25,0);
		objScene->m_model.translate(Vector3d(0,25,0));
		set3dSceneModel(objScene);
		break;
	default:
		objScene = new Object3D("room/room5.obj",0,25,0);
		objScene->m_model.translate(Vector3d(0,25,0));
		set3dSceneModel(objScene);
		break;
	}
}

void cSceneManager::findQuadVerts(Object3D* obj_, vector<tPlane>& quads_)
{
	for(int i = 0; i < obj_->m_model.numberOfVertices; i += 4)
	{
		tPlane quadVerts;

		quadVerts.vertices[0] = obj_->m_model.theVerts[i];
		quadVerts.vertices[1] = obj_->m_model.theVerts[i+1];
		quadVerts.vertices[2] = obj_->m_model.theVerts[i+2];
		quadVerts.vertices[3] = obj_->m_model.theVerts[i+3];
		
		//Normal can be found here!!! 

		quads_.push_back(quadVerts);
	}
}

bool cSceneManager::isPortal(tPlane quad)
{
	int numSimParY = 0;

	for(int i = 0; i < NUM_VERT_IN_PORTAL; i++)
	{
		switch(i)
		{
		case 0:
			if(quad.vertices[i].y == quad.vertices[1].y ||
				quad.vertices[i].y == quad.vertices[2].y ||
				quad.vertices[i].y == quad.vertices[3].y)
			{
				numSimParY++;
			}
			break;
		case 1:
			if(quad.vertices[i].y == quad.vertices[0].y ||
				quad.vertices[i].y == quad.vertices[2].y ||
				quad.vertices[i].y == quad.vertices[3].y)
			{
				numSimParY++;
			}
			break;
		case 2:
			if(quad.vertices[i].y == quad.vertices[1].y ||
				quad.vertices[i].y == quad.vertices[0].y ||
				quad.vertices[i].y == quad.vertices[3].y)
			{
				numSimParY++;
			}
			break;
		case 3:
			if(quad.vertices[i].y == quad.vertices[1].y ||
				quad.vertices[i].y == quad.vertices[2].y ||
				quad.vertices[i].y == quad.vertices[0].y)
			{
				numSimParY++;
			}
			break;
		}
	}

	if(numSimParY / 2 == 2)//numSimParY / 2 because pair of similar vertices in previous loop processed twice
	{
		return false;
	}
	else if(numSimParY / 2 == 1)
	{
		return true;
	}
	else
	{
		MessageBox(NULL,"Error in function - isPortal(CPortal)!","ERROR",MB_OK|MB_ICONEXCLAMATION);
	}
}

bool cSceneManager::isLeft(Vector3d& a, Vector3d& b, Vector3d& c)
{
	if(a.x == b.x)
	{
		return ((b.z - a.z)*(c.y - a.y) - (b.y - a.y)*(c.z - a.z)) > 0;
	}
	else
	{
		return ((b.x - a.x)*(c.y - a.y) - (b.y - a.y)*(c.x - a.x)) > 0;
	}


	/*Vector3d A(b - a);
	Vector3d B(c - a);
	double sa;

	if(a.x == b.x)
	{
		sa = A.z * B.y - B.z * A.y;
	}
	else
	{
		sa = A.x * B.y - B.x * A.y;
	}

	if(sa > 0)
	{
		return true;
	}
	else
	{
		return false;
	}*/
}

void cSceneManager::changeToAntiCWOrder(tPlane& plane)
{
	Vector3d lowest1;
	Vector3d lowest2;
	Vector3d upper1;
	Vector3d upper2;

	//find two lower vertices---------------------
		//find smallest y---------------
		float smallestY = plane.vertices[0].y;

		for(int indVert = 1; indVert < 4; indVert++)
		{
			if(plane.vertices[indVert].y < smallestY)
			{
				smallestY = plane.vertices[indVert].y;
			}
		}
		//------------------------------

		bool isPrevSmallestYAssigned = false;//is lowest1 vertice assigned
		bool isPrevUpperAssigned = false;//is upper1 vertice assigned
		//find two lower vertices
		for(int indVert = 0; indVert < 4; indVert++)
		{
			if(plane.vertices[indVert].y == smallestY)
			{
				if(!isPrevSmallestYAssigned)
				{
					lowest1 = plane.vertices[indVert];
					isPrevSmallestYAssigned = true;
				}
				else
				{
					lowest2 = plane.vertices[indVert];
				}
			}
			else
			{
				if(!isPrevUpperAssigned)
				{
					upper1 = plane.vertices[indVert];
					isPrevUpperAssigned = true;
				}
				else
				{
					upper2 = plane.vertices[indVert];
				}
			}
		}
	//----------------------------------------------

	//choose one lowest vertice and check against vertical vector it is on the left or not
	//if it is ont he left then we take this vertex, then another lovest,
	//then upper vertex which was in prev vertical line and last vertex. It will chencg order of the vertices.

	//Check against lowest1 vertice---------------
	Vector3d upperVertical;//check onLeft test with this vertice (firstly need to be find)
	Vector3d upperNotVertical;

		//find vertice with the same x and z to the lowest2--
		if(isXandZEqual(lowest2, upper1))
		{
			upperVertical = upper1;
			upperNotVertical = upper2;
		}
		else if(isXandZEqual(lowest2, upper2))
		{
			upperVertical = upper2;
			upperNotVertical = upper1;
		}
		//---------------------------------------------------

	if(isLeft(lowest2, upperVertical, lowest1))
	{
		plane.vertices[0] = lowest2;
		plane.vertices[1] = lowest1;
		plane.vertices[2] = upperNotVertical;
		plane.vertices[3] = upperVertical;
	}
	else
	{
		plane.vertices[0] = lowest1;
		plane.vertices[1] = lowest2;
		plane.vertices[2] = upperVertical;
		plane.vertices[3] = upperNotVertical;
	}
	//--------------------------------------------
}

void cSceneManager::findIndTwoVert(int indA_, int indB_, int& finalIndC_, int& finalIndD_)
{
	switch(indA_)
	{
	case 0:
		switch(indB_)
		{
		case 1:
			finalIndC_ = 2;
			finalIndD_ = 3;
			break;
		case 2:
			finalIndC_ = 1;
			finalIndD_ = 3;
			break;
		case 3:
			finalIndC_ = 1;
			finalIndD_ = 2;
			break;
		}
		break;
	case 1:
		switch(indB_)
		{
		case 0:
			finalIndC_ = 2;
			finalIndD_ = 3;
			break;
		case 2:
			finalIndC_ = 0;
			finalIndD_ = 3;
			break;
		case 3:
			finalIndC_ = 0;
			finalIndD_ = 2;
			break;
		}
		break;
	case 2:
		switch(indB_)
		{
		case 0:
			finalIndC_ = 1;
			finalIndD_ = 3;
			break;
		case 1:
			finalIndC_ = 0;
			finalIndD_ = 3;
			break;
		case 3:
			finalIndC_ = 0;
			finalIndD_ = 1;
			break;
		}
		break;
	case 3:
		switch(indB_)
		{
		case 0:
			finalIndC_ = 1;
			finalIndD_ = 2;
			break;
		case 1:
			finalIndC_ = 0;
			finalIndD_ = 2;
			break;
		case 2:
			finalIndC_ = 1;
			finalIndD_ = 3;
			break;
		}
		break;
	}
}

void cSceneManager::processQuadToWall(tPlane& quad_, bool isPrevPartPort_, tPortal& newPortal_, tPlane& newPolygon_)
{
	bool equalYTable[4][4];

	//initialise equality matrix of the vertice's y values
	for(int i = 0; i < 4; i++)//rows
	{
		for(int j = 0; j < 4; j++)//columns
		{
			if(j == i)
			{
				equalYTable[i][j] = false;
			}
			else
			{
				if(isYEqual(quad_.vertices[i],quad_.vertices[j]))
				{
					equalYTable[i][j] = true;
				}
				else
				{
					equalYTable[i][j] = false;
				}
			}
		}
	}

	Vector3d lowestVert1;
	Vector3d lowestVert2;
	Vector3d otherVert1;
	Vector3d otherVert2;

	int indLowestVert1 = 0;
	int indLowestVert2 = 0;
	int indOtherVert1 = 0;
	int indOtherVert2 = 0;

	//search true in equalYTable matrix
	for(int i = 0; i < 4; i++)//rows
	{
		for(int j = 0; j < 4; j++)//columns
		{
			if(equalYTable[i][j] == true)
			{
				lowestVert1 = quad_.vertices[i];
				lowestVert2 = quad_.vertices[j];

				indLowestVert1= i;
				indLowestVert2 = j;

				findIndTwoVert(i, j, indOtherVert1, indOtherVert2);

				otherVert1 = quad_.vertices[indOtherVert1];
				otherVert2 = quad_.vertices[indOtherVert2];

				goto cont;
				//break;
			}
		}
	}

	cont:

	Vector3d middleVert;
	Vector3d quadVert1;//vertice for the wall
	Vector3d quadVert2;//vertice for the wall

	//find lower vertice between 2 other vertices
	if(otherVert1.y < otherVert2.y)
	{
		middleVert = otherVert1;
		quadVert1 = otherVert2;
	}
	else if(otherVert2.y < otherVert1.y)
	{
		middleVert = otherVert2;
		quadVert1 = otherVert1;
	}

	Vector3d portalVert1 = middleVert;
	Vector3d portalVert2;

	//find vertices which X and Z equal to the X and Z of the middleVert
	if(isXandZEqual(middleVert, lowestVert1))
	{
		portalVert2 = lowestVert1;
		quadVert2 = lowestVert2;
	}
	else if(isXandZEqual(middleVert, lowestVert2))
	{
		portalVert2 = lowestVert2;
		quadVert2 = lowestVert1;
	}

	if(!isPrevPartPort_)
	{
		newPortal_.plain.vertices[0] = portalVert1;
		newPortal_.plain.vertices[1] = portalVert2;
		newPolygon_.vertices[0] = quadVert2;
		newPolygon_.vertices[1] = quadVert1;

		//cout << "Portal vertice 0: x= " << portalVert1.x << " y= "
		//		<< portalVert1.y << " z= " << portalVert1.z << endl;
		//cout << "Portal vertice 1: x= " << portalVert2.x << " y= "
		//		<< portalVert2.y << " z= " << portalVert2.z << endl;
	}
	else
	{
		newPortal_.plain.vertices[2] = portalVert2;
		newPortal_.plain.vertices[3] = portalVert1;
		newPolygon_.vertices[2] = quadVert1;
		newPolygon_.vertices[3] = quadVert2;

		//cout << "Portal vertice 2: x= " << portalVert1.x << " y= "
		//		<< portalVert1.y << " z= " << portalVert1.z << endl;
		//cout << "Portal vertice 3: x= " << portalVert2.x << " y= "
		//		<< portalVert2.y << " z= " << portalVert2.z << endl;
	}
}

void cSceneManager::initWallsWithPortals(Object3D* obj_, vector<tWall*>& vSceneWalls_)
{
	vector<tPlane> quads;
	findQuadVerts(obj_, quads);//partition of the scene's walls (polygons) to the quads (with 4 vertices)

	//array for boolean values determine is particular quad is part of the portal or not.
	//If quad is part of the portal, then value with index of this quad will be true, otherwise false.
	bool* quadIsPortalArray = new bool[quads.size()];
	memset(quadIsPortalArray, false, sizeof(bool) * quads.size());//assign all values to the false

	for(int i = 0; i < quads.size(); i++)
	{
		//Find it is common quad or portal's part quad
		if(isPortal(quads[i]))
		{
			quadIsPortalArray[i] = true;
		}
	}

	int indID = 0;//walls should have the same IDs as indexes.
	//Need this variable to make work walls with portals in good way (to make right index) 

	int numPortal = 0;//IDs of portals starts from the 0

	for(int i = 0; i < quads.size(); i++)
	{
		if(quadIsPortalArray[i] == false)//if its quad(plane) without portals
		{
			//calculate normal--------
			Vector3d a(quads[i].vertices[2] - quads[i].vertices[1]);
			Vector3d b(quads[i].vertices[0] - quads[i].vertices[1]);
			Vector3d temp_normal(a * b);

			if((temp_normal.x + temp_normal.y + temp_normal.z) < 0)//check it is anticlockwice or clockwice
			{
				changeToAntiCWOrder(quads[i]);
			}

			tWall* newWall = new tWall;
			newWall->setId(indID);
			indID++;
			newWall->setType(1);
			//calculate normal--------
			Vector3d v1(quads[i].vertices[2] - quads[i].vertices[1]);
			Vector3d v2(quads[i].vertices[0] - quads[i].vertices[1]);
			Vector3d normal(v1 * v2);
			//------------------------
			quads[i].normal = normal;
			newWall->setPlain(quads[i]);
			vSceneWalls_.push_back(newWall);
		}
		else//if its quad(plane) with portals
		{
			tWall* newWall = new tPortalWall;
			newWall->setId(indID);
			indID++;
			newWall->setType(2);

			tPortal newPortal;
			newPortal.idPortal = numPortal;
			numPortal++;
			tPlane newPlane;
			//find 2 vertices of the portlal and 2 vertices of the plane for the part of the portal
			processQuadToWall(quads[i+1], false, newPortal, newPlane);
			//find 2 vertices of the portlal and 2 vertices of the plane for the part of the portal
			processQuadToWall(quads[i], true, newPortal, newPlane);

			//swap vertices in the plane------------
			Vector3d planeVerts[4];
			planeVerts[0] = newPlane.vertices[0];
			planeVerts[1] = newPlane.vertices[1];
			planeVerts[2] = newPlane.vertices[2];
			planeVerts[3] = newPlane.vertices[3];

			newPlane.vertices[0] = planeVerts[0];
			newPlane.vertices[1] = planeVerts[3];
			newPlane.vertices[2] = planeVerts[2];
			newPlane.vertices[3] = planeVerts[1];

				//calculate normal
				Vector3d a1(newPlane.vertices[2] - newPlane.vertices[1]);
				Vector3d b1(newPlane.vertices[0] - newPlane.vertices[1]);
				Vector3d temp_normal1(a1 * b1);

				if((temp_normal1.x + temp_normal1.y + temp_normal1.z) < 0)//check it is anticlockwice or clockwice
				{
					//changeToAntiCWOrder(newPlane);

					Vector3d temp[4];
					temp[0] = newPlane.vertices[0];
					temp[1] = newPlane.vertices[1];
					temp[2] = newPlane.vertices[2];
					temp[3] = newPlane.vertices[3];

					newPlane.vertices[0] = temp[1];
					newPlane.vertices[1] = temp[0];
					newPlane.vertices[2] = temp[3];
					newPlane.vertices[3] = temp[2];
				}
			//--------------------------------------
			//swap vertices in the portal-----------
			Vector3d portalVerts[4];
			portalVerts[0] = newPortal.plain.vertices[0];
			portalVerts[1] = newPortal.plain.vertices[1];
			portalVerts[2] = newPortal.plain.vertices[2];
			portalVerts[3] = newPortal.plain.vertices[3];

			newPortal.plain.vertices[0] = portalVerts[1];
			newPortal.plain.vertices[1] = portalVerts[2];
			newPortal.plain.vertices[2] = portalVerts[3];
			newPortal.plain.vertices[3] = portalVerts[0];

				//calculate normal
				Vector3d a2(newPortal.plain.vertices[2] - newPortal.plain.vertices[1]);
				Vector3d b2(newPortal.plain.vertices[0] - newPortal.plain.vertices[1]);
				Vector3d temp_normal2(a2 * b2);

				if((temp_normal2.x + temp_normal2.y + temp_normal2.z) < 0)//check it is anticlockwice or clockwice
				{
					//changeToAntiCWOrder(newPortal.plain);

					Vector3d temp[4];
					temp[0] = newPortal.plain.vertices[0];
					temp[1] = newPortal.plain.vertices[1];
					temp[2] = newPortal.plain.vertices[2];
					temp[3] = newPortal.plain.vertices[3];

					newPortal.plain.vertices[0] = temp[1];
					newPortal.plain.vertices[1] = temp[0];
					newPortal.plain.vertices[2] = temp[3];
					newPortal.plain.vertices[3] = temp[2];
				}
			//--------------------------------------

			i++;//because wall with portal made by 2 quads

			//calculate normal for the wall (basik plane)--------
			Vector3d v1(newPlane.vertices[2] - newPlane.vertices[1]);
			Vector3d v2(newPlane.vertices[0] - newPlane.vertices[1]);
			Vector3d normalWall(v1 * v2);
			//------------------------
			newPlane.normal = normalWall;
			newWall->setPlain(newPlane);
			//Calculate normal for the portal-------
			Vector3d v3(newPortal.plain.vertices[2] - newPortal.plain.vertices[1]);
			Vector3d v4(newPortal.plain.vertices[0] - newPortal.plain.vertices[1]);
			Vector3d normalPortal(v3 * v4);
			//--------------------------------------
			newPortal.plain.normal = normalPortal;
			newWall->addPortal(&newPortal);

			vSceneWalls_.push_back(newWall);
		}
	}
}

void cSceneManager::drawAllWallPolygons(int drawType)
{
	const int DIVIDER_FOR_NORMAL = 300;//to make normal shorter (for drawing normal), because normal in the polygon NOT unit
	const int NUM_VERTS_PER_POLY = 4;

	glDisable(GL_LIGHTING);
	glPointSize(10);
	glLineWidth(6);
	//glColor3f(0,1,0);

		for(int indPolygon = 0; indPolygon < m_vSceneWallPlygons.size(); indPolygon++)
		{
			tPlane polygon = m_vSceneWallPlygons[indPolygon]->getPlain();

			switch(drawType)
			{
			case DRAW_ALL:
				{
					Vector3d normalizedNormal(Vector3d::normalize(polygon.normal));

					glEnable(GL_LIGHTING);
						//draw polygon as quad with normal
						glBegin(GL_POLYGON);
							glNormal3f(normalizedNormal.x, normalizedNormal.y, normalizedNormal.z);
							for(int indVertice = 0; indVertice < NUM_VERTS_PER_POLY; indVertice++)
							{
								glVertex3f(polygon.vertices[indVertice].x,
											polygon.vertices[indVertice].y,
											polygon.vertices[indVertice].z);
							}
						glEnd();
					glDisable(GL_LIGHTING);

					//draw contour of the polygon (wall)
					glBegin(GL_LINE_LOOP/*GL_POINTS*/);
						for(int indVertice = 0; indVertice < NUM_VERTS_PER_POLY; indVertice++)
						{
							glVertex3f(polygon.vertices[indVertice].x,
										polygon.vertices[indVertice].y,
										polygon.vertices[indVertice].z);
						}
					glEnd();

			
					Vector3d center((polygon.vertices[0].x + polygon.vertices[2].x) / 2,
									(polygon.vertices[0].y + polygon.vertices[2].y) / 2,
									(polygon.vertices[0].z + polygon.vertices[2].z) / 2);//center of the current polygon (wall) 
					Vector3d normal(polygon.normal / DIVIDER_FOR_NORMAL);//because polygon.normal is NOT unit

					//draw normal grom center of the polygon (wall) to the polygon.normal / DIVIDER_FOR_NORMAL
					glBegin(GL_LINES/*GL_POINTS*/);
						glVertex3f(center.x, center.y, center.z);
						glVertex3f(normal.x + center.x, normal.y + center.y, normal.z + center.z);
					glEnd();
				}
				break;
			case DRAW_CONTOUR:
				{
					//draw contour of the polygon (wall)
					glBegin(GL_LINE_LOOP/*GL_POINTS*/);
						for(int indVertice = 0; indVertice < NUM_VERTS_PER_POLY; indVertice++)
						{
							glVertex3f(polygon.vertices[indVertice].x,
										polygon.vertices[indVertice].y,
										polygon.vertices[indVertice].z);
						}
					glEnd();
				}
				break;
			case DRAW_POLY:
				{
					Vector3d normalizedNormal(Vector3d::normalize(polygon.normal));

					glEnable(GL_LIGHTING);
						//draw polygon as quad with normal
						glBegin(GL_POLYGON);
							glNormal3f(normalizedNormal.x, normalizedNormal.y, normalizedNormal.z);
							for(int indVertice = 0; indVertice < NUM_VERTS_PER_POLY; indVertice++)
							{
								glVertex3f(polygon.vertices[indVertice].x,
											polygon.vertices[indVertice].y,
											polygon.vertices[indVertice].z);
							}
						glEnd();
					glDisable(GL_LIGHTING);
				}
				break;
			case DRAW_NORMAL:
				{
					Vector3d center((polygon.vertices[0].x + polygon.vertices[2].x) / 2,
									(polygon.vertices[0].y + polygon.vertices[2].y) / 2,
									(polygon.vertices[0].z + polygon.vertices[2].z) / 2);//center of the current polygon (wall) 
					Vector3d normal(polygon.normal / DIVIDER_FOR_NORMAL);//because polygon.normal is NOT unit

					//draw normal grom center of the polygon (wall) to the polygon.normal / DIVIDER_FOR_NORMAL
					glBegin(GL_LINES/*GL_POINTS*/);
						glVertex3f(center.x, center.y, center.z);
						glVertex3f(normal.x + center.x, normal.y + center.y, normal.z + center.z);
					glEnd();
				}
				break;
			case DRAW_NORMAL_AND_CONTOUR:
				{
					//draw contour of the polygon (wall)
					glBegin(GL_LINE_LOOP/*GL_POINTS*/);
						for(int indVertice = 0; indVertice < NUM_VERTS_PER_POLY; indVertice++)
						{
							glVertex3f(polygon.vertices[indVertice].x,
										polygon.vertices[indVertice].y,
										polygon.vertices[indVertice].z);
						}
					glEnd();

					Vector3d center((polygon.vertices[0].x + polygon.vertices[2].x) / 2,
									(polygon.vertices[0].y + polygon.vertices[2].y) / 2,
									(polygon.vertices[0].z + polygon.vertices[2].z) / 2);//center of the current polygon (wall) 
					Vector3d normal(polygon.normal / DIVIDER_FOR_NORMAL);//because polygon.normal is NOT unit

					//draw normal grom center of the polygon (wall) to the polygon.normal / DIVIDER_FOR_NORMAL
					glBegin(GL_LINES/*GL_POINTS*/);
						glVertex3f(center.x, center.y, center.z);
						glVertex3f(normal.x + center.x, normal.y + center.y, normal.z + center.z);
					glEnd();
				}
				break;
			}
		}

	glColor3f(1,1,1);
	glLineWidth(1);
	glPointSize(1);
	glEnable(GL_LIGHTING);
}

void cSceneManager::initPortals()
{
	//go throw all polyogons (walls)
	for(int indPolygon = 0; indPolygon < m_vSceneWallPlygons.size(); indPolygon++)
	{
		if(m_vSceneWallPlygons[indPolygon]->getType() == 2)//if this polygon (wall) has portal
		{
			vector<tPortal*> portals = *m_vSceneWallPlygons[indPolygon]->getPortals();
			int numPortals = portals.size();

			//add all portals of the current polygon (wall) to the vector (array)
			for(int indPortal = 0; indPortal < numPortals; indPortal++)
			{
				m_vPortals.push_back(portals[indPortal]);
			}
		}
	}
}

void cSceneManager::drawAllPortals(int drawType)
{
	const int DIVIDER_FOR_NORMAL = 300;//to make normal shorter (for drawing normal), because normal in the polygon NOT unit
	const int NUM_VERTS_PER_POLY = 4;

	glDisable(GL_LIGHTING);
	glPointSize(10);
	glLineWidth(6);
	//glColor3f(0,1,0);

		for(int indPolygon = 0; indPolygon < m_vPortals.size(); indPolygon++)
		{
			tPlane polygon = m_vPortals[indPolygon]->plain;

			switch(drawType)
			{
			case DRAW_ALL:
				{
					Vector3d normalizedNormal(Vector3d::normalize(polygon.normal));

					glEnable(GL_LIGHTING);
						//draw polygon as quad with normal
						glBegin(GL_POLYGON);
							glNormal3f(normalizedNormal.x, normalizedNormal.y, normalizedNormal.z);
							for(int indVertice = 0; indVertice < NUM_VERTS_PER_POLY; indVertice++)
							{
								glVertex3f(polygon.vertices[indVertice].x,
											polygon.vertices[indVertice].y,
											polygon.vertices[indVertice].z);
							}
						glEnd();
					glDisable(GL_LIGHTING);

					//draw contour of the polygon (wall)
					glBegin(GL_LINE_LOOP/*GL_POINTS*/);
						for(int indVertice = 0; indVertice < NUM_VERTS_PER_POLY; indVertice++)
						{
							glVertex3f(polygon.vertices[indVertice].x,
										polygon.vertices[indVertice].y,
										polygon.vertices[indVertice].z);
						}
					glEnd();

			
					Vector3d center((polygon.vertices[0].x + polygon.vertices[2].x) / 2,
									(polygon.vertices[0].y + polygon.vertices[2].y) / 2,
									(polygon.vertices[0].z + polygon.vertices[2].z) / 2);//center of the current polygon (wall) 
					Vector3d normal(polygon.normal / DIVIDER_FOR_NORMAL);//because polygon.normal is NOT unit

					//draw normal grom center of the polygon (wall) to the polygon.normal / DIVIDER_FOR_NORMAL
					glBegin(GL_LINES/*GL_POINTS*/);
						glVertex3f(center.x, center.y, center.z);
						glVertex3f(normal.x + center.x, normal.y + center.y, normal.z + center.z);
					glEnd();
				}
				break;
			case DRAW_CONTOUR:
				{
					//draw contour of the polygon (wall)
					glBegin(GL_LINE_LOOP/*GL_POINTS*/);
						for(int indVertice = 0; indVertice < NUM_VERTS_PER_POLY; indVertice++)
						{
							glVertex3f(polygon.vertices[indVertice].x,
										polygon.vertices[indVertice].y,
										polygon.vertices[indVertice].z);
						}
					glEnd();
				}
				break;
			case DRAW_POLY:
				{
					Vector3d normalizedNormal(Vector3d::normalize(polygon.normal));

					glEnable(GL_LIGHTING);
						//draw polygon as quad with normal
						glBegin(GL_POLYGON);
							glNormal3f(normalizedNormal.x, normalizedNormal.y, normalizedNormal.z);
							for(int indVertice = 0; indVertice < NUM_VERTS_PER_POLY; indVertice++)
							{
								glVertex3f(polygon.vertices[indVertice].x,
											polygon.vertices[indVertice].y,
											polygon.vertices[indVertice].z);
							}
						glEnd();
					glDisable(GL_LIGHTING);
				}
				break;
			case DRAW_NORMAL:
				{
					Vector3d center((polygon.vertices[0].x + polygon.vertices[2].x) / 2,
									(polygon.vertices[0].y + polygon.vertices[2].y) / 2,
									(polygon.vertices[0].z + polygon.vertices[2].z) / 2);//center of the current polygon (wall) 
					Vector3d normal(polygon.normal / DIVIDER_FOR_NORMAL);//because polygon.normal is NOT unit

					//draw normal grom center of the polygon (wall) to the polygon.normal / DIVIDER_FOR_NORMAL
					glBegin(GL_LINES/*GL_POINTS*/);
						glVertex3f(center.x, center.y, center.z);
						glVertex3f(normal.x + center.x, normal.y + center.y, normal.z + center.z);
					glEnd();
				}
				break;
			case DRAW_NORMAL_AND_CONTOUR:
				{
					//draw contour of the polygon (wall)
					glBegin(GL_LINE_LOOP/*GL_POINTS*/);
						for(int indVertice = 0; indVertice < NUM_VERTS_PER_POLY; indVertice++)
						{
							glVertex3f(polygon.vertices[indVertice].x,
										polygon.vertices[indVertice].y,
										polygon.vertices[indVertice].z);
						}
					glEnd();

					Vector3d center((polygon.vertices[0].x + polygon.vertices[2].x) / 2,
									(polygon.vertices[0].y + polygon.vertices[2].y) / 2,
									(polygon.vertices[0].z + polygon.vertices[2].z) / 2);//center of the current polygon (wall) 
					Vector3d normal(polygon.normal / DIVIDER_FOR_NORMAL);//because polygon.normal is NOT unit

					//draw normal grom center of the polygon (wall) to the polygon.normal / DIVIDER_FOR_NORMAL
					glBegin(GL_LINES/*GL_POINTS*/);
						glVertex3f(center.x, center.y, center.z);
						glVertex3f(normal.x + center.x, normal.y + center.y, normal.z + center.z);
					glEnd();
				}
				break;
			}
		}

	glColor3f(1,1,1);
	glLineWidth(1);
	glPointSize(1);
	glEnable(GL_LIGHTING);
}

void cSceneManager::initLeafNodesCollection(CNode* node, vector<CNode*>& vLeafNodes_)
{
	if(node->left == NULL || node->right == NULL)
	{
		vLeafNodes_.push_back(node);

		////////////////////////////////////////////////////
		//tWall* walls = node->getDataPolys();
		//int numWalls = node->numDataPolys();
		////////////////////////////////////////////////////
	}
	else
	{
		initLeafNodesCollection(node->left, vLeafNodes_);
		initLeafNodesCollection(node->right, vLeafNodes_);
	}
}

void cSceneManager::initRoomNodesCollection(vector<CNode*>& vLeafNodes_, vector<CNode*>& vRoomsNodes_)
{
	for(int indLeafNode = 0; indLeafNode < vLeafNodes_.size(); indLeafNode++)
	{
		if(isRoomNode(vLeafNodes_[indLeafNode]))
		{
			vRoomsNodes_.push_back(vLeafNodes_[indLeafNode]);
		}
	}
}

bool cSceneManager::isRoomNode(CNode* leafNode_)
{
	vector<tWall*>* polygons = leafNode_->getDataPolys();//polygons (walls) in the node
	int numPolygons = polygons->size();

	if(numPolygons >= 3)
	{
		//Need to retrieve all vertical edges of the all polygons (walls) in this node---
		vector<tLine> vVerticalEdges;

		for(int indPolygon = 0; indPolygon < numPolygons; indPolygon++)
		{
			tLine twoVerticalEdges[2];

			//Every polygon has two vertical edges. We need to find it.
			findVerticalEdgesOfPoly(*(*polygons)[indPolygon], twoVerticalEdges);

			vVerticalEdges.push_back(twoVerticalEdges[0]);
			vVerticalEdges.push_back(twoVerticalEdges[1]);
		}
		//-------------------------------------------------------------------------------

		//Each vertical edge should be collinear with one another vertical edge
		for(int indChoosedVertEdge = 0; indChoosedVertEdge < vVerticalEdges.size(); indChoosedVertEdge++)
		{
			bool thereIsCollinearEdge = false;// there is collinear edge for this current vertical edge

			for(int indTestedVertEdge = 0; indTestedVertEdge < vVerticalEdges.size(); indTestedVertEdge++)
			{
				if(indChoosedVertEdge != indTestedVertEdge)
				{
					if(isLinesCollinear(vVerticalEdges[indChoosedVertEdge].p1, vVerticalEdges[indChoosedVertEdge].p2,
										vVerticalEdges[indTestedVertEdge].p1, vVerticalEdges[indTestedVertEdge].p2))
					{
						thereIsCollinearEdge = true;
						break;
					}
				}
			}

			if(!thereIsCollinearEdge)
			{
				return false;
			}
		}

		return true;
	}
	else
	{
		return false;
	}
}

void cSceneManager::findVerticalEdgesOfPoly(tWall& polygon, tLine* twoVertEdges)
{
	tPlane polygonPlane = polygon.getPlain();

	//indexesVerts need to be deleted
	int* indexesVerts = findVerticalEdges(polygonPlane);//two pairs of the indexes (first pair is one vertical line and the same for second one).

	twoVertEdges[0].p1 = polygonPlane.vertices[indexesVerts[0]];
	twoVertEdges[0].p2 = polygonPlane.vertices[indexesVerts[1]];
	twoVertEdges[1].p1 = polygonPlane.vertices[indexesVerts[2]];
	twoVertEdges[1].p2 = polygonPlane.vertices[indexesVerts[3]];

	delete[] indexesVerts;
}

int* cSceneManager::findVerticalEdges(tPlane& plane)
{
	int* indexesArray = new int[4];//return array of indexes in special order
	//(first pair of indexes is vertical edge and second pair is vertical edge)

	if(isXandZEqual(plane.vertices[0], plane.vertices[1]))
	{
		//find lower vertex of the first vertical edge
		if(plane.vertices[0].y < plane.vertices[1].y)
		{
			indexesArray[0] = 0;
			indexesArray[1] = 1;
		}
		else
		{
			indexesArray[0] = 1;
			indexesArray[1] = 0;
		}

		//find lower vertex of the secon vertical edge
		if(plane.vertices[2].y < plane.vertices[3].y)
		{
			indexesArray[2] = 2;
			indexesArray[3] = 3;
		}
		else
		{
			indexesArray[2] = 3;
			indexesArray[3] = 2;
		}
	}
	else if(isXandZEqual(plane.vertices[0], plane.vertices[2]))
	{
		if(plane.vertices[0].y < plane.vertices[2].y)
		{
			indexesArray[0] = 0;
			indexesArray[1] = 2;
		}
		else
		{
			indexesArray[0] = 2;
			indexesArray[1] = 0;
		}

		if(plane.vertices[1].y < plane.vertices[3].y)
		{
			indexesArray[2] = 1;
			indexesArray[3] = 3;
		}
		else
		{
			indexesArray[2] = 3;
			indexesArray[3] = 1;
		}
	}
	else if(isXandZEqual(plane.vertices[0], plane.vertices[3]))
	{
		if(plane.vertices[0].y < plane.vertices[3].y)
		{
			indexesArray[0] = 0;
			indexesArray[1] = 3;
		}
		else
		{
			indexesArray[0] = 3;
			indexesArray[1] = 0;
		}

		if(plane.vertices[1].y < plane.vertices[2].y)
		{
			indexesArray[2] = 1;
			indexesArray[3] = 2;
		}
		else
		{
			indexesArray[2] = 2;
			indexesArray[3] = 1;
		}
	}

	return indexesArray;
}

CBinaryTree* cSceneManager::getWallPolysBTree()
{
	return m_wallPolygonsBTree;
}

int cSceneManager::rec_findIdNodeWithCam(CNode* node_, Vector3d camPos)
{
	const int NUM_VERTEX_IN_POLY = 4;//number of vertices in polygons

	int idNodeWithCum = node_->id;

	if(node_->left != NULL && node_->right != NULL)//if it is not leaf node
	{
		vector<tWall*>* divWalls = node_->getDivPolys();
		tPlane polygon = (*divWalls)[0]->getPlain();//we need to check only one polygon. It will be enought

		//check is camera location point on the left, on the right
		//or on the middle to this polygon---------------------------
		//find lowest two points (lowest edge) of the polygon
		Vector3d lowestP1;
		Vector3d lowestP2;
		//find smallest y value
		float smallestY = polygon.vertices[0].y;
		for(int indVertex = 1; indVertex < NUM_VERTEX_IN_POLY; indVertex++)
		{
			if(polygon.vertices[indVertex].y < smallestY)
			{
				smallestY = polygon.vertices[indVertex].y;
			}
		}
		//find lowest two points (lowest edge) of the polygon
		int whichLowestPAssign = 1;
		for(int indVertex = 0; indVertex < NUM_VERTEX_IN_POLY; indVertex++)
		{
			if(polygon.vertices[indVertex].y == smallestY && whichLowestPAssign == 1)
			{
				lowestP1 = polygon.vertices[indVertex];
				whichLowestPAssign = 2;//need to find and assign another lowest point
			}
			else if(polygon.vertices[indVertex].y == smallestY && whichLowestPAssign == 2)
			{
				lowestP2 = polygon.vertices[indVertex];
				break;
			}
		}

		if(/*node_->type == NORMAL_NODE*/ node_->left != NULL && node_->right != NULL)
		{
			//on left test gor camera location and edge of the polygon (lowestP1 and lowestP2 points)
			switch(onLeftForWall(lowestP2, lowestP1, camPos))
			{
			case 0://point on the middle
				break;
			case 1://point on the left
				idNodeWithCum = rec_findIdNodeWithCam(node_->left, camPos);
				break;
			case 2://point on the right
				idNodeWithCum = rec_findIdNodeWithCam(node_->right, camPos);
				break;
			}
		}
		//----------------------------------------------------------------
	}
	else//if it is leaf node
	{
		return idNodeWithCum;
	}
}

void cSceneManager::calculateIdNodeWithCamera(Vector3d camPos)
{
	//find node with camera in binary tree according to div planes
	m_idNodeWithCamera = rec_findIdNodeWithCam(this->getWallPolysBTree()->root, camPos);
}

int cSceneManager::getIdNodeWithCam()
{
	return m_idNodeWithCamera;
}

int cSceneManager::getCalculatedIdNodeWithCam(Vector3d camPos)
{
	calculateIdNodeWithCamera(camPos);

	return m_idNodeWithCamera;
}

void cSceneManager::claculateCamNodeIsRoom()
{
	for(int indRoomNode = 0; indRoomNode < m_vRoomsNodes.size(); indRoomNode++)
	{
		if(m_vRoomsNodes[indRoomNode]->id == m_idNodeWithCamera)
		{
			//cout << "You are inside the room with id = " << (*roomNodes)[indRoomNode]->id << endl;
			this->m_isRoomNodeWithCamera = true;
			return;
		}
	}

	this->m_isRoomNodeWithCamera = false;
}

bool cSceneManager::getIsRoomNodeWithCamera()
{
	return m_isRoomNodeWithCamera;
}

bool cSceneManager::getCalculatedIsRoomNodeWithCamera(Vector3d camPos)
{
	calculateIdNodeWithCamera(camPos);

	claculateCamNodeIsRoom();

	return m_isRoomNodeWithCamera;
}

vector<tPortal*>* cSceneManager::getPortals()
{
	return &m_vPortals;
}

vector<CRoom*>* cSceneManager::getRooms()
{
	return &m_vRooms;
}

void cSceneManager::initRooms()
{
	//go throw room nodes
	for(int indRoomNode = 0; indRoomNode < m_vRoomsNodes.size(); indRoomNode++)
	{
		CRoom* room = new CRoom(m_vRoomsNodes[indRoomNode]->id,
								m_vRoomsNodes[indRoomNode]->getDataPolys());

		//add all portals from the walls in this room node to the room object------------
		vector<tWall*>* wallPolygons = m_vRoomsNodes[indRoomNode]->getDataPolys();
		int numWallPolygons = wallPolygons->size();

		//go throw wall polygons
		for(int indWallPoly = 0; indWallPoly < numWallPolygons; indWallPoly++)
		{
			if((*wallPolygons)[indWallPoly]->getType() == 2)//if this wall with portal
			{
				vector<tPortal*>* portals = (*wallPolygons)[indWallPoly]->getPortals();
				int numPortals = portals->size();

				//add portals of this wall to the room
				for(int indPortal = 0; indPortal < numPortals; indPortal++)
				{
					room->addPortal(*(*portals)[indPortal]);
				}
			}
		}

		m_vRooms.push_back(room);
		//--------------------------------------------------------------------------------
	}
}

vector<Object3D*>* cSceneManager::getSceneObjects()
{
	return &m_sceneObjects;
}

Object3D*  cSceneManager::getSceneObject(int index)
{
	for(int indSceneObj = 0; indSceneObj < m_sceneObjects.size(); indSceneObj++)
	{
		if(indSceneObj == index)
		{
			return m_sceneObjects[indSceneObj];
		}
	}
}

int cSceneManager::addSceneObject(Object3D* object)
{
	m_sceneObjects.push_back(object);
	findAndAddObjToNode(object);//add object to some node according to the div plane of each node in binary tree
	return (m_sceneObjects.size() - 1);
}

void cSceneManager::drawAllObjects()
{
	for(int indObject = 0; indObject < m_sceneObjects.size(); indObject++)
	{
		m_sceneObjects[indObject]->drawWithTransform();
	}
}

void cSceneManager::rec_findAndAddObjToNode(CNode* node_, Object3D* obj_)
{
	const int NUM_BB_VERTEX_IN_OBJ = 4;

	if(node_->left != NULL && node_->right != NULL)//if it is NOT leaf node
	{
		rec_findAndAddObjToNode(node_->left, obj_);
		rec_findAndAddObjToNode(node_->right, obj_);
	}
	else//if it is leaf node
	{
		//if it is a node with room
		if(this->getRoom(node_->id) != NULL)
		{
			//get one data (bounding) plane of the node (one div plane will be enought)
			vector<tWall*>* boundingWalls = node_->getDataPolys();
			vector<tLine> bottomPointsOfBoundPolys;
			initBottomPoints(boundingWalls, &bottomPointsOfBoundPolys);
			swapBottomPointsIfNeeded(&bottomPointsOfBoundPolys, true);

			//check each vertex of the bb object to the each bounding wall's bottom edge of this node
		
			//how many points of the object's bounding box is inside this node
			int howManyPIsInNode = 0;

			//go throw all botom edges
			for(int indBottomEdge = 0; indBottomEdge < bottomPointsOfBoundPolys.size(); indBottomEdge++)
			{
				//how many points ob the object's boujnding box is on the left (or right) to the given plane (bouningPlane)
				int howManyPIsInPlane = 0;

				//go throw object's bounding box vertices
				for(int indVertex = 0; indVertex < NUM_BB_VERTEX_IN_OBJ; indVertex++)
				{
					int result = onLeftForWall(bottomPointsOfBoundPolys[indBottomEdge].p1,
									bottomPointsOfBoundPolys[indBottomEdge].p2,
									obj_->vertices[indVertex]);

					if(result == 0)//if this vertex is on the given plane
					{
					}
					else if(result == 1)//if on the left
					{
					}
					else//(result == 2)//if on the right
					{
						howManyPIsInPlane++;
					}
				}

				if(howManyPIsInPlane == NUM_BB_VERTEX_IN_OBJ)
				{
					howManyPIsInNode++;
				}
			}

			if(howManyPIsInNode == NUM_BB_VERTEX_IN_OBJ)//if all verteces of the object's bounding box in this node
			{
				this->getRoom(node_->id)->addRoomObject(obj_);
			}
			else
			{
				//if(howManyPIsInNode > 0)//if at least one vertes in insode the node
				//{
				//	//node_->addObject(obj_);
				//	this->getRoom(node_->id)->addRoomObject(obj_);
				//}

				//need to find 
				return;
			}
		}
		else//if it is NOT a room
		{
			////get one data (bounding) plane of the node (one div plane will be enought)
			//vector<tWall*>* boundingWalls = node_->getDataPolys();
			//vector<tLine> bottomPointsOfBoundPolys;
			//initBottomPoints(boundingWalls, &bottomPointsOfBoundPolys);
			////if(bottomPointsOfBoundPolys.size() > 1)
			////{
			//	swapBottomPointsIfNeeded(&bottomPointsOfBoundPolys, false);
			////}

			////check each vertex of the bb object to the each bounding wall's bottom edge of this node
		
			////how many points of the object's bounding box is inside this node
			//int howManyPIsInNode = 0;

			////go throw all botom edges
			//for(int indBottomEdge = 0; indBottomEdge < bottomPointsOfBoundPolys.size(); indBottomEdge++)
			//{
			//	//how many points ob the object's boujnding box is on the left (or right) to the given plane (bouningPlane)
			//	int howManyPIsInPlane = 0;

			//	//go throw object's bounding box vertices
			//	for(int indVertex = 0; indVertex < NUM_BB_VERTEX_IN_OBJ; indVertex++)
			//	{
			//		int result = onLeftForWall(bottomPointsOfBoundPolys[indBottomEdge].p1,
			//						bottomPointsOfBoundPolys[indBottomEdge].p2,
			//						obj_->vertices[indVertex]);

			//		if(result == 0)//if this vertex is on the given plane
			//		{
			//		}
			//		else if(result == 1)//if on the left
			//		{
			//		}
			//		else//(result == 2)//if on the right
			//		{
			//			howManyPIsInPlane++;
			//		}
			//	}

			//	if(howManyPIsInPlane == NUM_BB_VERTEX_IN_OBJ)
			//	{
			//		howManyPIsInNode++;
			//	}
			//}

			//if(howManyPIsInNode == NUM_BB_VERTEX_IN_OBJ)//if all verteces of the object's bounding box in this node
			//{
			//	//just add to leaf node
			//	node_->addObject(obj_);
			//}
			//else
			//{
			//	if(howManyPIsInNode > 0)//if at least one vertes in insode the node
			//	{
			//		node_->addObject(obj_);
			//	}

			//	//need to find 
			//	return;
			//}









			////get one data (bounding) plane of the node (one div plane will be enought)
			//vector<tWall*>* boundingWalls = node_->getDataPolys();
			//vector<tLine> bottomPointsOfBoundPolys;
			//initBottomPoints(boundingWalls, &bottomPointsOfBoundPolys);
			//if(bottomPointsOfBoundPolys.size() > 1)
			//{
			//	cout << endl;
			//}
			//swapBottomPointsIfNeeded(&bottomPointsOfBoundPolys);

			////how many points of the object's bounding box is inside this node
			//int howManyPIsInNode = 0;

			//for(int indBoundWall = 0; indBoundWall < (*boundingWalls).size(); indBoundWall++)
			//{
			//	tPlane boundPlane = (*boundingWalls)[indBoundWall]->getPlain();
			//	Vector3d boundPlaneNormal = Vector3d::normalize((boundPlane.vertices[2] - boundPlane.vertices[1]) *
			//													(boundPlane.vertices[0] - boundPlane.vertices[1]));

			//	//how many points ob the object's boujnding box is on the left (or right) to the given plane (bouningPlane)
			//	int howManyPIsInPlane = 0;

			//	for(int indVertex = 0; indVertex < NUM_BB_VERTEX_IN_OBJ; indVertex++)
			//	{
			//		//dot product between normal of the boundPlane and
			//		//the vector between object vertex and any of the plane vertices
			//		float dotProd = Vector3d::dotProduct(boundPlaneNormal, obj_->vertices[indVertex] - boundPlane.vertices[1]);

			//		if(dotProd >= 0)//on left or on the line
			//		{
			//			howManyPIsInPlane++;
			//		}
			//	}

			//	if(howManyPIsInPlane == NUM_BB_VERTEX_IN_OBJ)
			//	{
			//		howManyPIsInNode++;
			//	}
			//}

			//if(howManyPIsInNode == NUM_BB_VERTEX_IN_OBJ)//if all verteces of the object's bounding box in this node
			//{
			//	//just add to leaf node
			//	node_->addObject(obj_);
			//}
			//else
			//{
			//	if(howManyPIsInNode > 0)//if at least one vertes in insode the node
			//	{
			//		//add object to the node
			//		//and continue function
			//		node_->addObject(obj_);
			//	}

			//	//need to find other nodes with this object
			//	return;
			//}
		}






		////get one data (bounding) plane of the node (one div plane will be enought)
		//vector<tWall*>* boundingWalls = node_->getDataPolys();
		//vector<tLine> bottomPointsOfBoundPolys;
		//initBottomPoints(boundingWalls, &bottomPointsOfBoundPolys);
		//swapBottomPointsIfNeeded(&bottomPointsOfBoundPolys);

		////check each vertex of the bb object to the each bounding wall's bottom edge of this node
		//
		////how many points of the object's bounding box is inside this node
		//int howManyPIsInNode = 0;

		////go throw all botom edges
		//for(int indBottomEdge = 0; indBottomEdge < bottomPointsOfBoundPolys.size(); indBottomEdge++)
		//{
		//	//how many points ob the object's boujnding box is on the left (or right) to the given plane (bouningPlane)
		//	int howManyPIsInPlane = 0;

		//	//go throw object's bounding box vertices
		//	for(int indVertex = 0; indVertex < NUM_BB_VERTEX_IN_OBJ; indVertex++)
		//	{
		//		int result = onLeftForWall(bottomPointsOfBoundPolys[indBottomEdge].p1,
		//						bottomPointsOfBoundPolys[indBottomEdge].p2,
		//						obj_->vertices[indVertex]);

		//		if(result == 0)//if this vertex is on the given plane
		//		{
		//		}
		//		else if(result == 1)//if on the left
		//		{
		//		}
		//		else//(result == 2)//if on the right
		//		{
		//			howManyPIsInPlane++;
		//		}
		//	}

		//	if(howManyPIsInPlane == NUM_BB_VERTEX_IN_OBJ)
		//	{
		//		howManyPIsInNode++;
		//	}
		//}

		//if(howManyPIsInNode == NUM_BB_VERTEX_IN_OBJ)//if all verteces of the object's bounding box in this node
		//{
		//	//add object to the node
		//	//and dont continue recursion and function
		//	if(this->getRoom(node_->id) != NULL)//(isRoomNode(node_))//if this leaf node is a room node
		//	{
		//		//add to eaf node and to the room
		//		//node_->addObject(obj_);
		//		this->getRoom(node_->id)->addRoomObject(obj_);
		//	}
		//	else
		//	{
		//		//just add to leaf node
		//		node_->addObject(obj_);
		//	}
		//}
		//else
		//{
		//	if(howManyPIsInNode > 0)//if at least one vertes in insode the node
		//	{
		//		//add object to the node
		//		//and continue function
		//		if(this->getRoom(node_->id) != NULL/*isRoomNode(node_)*/)//if this leaf node is a room node
		//		{
		//			node_->addObject(obj_);
		//		}
		//		else
		//		{
		//			node_->addObject(obj_);
		//		}
		//	}

		//	//need to find 
		//	return;
		//}


		{
		////check each vertex of the bb object to the each bounding wall of the node
		//for(int indBoundWall = 0; indBoundWall < boundingWalls->size(); indBoundWall++)//go thtow all bounding walls
		//{
		//	tPlane bouningPlane = (*boundingWalls)[indBoundWall]->getPlain();
		//	Vector3d bouningPlaneNormal = Vector3d::normalize((bouningPlane.vertices[2] - bouningPlane.vertices[1]) *
		//												(bouningPlane.vertices[0] - bouningPlane.vertices[1]));

		//	//how many points ob the object's boujnding box is on the left (or right) to the given plane (bouningPlane)
		//	int howManyPIsInPlane = 0;

		//	//go throw vertices of the object's bounding box
		//	for(int indVertex = 0; indVertex < NUM_BB_VERTEX_IN_OBJ; indVertex++)
		//	{
		//		//dot product between normal of the div plane and
		//		//the vector between object vertex and any of the plane vertices
		//		float dotProd = Vector3d::dotProduct(bouningPlaneNormal, obj_->vertices[indVertex] - bouningPlane.vertices[1]);

		//		if(dotProd >= 0)//on left or on the line
		//		{
		//			//howManyPIsInNode++;
		//			howManyPIsInPlane++;
		//		}
		//	}

		//	if(howManyPIsInPlane == NUM_BB_VERTEX_IN_OBJ)
		//	{
		//		howManyPIsInNode++;
		//	}
		//}

		//if(howManyPIsInNode == NUM_BB_VERTEX_IN_OBJ)//if all verteces of the object's bounding box in this node
		//{
		//	//add object to the node
		//	//and dont continue recursion and function
		//	if(this->getRoom(node_->id) != NULL)//(isRoomNode(node_))//if this leaf node is a room node
		//	{
		//		//add to eaf node and to the room
		//		node_->addObject(obj_);
		//		this->getRoom(node_->id)->addRoomObject(obj_);
		//	}
		//	else
		//	{
		//		//just add to leaf node
		//		node_->addObject(obj_);
		//	}
		//}
		//else
		//{
		//	if(howManyPIsInNode > 0)//if at least one vertes in insode the node
		//	{
		//		//add object to the node
		//		//and continue function
		//		if(isRoomNode(node_))//if this leaf node is a room node
		//		{
		//			node_->addObject(obj_);
		//		}
		//		else
		//		{
		//			node_->addObject(obj_);
		//		}
		//	}

		//	//need to find 
		//	return;
		//}
		}
	}
}

void cSceneManager::findAndAddObjToNode(Object3D* obj_)
{
	rec_findAndAddObjToNode(m_wallPolygonsBTree->root, obj_);
}

CRoom* cSceneManager::getRoom(int id_)
{
	for(int indRoom = 0; indRoom < m_vRooms.size(); indRoom++)
	{
		if(id_ == m_vRooms[indRoom]->getId())
		{
			return m_vRooms[indRoom];
		}
	}

	return NULL;
}

void cSceneManager::swapBottomPointsIfNeeded(vector<tLine>* bottomLines, bool isForRoom)
{
	//number of points of bottom edges of each wall (polygon) whick need to check
	const int NUM_NEED_CHECK_POINTS = 2;
	vector<int> usedBottomLines;

	//initialise variables for first taken line of the first plane (lines have the same indexes as points)
	int indTakenLine = 0;
	int indTakenBottomPoint = 1;
	Vector3d takenBottomPoint = (*bottomLines)[indTakenLine].p2;

	for(int indCheckedLine = 0; indCheckedLine < (*bottomLines).size(); indCheckedLine++)
	{
		if(isForRoom)
		{
			if(usedBottomLines.size() == (*bottomLines).size())//if all lines is used
			{
				break;
			}
			else
			{
				if(indCheckedLine != indTakenLine)//dont need to check the same lines (planes)
				{
					for(int indVertex = 0; indVertex < NUM_NEED_CHECK_POINTS; indVertex++)
					{
						Vector3d checkedBottomPoint;
						switch(indVertex)
						{
						case 0:
							checkedBottomPoint = (*bottomLines)[indCheckedLine].p1;
							break;
						case 1:
							checkedBottomPoint = (*bottomLines)[indCheckedLine].p2;
							break;
						}

						float EPSILON = numeric_limits<float>::epsilon();
						if(Vector3d::isEqual(takenBottomPoint, checkedBottomPoint, EPSILON))
						{
							switch(indVertex)
							{
							case 0:
								//dont need to swap p1 with p2
								usedBottomLines.push_back(indTakenLine);
								indTakenLine = indCheckedLine;
								indTakenBottomPoint = 1;
								takenBottomPoint = (*bottomLines)[indCheckedLine].p2;
								indCheckedLine = 0;//start loop from the begining
								break;
							case 1:
								//need to swap p1 with p2
								Vector3d point1 = (*bottomLines)[indCheckedLine].p1;
								(*bottomLines)[indCheckedLine].p1 = (*bottomLines)[indCheckedLine].p2;
								(*bottomLines)[indCheckedLine].p2 = point1;
								usedBottomLines.push_back(indTakenLine);
								indTakenLine = indCheckedLine;
								indTakenBottomPoint = indVertex;
								takenBottomPoint = (*bottomLines)[indCheckedLine].p2;
								indCheckedLine = 0;//start loop from the begining
								break;
							}
						}
					}
				}
			}
		}
		else
		{
			if(usedBottomLines.size() == (*bottomLines).size() - 1)//if all lines is used
			{
				break;
			}
			else
			{
				if(indCheckedLine != indTakenLine)//dont need to check the same lines (planes)
				{
					for(int indVertex = 0; indVertex < NUM_NEED_CHECK_POINTS; indVertex++)
					{
						Vector3d checkedBottomPoint;
						switch(indVertex)
						{
						case 0:
							checkedBottomPoint = (*bottomLines)[indCheckedLine].p1;
							break;
						case 1:
							checkedBottomPoint = (*bottomLines)[indCheckedLine].p2;
							break;
						}

						float EPSILON = numeric_limits<float>::epsilon();
						if(Vector3d::isEqual(takenBottomPoint, checkedBottomPoint, EPSILON))
						{
							switch(indVertex)
							{
							case 0:
								//dont need to swap p1 with p2
								usedBottomLines.push_back(indTakenLine);
								indTakenLine = indCheckedLine;
								indTakenBottomPoint = 1;
								takenBottomPoint = (*bottomLines)[indCheckedLine].p2;
								indCheckedLine = 0;//start loop from the begining
								break;
							case 1:
								//need to swap p1 with p2
								Vector3d point1 = (*bottomLines)[indCheckedLine].p1;
								(*bottomLines)[indCheckedLine].p1 = (*bottomLines)[indCheckedLine].p2;
								(*bottomLines)[indCheckedLine].p2 = point1;
								usedBottomLines.push_back(indTakenLine);
								indTakenLine = indCheckedLine;
								indTakenBottomPoint = indVertex;
								takenBottomPoint = (*bottomLines)[indCheckedLine].p2;
								indCheckedLine = 0;//start loop from the begining
								break;
							}
						}
					}
				}
			}
		}
	}

	if(isForRoom)
	{
		//check clockwise or anticlockwise-------
			float count = 0;
			for(int indLine = 0; indLine < (*bottomLines).size(); indLine++)
			{
				count += ((*bottomLines)[indLine].p2.x - (*bottomLines)[indLine].p1.x) * 
							((*bottomLines)[indLine].p2.z + (*bottomLines)[indLine].p1.z);
			}	

			if(count > 0)//if clockwise
			{
				for(int indLine = 0; indLine < (*bottomLines).size(); indLine++)
				{
					//swap every point of the every line in bottomLines
					Vector3d tempPoint((*bottomLines)[indLine].p1);
					(*bottomLines)[indLine].p1 = (*bottomLines)[indLine].p2;
					(*bottomLines)[indLine].p2 = tempPoint;
				}
			}
		//---------------------------------------
	}
	else
	{
		//check clockwise or anticlockwise-------
			float count = 0;
			for(int indLine = 0; indLine < (*bottomLines).size(); indLine++)
			{
				count += ((*bottomLines)[indLine].p2.x - (*bottomLines)[indLine].p1.x) * 
							((*bottomLines)[indLine].p2.z + (*bottomLines)[indLine].p1.z);
			}	

			if(count > 0)//if clockwise
			{
				for(int indLine = 0; indLine < (*bottomLines).size(); indLine++)
				{
					//swap every point of the every line in bottomLines
					Vector3d tempPoint((*bottomLines)[indLine].p1);
					(*bottomLines)[indLine].p1 = (*bottomLines)[indLine].p2;
					(*bottomLines)[indLine].p2 = tempPoint;
				}
			}
		//---------------------------------------
	}
}

void cSceneManager::initBottomPoints(vector<tWall*>* boundingWalls_, vector<tLine>* resultBottomLines)
{
	//take only 0 and 1 vertices (bottom points) of the bounding planes----
	for(int indPlane = 0; indPlane < boundingWalls_->size(); indPlane++)//go throw all bounding walls
	{
		tLine twoBottomPoints;
		twoBottomPoints.p1 = (*boundingWalls_)[indPlane]->getPlain().vertices[0];
		twoBottomPoints.p2 = (*boundingWalls_)[indPlane]->getPlain().vertices[1];
		(*resultBottomLines).push_back(twoBottomPoints);
	}
	//---------------------------------------------------------------------
}