#pragma once
#include "Object3D.h"
#include "CBinaryTree.h"
#include "CRoom.h"

//Draw type
enum {DRAW_ALL, DRAW_CONTOUR, DRAW_POLY, DRAW_NORMAL, DRAW_NORMAL_AND_CONTOUR};

class cSceneManager
{
public:
	// FUNCTIONS:
		//Parameters:
		//idScene - determine with which scene from the project folder this scene manager will work.
		cSceneManager(int idScene);
		~cSceneManager();

		void set3dSceneModel(Object3D* object);
		inline Object3D* get3dSceneModel()
		{
			return m_3dSceneModel;
		}

		vector<CNode*>* getRoomsNodes();
		vector<CNode*>* getLeafNodes();
		CBinaryTree* getWallPolysBTree();
		int getIdNodeWithCam();//if want to use it, calculateIdNodeWithCamera() must be called before
		int getCalculatedIdNodeWithCam(Vector3d camPos);//calculate and get node id with camera
		bool getIsRoomNodeWithCamera();//if want to use it, claculateCamNodeIsRoom() must be called before
		bool getCalculatedIsRoomNodeWithCamera(Vector3d camPos);
		vector<tPortal*>* getPortals();
		vector<CRoom*>* getRooms();
		CRoom* getRoom(int id_);
		vector<Object3D*>* getSceneObjects();
		Object3D* getSceneObject(int index);
		//add object to the scene and return id of this object to having acces to this object latter
		int addSceneObject(Object3D* object);

		void calculateIdNodeWithCamera(Vector3d camPos);//find node with camera in the m_wallPolygonsBTree binary tree
		void claculateCamNodeIsRoom();//check is node with camera - room node or not

		// DRAW FUNCTIONS:
			//draw all quad walls of the scene without portals at all
			void drawAllWallPolygons(int drawType);
			void drawAllPortals(int drawType);
			void drawAllObjects();

private:
	// VARIABLES:
		Object3D* m_3dSceneModel;//all the time drawn. 3d model of the whole scene with textures.
		vector<tPortal*> m_vPortals;//all portals (doors) of the scene
		vector<tWall*> m_vSceneWallPlygons;//polygons (quads) of the whole scene which represent walls (walls with and without portals)
		CBinaryTree* m_wallPolygonsBTree;//binary tree with all wall polygons in the scene
		vector<CNode*> m_vLeafNodes;//vector with leaf nodes from the m_wallPolygonsBTree
		vector<CNode*> m_vRoomsNodes;//vector with leaf nodes, which are rooms, from the m_vLeafNodes
		vector<CRoom*> m_vRooms;
		vector<Object3D*> m_sceneObjects;//all objects of the scene
		
		//variables for camera and scene
		int m_idNodeWithCamera;//current node from the m_wallPolygonsBTree with camera
		bool m_isRoomNodeWithCamera;//is current node with camera from the m_wallPolygonsBTree - room node or not

	// FUNCTIONS:
		void choose3dSceneModel(int idScene);
		//find a quad polygons (planes) of the whole scene
		//return - vector of the quad polygons (planes)
		void findQuadVerts(Object3D* obj_, vector<tPlane>& quads_);
		//check if it is part of portal
		bool isPortal(tPlane quad);
		//check is c on the left to the a and b or not
		bool isLeft(Vector3d& a, Vector3d& b, Vector3d& c);
		//change order of the vertices to the anticlockwice
		void changeToAntiCWOrder(tPlane& plane);
		//Parameters:
		//quad_ - part of the wall with portal
		//isPrevPartPort_ - it is first or second part of the wall with portal (each wall with portal has 2 parts)
		//newPortal_ - final portal. When function called first time, [0] and [1] elements is assigmed. In second time it is [2] and [3]
		//newPolygon_ - final plane (wall). When function called first time, [0] and [1] elements is assigmed. In second time it is [2] and [3]
		void processQuadToWall(tPlane& quad_, bool isPrevPartPort_, tPortal& newPortal_, tPlane& newPolygon_);
		//find indexes of the other two vertices
		void findIndTwoVert(int indA_, int indB_, int& finalIndC_, int& finalIndD_);
		//Parameters:
		//obj_ - 3d object of the scene (should be with walls)
		//vSceneWalls_ - final vector, which will contains all walls of the scene (walls with portals and walls without portals).
		void initWallsWithPortals(Object3D* obj_, vector<tWall*>& vSceneWalls_);
		//find portals in the m_vSceneWallPlygons and save it to the m_vPortals
		void initPortals();
		//make vector (array) with leaf nodes from the binary tree
		void initLeafNodesCollection(CNode* node, vector<CNode*>& vLeafNodes_);
		//find rooms in the leaf nodes and then save it to the parameter array (vector)
		void initRoomNodesCollection(vector<CNode*>& vLeafNodes_, vector<CNode*>& vRoomsNodes_);
		//check is given leaf node is room node or not
		bool isRoomNode(CNode* leafNode_);
		//should find two vertical edges of the polygon and add it to the twoVertEdges array
		void findVerticalEdgesOfPoly(tWall& polygon, tLine* twoVertEdges);
		//find vertival edges of the polygon
		//they should have the same x and z
		int* findVerticalEdges(tPlane& plane);
		//function used in getIdNodeWithCamera(). Used recursion
		int rec_findIdNodeWithCam(CNode* node_, Vector3d camPos);
		//maker room objects from the room nodes
		void initRooms();
		//find node for the given object and add it to the node
		void findAndAddObjToNode(Object3D* obj_);
		void rec_findAndAddObjToNode(CNode* node_, Object3D* obj_);
		void swapBottomPointsIfNeeded(vector<tLine>* bottomLines, bool isForRoom);
		void initBottomPoints(vector<tWall*>* boundingWalls_, vector<tLine>* resultBottomLines);
};