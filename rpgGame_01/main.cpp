#pragma warning(disable:4244)

#include "GameEngine.h"
#include "Object3D.h"
#include "DrawFunctions.h"
#include "cSceneManager.h"
#include "CFrustum.h"
#include "tFrustum.h"

#define MAX_NUM_PORTALS_PARTS	30
#define ID_SCENE				2
//
////which frustum to use
////it can be 1 or 2
////if 1, then standard view frustum will be used
////if 2, then NOT standard view frustum will be used
//#define USE_FRUSTUM				2

bool g_bIgnoreFrustum = false;
bool g_bCameraOn = true;

//IDs of the scene objects---
#define NUM_OBJECTS	70
int ID_OBJECTS[NUM_OBJECTS] = {0};//array with IDs of teapots
//---------------------------

CFrustum g_Frustum;//main frustum
cSceneManager* sceneManager;
tFrustum* g_Frustum2;//additional frustum

vector<int> g_idRoomsNeedDraw;

void display()								
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
	glLoadIdentity();
	
	g_Camera.Look();

	drawFloor();
	sceneManager->get3dSceneModel()->drawWithTransform();//draw 3d model of the scene

	switch(USE_FRUSTUM)
	{
	case 1:
		glDisable(GL_LIGHTING);
		glPointSize(16);
		glLineWidth(10);
		glColor3f(1,1,0);

			if(sceneManager->getIsRoomNodeWithCamera())//if camera is inside some room
			{
				//draw near rooms if portal to this room is in the view_frustum
				vector<CRoom*>* rooms = sceneManager->getRooms();
				for(int indNearRoomID = 0; indNearRoomID < g_Frustum.getIdRoomsNeedDraw()->size(); indNearRoomID++)
				{
					drawRoom(rooms, (*g_Frustum.getIdRoomsNeedDraw())[indNearRoomID]);
					cout << "room " << indNearRoomID << endl << endl;
				}
			}
			else//if camera is not in the rooms
			{
				//draw node which is not a room but a leaf node
				drawLeafNodeWithId(*sceneManager->getLeafNodes(), sceneManager->getIdNodeWithCam());
			}

		glColor3f(1,1,1);
		glLineWidth(1);
		glPointSize(1);
		glEnable(GL_LIGHTING);
		break;
	case 2:
		{
			g_Frustum2->draw();

			glDisable(GL_LIGHTING);
			glPointSize(16);
			glLineWidth(10);
			glColor3f(1,1,0);

				if(sceneManager->getIsRoomNodeWithCamera())//if camera is inside some room
				{
					//draw near rooms if portal to this room is in the view_frustum
					vector<CRoom*>* rooms = sceneManager->getRooms();
					for(int indNearRoomID = 0; indNearRoomID < g_Frustum2->getIdRoomsNeedDraw()->size(); indNearRoomID++)
					{
						drawRoom(rooms, (*g_Frustum2->getIdRoomsNeedDraw())[indNearRoomID]);
					}
				}
				else//if camera is not in the rooms
				{
					//draw node which is not a room but a leaf node
					drawLeafNodeWithId(*sceneManager->getLeafNodes(), sceneManager->getIdNodeWithCam());
				}

				/*Vector3d* nearVerts = g_Frustum2->getNearPlanesVerts();
				Vector3d* nearCenter = g_Frustum2->getNearPlaneCenter();

				glBegin(GL_POINTS);
					glVertex3f(nearVerts[0].x, nearVerts[0].y, nearVerts[0].z);
					glVertex3f(nearVerts[1].x, nearVerts[1].y, nearVerts[1].z);
					glVertex3f(nearVerts[2].x, nearVerts[2].y, nearVerts[2].z);
					glVertex3f(nearVerts[3].x, nearVerts[3].y, nearVerts[3].z);
					glVertex3f(nearCenter[0].x, nearCenter[0].y, nearCenter[0].z);
				glEnd();*/

			glColor3f(1,1,1);
			glLineWidth(1);
			glPointSize(1);
			glEnable(GL_LIGHTING);
			break;
		}
	default:
		MessageBox(NULL,"There is no this type of frustum!","ERROR",MB_OK|MB_ICONEXCLAMATION);
	}

	//cout << "cam pos = " << g_Frustum2->getPosition() << endl;

	glFlush();
}

//initialise vertices of the scene's bounding box
void initSceneBBVertices(tWall* wholeSceneBBplanes, Object3D* scene_)
{
	const float EXTRA_SIZE = 20;

	float hsX = (scene_->m_model.theBBox.boxWidthX + EXTRA_SIZE) / 2;//halfSizeX 
	float hsY = (scene_->m_model.theBBox.boxWidthY) / 2;//halfSizeY
	float hsZ = (scene_->m_model.theBBox.boxWidthZ + EXTRA_SIZE) / 2;//halfSizeZ
	Vector3d c = scene_->m_center;//center of the scene

	Vector3d sceneBoundBoxVertices[8];

	//bottom vertices-------------------------------
	sceneBoundBoxVertices[0] = Vector3d(c.x - hsX, c.y - hsY, c.z + hsZ);
	sceneBoundBoxVertices[1] = Vector3d(c.x + hsX, c.y - hsY, c.z + hsZ);
	sceneBoundBoxVertices[2] = Vector3d(c.x + hsX, c.y - hsY, c.z - hsZ);
	sceneBoundBoxVertices[3] = Vector3d(c.x - hsX, c.y - hsY, c.z - hsZ);
	//----------------------------------------------

	//upper vertices--------------------------------
	sceneBoundBoxVertices[4] = Vector3d(c.x - hsX, c.y + hsY, c.z + hsZ);
	sceneBoundBoxVertices[5] = Vector3d(c.x + hsX, c.y + hsY, c.z + hsZ);
	sceneBoundBoxVertices[6] = Vector3d(c.x + hsX, c.y + hsY, c.z - hsZ);
	sceneBoundBoxVertices[7] = Vector3d(c.x - hsX, c.y + hsY, c.z - hsZ);
	//----------------------------------------------

	for(int i = 0; i < 4; i++)
	{
		tPlane plane;

		if(i != 3)
		{
			plane.vertices[0] = sceneBoundBoxVertices[i];
			plane.vertices[1] = sceneBoundBoxVertices[i+1];
			plane.vertices[2] = sceneBoundBoxVertices[i+5];
			plane.vertices[3] = sceneBoundBoxVertices[i+4];
		}
		else
		{
			plane.vertices[0] = sceneBoundBoxVertices[3];
			plane.vertices[1] = sceneBoundBoxVertices[0];
			plane.vertices[2] = sceneBoundBoxVertices[4];
			plane.vertices[3] = sceneBoundBoxVertices[7];
		}

		Vector3d normal((plane.vertices[2] - plane.vertices[1]) *
								(plane.vertices[0] - plane.vertices[1]));
		plane.normal = normal;

		wholeSceneBBplanes[i].setId(i);
		wholeSceneBBplanes[i].setType(1);
		wholeSceneBBplanes[i].setPlain(plane);
	}
}

void initBottomPointsOfRoom(vector<tPlane>* planes, vector<tLine>* resultBottomLines)
{
	//take only 0 and 1 vertices (bottom points) of the bounding planes----
	for(int indPlane = 0; indPlane < planes->size(); indPlane++)//go throw all bounding walls
	{
		tLine twoBottomPoints;
		twoBottomPoints.p1 = (*planes)[indPlane].vertices[0];
		twoBottomPoints.p2 = (*planes)[indPlane].vertices[1];
		(*resultBottomLines).push_back(twoBottomPoints);
	}
	//---------------------------------------------------------------------
}

void swapBottomPointsIfNeeded(vector<tLine>* bottomLines)
{
	//number of points of bottom edges of each wall (polygon) whick need to check
	const int NUM_NEED_CHECK_POINTS = 2;
	vector<int> usedBottomLines;

	//initialise variables for first taken line of the first plane (lines have the same indexes as points)
	int indTakenLine = 0;
	int indTakenBottomPoint = 1;
	Vector3d takenBottomPoint = (*bottomLines)[indTakenLine].p2;

	for(int indCheckedLine = 0; indCheckedLine < (*bottomLines).size(); indCheckedLine++)
	{
		if(usedBottomLines.size() == (*bottomLines).size())//if all lines is used
		{
			break;
		}
		else
		{
			if(indCheckedLine != indTakenLine)//dint need to check the same lines (planes)
			{
				for(int indVertex = 0; indVertex < NUM_NEED_CHECK_POINTS; indVertex++)
				{
					Vector3d checkedBottomPoint;
					switch(indVertex)
					{
					case 0:
						checkedBottomPoint = (*bottomLines)[indCheckedLine].p1;
						break;
					case 1:
						checkedBottomPoint = (*bottomLines)[indCheckedLine].p2;
						break;
					}

					float EPSILON = numeric_limits<float>::epsilon();
					if(Vector3d::isEqual(takenBottomPoint, checkedBottomPoint, EPSILON))
					{
						switch(indVertex)
						{
						case 0:
							//dont need to swap p1 with p2
							usedBottomLines.push_back(indTakenLine);
							indTakenLine = indCheckedLine;
							indTakenBottomPoint = 1;
							takenBottomPoint = (*bottomLines)[indCheckedLine].p2;
							indCheckedLine = 0;//start loop from the begining
							break;
						case 1:
							//need to swap p1 with p2
							Vector3d point1 = (*bottomLines)[indCheckedLine].p1;
							(*bottomLines)[indCheckedLine].p1 = (*bottomLines)[indCheckedLine].p2;
							(*bottomLines)[indCheckedLine].p2 = point1;
							usedBottomLines.push_back(indTakenLine);
							indTakenLine = indCheckedLine;
							indTakenBottomPoint = indVertex;
							takenBottomPoint = (*bottomLines)[indCheckedLine].p2;
							indCheckedLine = 0;//start loop from the begining
							break;
						}
					}
				}
			}
		}
	}

	//check clockwise or anticlockwise-------
		float count = 0;
		for(int indLine = 0; indLine < (*bottomLines).size(); indLine++)
		{
			count += ((*bottomLines)[indLine].p2.x - (*bottomLines)[indLine].p1.x) * 
						((*bottomLines)[indLine].p2.z + (*bottomLines)[indLine].p1.z);
		}	

		if(count > 0)//if clockwise
		{
			for(int indLine = 0; indLine < (*bottomLines).size(); indLine++)
			{
				//swap every point of the every line in bottomLines
				Vector3d tempPoint((*bottomLines)[indLine].p1);
				(*bottomLines)[indLine].p1 = (*bottomLines)[indLine].p2;
				(*bottomLines)[indLine].p2 = tempPoint;
			}
		}
	//---------------------------------------
}

void init()
{
	g_Camera.PositionCamera(80, 8.5f, 6,  80, 8.5f, 0,   0, 1, 0);

	engine_LoadImage("floor.png", &g_Texture[ID_TEX_FLOOR]);

	sceneManager = new cSceneManager(ID_SCENE);

	ThreeDModel teapotModel;
	load3DModel("room/teapot.obj", teapotModel);
	ThreeDModel armchairModel;
	load3DModel("room/torus.obj", armchairModel);

	for(int i = 0; i < NUM_OBJECTS;i++)
	{
		int randXpos = rand() % 144 - 72;
		int randZpos = rand() % 144 - 72;
		Object3D* armchairObj = new Object3D(armchairModel, randXpos,4,randZpos);
		ID_OBJECTS[i] = sceneManager->addSceneObject(armchairObj);
	}

	/*Object3D* armchairObj1 = new Object3D(armchairModel, 40,9.1,20);
	Object3D* armchairObj2 = new Object3D(armchairModel, 50,9.1,20);
	Object3D* armchairObj3 = new Object3D(armchairModel, 60,9.1,20);
	Object3D* armchairObj4 = new Object3D(armchairModel, 40,9.1,-20);
	Object3D* armchairObj5 = new Object3D(armchairModel, 50,9.1,-20);
	Object3D* armchairObj6 = new Object3D(armchairModel, 60,9.1,-20);
	Object3D* armchairObj7 = new Object3D(armchairModel, 70,9.1,0);
	ID_OBJECTS[0] = sceneManager->addSceneObject(armchairObj1);
	ID_OBJECTS[1] = sceneManager->addSceneObject(armchairObj2);
	ID_OBJECTS[2] = sceneManager->addSceneObject(armchairObj3);
	ID_OBJECTS[3] = sceneManager->addSceneObject(armchairObj4);
	ID_OBJECTS[4] = sceneManager->addSceneObject(armchairObj5);
	ID_OBJECTS[5] = sceneManager->addSceneObject(armchairObj6);
	ID_OBJECTS[6] = sceneManager->addSceneObject(armchairObj7);*/

	g_Frustum.setSceneManager(sceneManager);
	g_Frustum.setIsIgnoreFrustum(g_bIgnoreFrustum);
	g_Frustum.setCamera(&g_Camera);

	g_Frustum2 = new tFrustum(Vector3d(80, 8.5f, 6), Vector3d(80, 8.5f, 0), 0.5f, 6.0f, Vector3d( 0, 1, 0), 90.0f, 1.4);
	g_Frustum2->setIsIgnoreFrustum(g_bIgnoreFrustum);
	g_Frustum2->setSceneManager(sceneManager);
}

void processKeys()
{
	if(keys['C'])
	{
		g_bCameraOn = !g_bCameraOn;

		keys['C'] = false;
	}

	if(keys['F'])
	{
		g_bIgnoreFrustum = !g_bIgnoreFrustum;
		g_Frustum.setIsIgnoreFrustum(g_bIgnoreFrustum);
		g_Frustum2->setIsIgnoreFrustum(g_bIgnoreFrustum);

		keys['F'] = false;
	}

	if(keys[VK_SPACE])
	{
		g_RenderMode = !g_RenderMode;			// Change the rendering mode

		// Change the rendering mode to and from lines or triangles
		if(g_RenderMode) 				
		{
			// Render the triangles in fill mode		
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);	
		}
		else 
		{
			// Render the triangles in wire frame mode
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);	
		}

		keys[VK_SPACE] = false;
	}
}

//void findRoomsWithThisPortal(tPortal& portal, vector<int>& idRoomsWithThisPortal)
//{
//	vector<CRoom*>* rooms = sceneManager->getRooms();
//
//	for(int indRoom = 0; indRoom < rooms->size(); indRoom++)
//	{
//		vector<tPortal>* roomPortals = (*rooms)[indRoom]->getPortals();
//
//		for(int indPortal = 0; indPortal < (*roomPortals).size(); indPortal++)
//		{
//			if(portal.idPortal == (*roomPortals)[indPortal].idPortal)
//			{
//				idRoomsWithThisPortal.push_back((*rooms)[indRoom]->getId());
//			}
//		}
//	}
//}

//void rec_findRoomsToDraw(int roomID)
//{
//	//data of this room-----------------------------------------
//	CRoom* roomWithCam = sceneManager->getRoom(roomID);
//	vector<tPortal>* roomPortals = (*roomWithCam).getPortals();
//	//-----------------------------------------------------------
//	g_idRoomsNeedDraw.push_back(roomID);//draw this room
//
//	vector<tPortal> portalsInsideViewFrust;
//
//	//go throw all portals and find portals which are inside view-frustym
//	for(int indPortal = 0; indPortal < (*roomPortals).size(); indPortal++)
//	{
//		//if the portal inside the view-frustum
//		if(g_bIgnoreFrustum || g_Frustum.PortalInFrustum((*roomPortals)[indPortal]))
//		{
//			portalsInsideViewFrust.push_back((*roomPortals)[indPortal]);
//		}
//	}
//
//	vector<int> idRoomsWithThisPortals;
//	//go throw portals which are inside the view-frustym
//	for(int indPortal = 0; indPortal < portalsInsideViewFrust.size(); indPortal++)
//	{
//		findRoomsWithThisPortal(portalsInsideViewFrust[indPortal], idRoomsWithThisPortals);
//	}
//
//	for(int indIdRoom = 0; indIdRoom < idRoomsWithThisPortals.size(); indIdRoom++)
//	{
//		//only ncontinue recursion function if there is no rooms with this id
//		if(idRoomsWithThisPortals[indIdRoom] != roomID)
//		{
//			bool continueRec = true;
//
//			for(int indRoomNeedDraw = 0; indRoomNeedDraw < g_idRoomsNeedDraw.size(); indRoomNeedDraw++)
//			{
//				if(idRoomsWithThisPortals[indIdRoom] == g_idRoomsNeedDraw[indRoomNeedDraw])
//				{
//					continueRec = false;
//				}
//			}
//
//			if(continueRec)
//			{
//				rec_findRoomsToDraw(idRoomsWithThisPortals[indIdRoom]);
//			}
//		}
//	}
//}

void update()
{
	if(g_bCameraOn)
	{
		g_Camera.Update();
	}

	switch(USE_FRUSTUM)
	{
	case 1:
		{
			sceneManager->calculateIdNodeWithCamera(g_Camera.Position());//id node with camera
			sceneManager->claculateCamNodeIsRoom();//is camera inside the room
			g_Frustum.eraseIdRoomNeedDraw();
			//if camera in some room
			if(sceneManager->getIsRoomNodeWithCamera())
			{
				g_Frustum.CalculateFrustum();
				int idNodeWithCam = sceneManager->getIdNodeWithCam();
				g_Frustum.rec_findRoomsToDraw(idNodeWithCam);
			}
			break;
		}
	case 2:
		{
			g_Frustum2->update();
			sceneManager->calculateIdNodeWithCamera(g_Frustum2->getPosition());//id node with camera
			sceneManager->claculateCamNodeIsRoom();//is camera inside the room
			//cout << "node with camera = " << sceneManager->getIdNodeWithCam() << endl;
			//cout << "is node in room = " << sceneManager->getIsRoomNodeWithCamera() << endl;
			g_Frustum2->eraseIdRoomNeedDraw();
			//if camera in some room
			if(sceneManager->getIsRoomNodeWithCamera())
			{
				int idNodeWithCam = sceneManager->getIdNodeWithCam();
				g_Frustum2->rec_findRoomsToDraw(idNodeWithCam);
			}
			break;
		}
	default:
		MessageBox(NULL,"There is no this type of frustum!","ERROR",MB_OK|MB_ICONEXCLAMATION);
	}
}

int WINAPI WinMain(	HINSTANCE	hInstance,			// Instance
					HINSTANCE	hPrevInstance,		// Previous Instance
					LPSTR		lpCmdLine,			// Command Line Parameters
					int			nCmdShow)			// Window Show State
{
	if(!initEngineWindow("OpenGL", SCREEN_WIDTH, SCREEN_HEIGHT, 32, IS_FULL_SCREEN))
	{
		return 0;	
	}

	regInitFunc(init);
	regDisplayFunc(display);
	regProcessKeysFunc(processKeys);
	regUpdateFunc(update);

	mainLoop();

	return (int)(msg.wParam);// Exit The Program
}