#pragma once
#include <vector>
#include "../3dStruct/3DModel.h"

class CFace
{
public:
	unsigned int m_vertIndex[3];
	unsigned int m_normalIndex[3];
	unsigned int m_texIndex[3];

	int matId;

	CFace(unsigned int vert[3], unsigned int text[3], unsigned int norm[3], int newMatId)
	{

	}
};

class CMaterial
{
public:
	char m_matName[255];
	float m_ambientCol[3];
	float m_diffuseCol[3];
	float m_specCol[3];
	float m_d;
	float m_ns;
	int m_illum;
	char m_textureName[255];
	BYTE  m_color[3];

	//Created
	int m_glIndex;
};

class myOBJLoader
{
public:
	myOBJLoader();

	/////////////////////////////////////////////////////////////////////
	std::vector<Vector3d>	m_vVertices;
	std::vector<Vector3d>	m_vNormals;
	std::vector<Vector3d>	m_vTexCoords;
	std::vector<CFace>		m_vFaces;
	std::vector<CMaterial>	m_vMats;

	//Pointer to objects to cut down on operator[] use with std::vector
	Vector3d*				m_pVerts;
	Vector3d*				m_pNormals;
	Vector3d*				m_pTexCoords;
	CFace*					m_pFaces;

	bool					m_bHasTexCoords;
	bool					m_bHasNormals;
	//////////////////////////////////////////////////////////////////////
};