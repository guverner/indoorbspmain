#include "tFrustum.h"

enum FrustumSide
{
	RIGHT	= 0,		// The RIGHT side of the frustum
	LEFT	= 1,		// The LEFT	 side of the frustum
	BOTTOM	= 2,		// The BOTTOM side of the frustum
	TOP		= 3,		// The TOP side of the frustum
	BACK	= 4,		// The BACK	side of the frustum
	FRONT	= 5			// The FRONT side of the frustum
};

enum PlaneData
{
	A = 0,				// The X value of the plane's normal
	B = 1,				// The Y value of the plane's normal
	C = 2,				// The Z value of the plane's normal
	D = 3				// The distance the plane is from the origin
};

tFrustum::tFrustum(Vector3d& camPos, Vector3d& camViewPos, float zNearDist,
				float zFarDist, Vector3d& upVector, float fov, float aspectRation)
{
	m_camPos = camPos;
	m_camViewPos = camViewPos;
	m_zNearDist = zNearDist;
	m_zFarDist = zFarDist;
	m_upVector = Vector3d::normalize(upVector);
	m_fov = fov;
	m_aspectRation = aspectRation;

	//Vector3d viewDir(Vector3d::normalize(camViewPos - camPos));
	//m_rightVector = viewDir * m_upVector;

	Vector3d viewDir(m_camViewPos - m_camPos);
	Vector3d rightDir(Vector3d::normalize(viewDir * m_upVector));

	m_hNear = 2 * tanf(m_fov / 2) * m_zNearDist;
	m_wNear = m_hNear * aspectRation;
	m_hFar = 2 * tanf(m_fov / 2) * m_zFarDist;
	m_wFar = m_hFar * aspectRation;

	//for far vertices
	m_farPlaneCenter = m_camPos + viewDir * m_zFarDist;
	m_farPlaneVerts[0] = m_farPlaneCenter + (m_upVector * m_hFar / 2) - (rightDir * m_wFar / 2);
	m_farPlaneVerts[1] = m_farPlaneCenter + (m_upVector * m_hFar / 2) + (rightDir * m_wFar / 2);
	m_farPlaneVerts[2] = m_farPlaneCenter - (m_upVector * m_hFar / 2) + (rightDir * m_wFar / 2);
	m_farPlaneVerts[3] = m_farPlaneCenter - (m_upVector * m_hFar / 2) - (rightDir * m_wFar / 2);

	//for near vertices
	m_nearPlaneCenter = m_camPos + viewDir * m_zNearDist;
	m_nearPlaneVerts[0] = m_nearPlaneCenter + (m_upVector * m_hNear / 2) - (rightDir * m_wNear / 2);
	m_nearPlaneVerts[1] = m_nearPlaneCenter + (m_upVector * m_hNear / 2) + (rightDir * m_wNear / 2);
	m_nearPlaneVerts[2] = m_nearPlaneCenter - (m_upVector * m_hNear / 2) + (rightDir * m_wNear / 2);
	m_nearPlaneVerts[3] = m_nearPlaneCenter - (m_upVector * m_hNear / 2) - (rightDir * m_wNear / 2);

	m_sceneManager = NULL;
	m_isIgnoreFrustum = false;
}

void tFrustum::update()
{
	checkForMovement();
}

void tFrustum::checkForMovement()
{
	const float SPEED = 0.7;

	updateCenterPosAndCenterView(SPEED);

	updateCornerVertices();
}

void tFrustum::updateCornerVertices()
{
	Vector3d viewDir(m_camViewPos - m_camPos);
	Vector3d rightDir(Vector3d::normalize(viewDir * m_upVector));

	//for far vertices
	m_farPlaneCenter = m_camPos + viewDir * m_zFarDist;
	m_farPlaneVerts[0] = m_farPlaneCenter + (m_upVector * m_hFar / 2) - (rightDir * m_wFar / 2);
	m_farPlaneVerts[1] = m_farPlaneCenter + (m_upVector * m_hFar / 2) + (rightDir * m_wFar / 2);
	m_farPlaneVerts[2] = m_farPlaneCenter - (m_upVector * m_hFar / 2) + (rightDir * m_wFar / 2);
	m_farPlaneVerts[3] = m_farPlaneCenter - (m_upVector * m_hFar / 2) - (rightDir * m_wFar / 2);

	//for near vertices
	m_nearPlaneCenter = m_camPos + viewDir * m_zNearDist;
	m_nearPlaneVerts[0] = m_nearPlaneCenter + (m_upVector * m_hNear / 2) - (rightDir * m_wNear / 2);
	m_nearPlaneVerts[1] = m_nearPlaneCenter + (m_upVector * m_hNear / 2) + (rightDir * m_wNear / 2);
	m_nearPlaneVerts[2] = m_nearPlaneCenter - (m_upVector * m_hNear / 2) + (rightDir * m_wNear / 2);
	m_nearPlaneVerts[3] = m_nearPlaneCenter - (m_upVector * m_hNear / 2) - (rightDir * m_wNear / 2);
}

void tFrustum::updateCenterPosAndCenterView(float speed)
{
	if(keys[VK_NUMPAD8])
	{
		Vector3d viewDir(Vector3d::normalize(m_camViewPos - m_camPos));//view vector

		m_camPos.x += viewDir.x * speed;
		//m_camPos.y += viewDir.y * speed;
		m_camPos.z += viewDir.z * speed;
		m_camViewPos.x += viewDir.x * speed;
		//m_camViewPos.y += viewDir.y * speed;
		m_camViewPos.z += viewDir.z * speed;
	}
	else if(keys[VK_NUMPAD2])
	{
		Vector3d viewDir(Vector3d::normalize(m_camViewPos - m_camPos));//view vector

		m_camPos.x += viewDir.x * -speed;
		//m_camPos.y += viewDir.y * -speed;
		m_camPos.z += viewDir.z * -speed;
		m_camViewPos.x += viewDir.x * -speed;
		//m_camViewPos.y += viewDir.y * -speed;
		m_camViewPos.z += viewDir.z * -speed;
	}

	if(keys[VK_NUMPAD4])
	{
		Vector3d viewDir(m_camViewPos - m_camPos);//view vector
		Vector3d rightDir(Vector3d::normalize(viewDir * m_upVector));

		m_camPos.x += rightDir.x * -speed;
		//m_camPos.y += rightDir.y * -speed;
		m_camPos.z += rightDir.z * -speed;
		m_camViewPos.x += rightDir.x * -speed;
		//m_camViewPos.y += rightDir.y * -speed;
		m_camViewPos.z += rightDir.z * -speed;
	}
	else if(keys[VK_NUMPAD6])
	{
		Vector3d viewDir(Vector3d::normalize(m_camViewPos - m_camPos));//view vector
		Vector3d rightDir(Vector3d::normalize(viewDir * m_upVector));

		m_camPos.x += rightDir.x * speed;
		//m_camPos.y += rightDir.y * speed;
		m_camPos.z += rightDir.z * speed;
		m_camViewPos.x += rightDir.x * speed;
		//m_camViewPos.y += rightDir.y * speed;
		m_camViewPos.z += rightDir.z * speed;
	}

	const float ROTATE_ANGLE = 0.01f;

	//rotation
	if(keys[VK_NUMPAD7])//left
	{
		rotateView(ROTATE_ANGLE, 0, 1, 0);
	}
	else if(keys[VK_NUMPAD9])//right
	{
		rotateView(-ROTATE_ANGLE, 0, 1, 0);
	}

	//rotation
	if(keys[VK_ADD])//up
	{
		Vector3d viewDir(Vector3d::normalize(m_camViewPos - m_camPos));//view vector
		Vector3d rightDir(Vector3d::normalize(viewDir * m_upVector));

		rotateView(ROTATE_ANGLE, rightDir.x, rightDir.y, rightDir.z);
	}
	else if(keys[VK_SUBTRACT])//down
	{
		Vector3d viewDir(Vector3d::normalize(m_camViewPos - m_camPos));//view vector
		Vector3d rightDir(Vector3d::normalize(viewDir * m_upVector));

		rotateView(-ROTATE_ANGLE, rightDir.x, rightDir.y, rightDir.z);
	}
}

void tFrustum::draw()
{
	const int NUM_VERTS_PER_PLANE = 4;

	glDisable(GL_LIGHTING);
	glPointSize(16);
	glLineWidth(10);

	//frustum position
	glColor3f(1,1,0);

	glBegin(GL_POINTS);
		glVertex3f(m_camPos.x, m_camPos.y, m_camPos.z);
	glEnd();

	glColor3f(0,1,0);

	//draw far plane
	glBegin(GL_LINE_LOOP);
		for(int indVertex = 0; indVertex < NUM_VERTS_PER_PLANE; indVertex++)
		{
			glVertex3f(m_farPlaneVerts[indVertex].x,
						m_farPlaneVerts[indVertex].y,
						m_farPlaneVerts[indVertex].z);
		}
	glEnd();

	//draw near plane
	glBegin(GL_LINE_LOOP);
		for(int indVertex = 0; indVertex < NUM_VERTS_PER_PLANE; indVertex++)
		{
			glVertex3f(m_nearPlaneVerts[indVertex].x,
						m_nearPlaneVerts[indVertex].y,
						m_nearPlaneVerts[indVertex].z);
		}
	glEnd();

	//draw top plane
	glBegin(GL_LINE_LOOP);
		glVertex3f(m_farPlaneVerts[0].x,
					m_farPlaneVerts[0].y,
					m_farPlaneVerts[0].z);
		glVertex3f(m_farPlaneVerts[1].x,
					m_farPlaneVerts[1].y,
					m_farPlaneVerts[1].z);
		glVertex3f(m_nearPlaneVerts[1].x,
					m_nearPlaneVerts[1].y,
					m_nearPlaneVerts[1].z);
		glVertex3f(m_nearPlaneVerts[0].x,
					m_nearPlaneVerts[0].y,
					m_nearPlaneVerts[0].z);
	glEnd();

	//draw bottom plane
	glBegin(GL_LINE_LOOP);
		glVertex3f(m_farPlaneVerts[3].x,
					m_farPlaneVerts[3].y,
					m_farPlaneVerts[3].z);
		glVertex3f(m_farPlaneVerts[2].x,
					m_farPlaneVerts[2].y,
					m_farPlaneVerts[2].z);
		glVertex3f(m_nearPlaneVerts[2].x,
					m_nearPlaneVerts[2].y,
					m_nearPlaneVerts[2].z);
		glVertex3f(m_nearPlaneVerts[3].x,
					m_nearPlaneVerts[3].y,
					m_nearPlaneVerts[3].z);
	glEnd();

	glColor3f(1,1,1);
	glLineWidth(1);
	glPointSize(1);
	glEnable(GL_LIGHTING);
}

void tFrustum::rotateView(float angle, float x, float y, float z)
{
	Vector3d newViewDir;

	//view vector (The direction we are facing)
	Vector3d viewDir = m_camViewPos - m_camPos;	

	// Calculate the sine and cosine of the angle once
	float cosTheta = (float)cos(angle);
	float sinTheta = (float)sin(angle);

	// Find the new x position for the new rotated point
	newViewDir.x  = (cosTheta + (1 - cosTheta) * x * x)		* viewDir.x;
	newViewDir.x += ((1 - cosTheta) * x * y - z * sinTheta)	* viewDir.y;
	newViewDir.x += ((1 - cosTheta) * x * z + y * sinTheta)	* viewDir.z;

	// Find the new y position for the new rotated point
	newViewDir.y  = ((1 - cosTheta) * x * y + z * sinTheta)	* viewDir.x;
	newViewDir.y += (cosTheta + (1 - cosTheta) * y * y)		* viewDir.y;
	newViewDir.y += ((1 - cosTheta) * y * z - x * sinTheta)	* viewDir.z;

	// Find the new z position for the new rotated point
	newViewDir.z  = ((1 - cosTheta) * x * z - y * sinTheta)	* viewDir.x;
	newViewDir.z += ((1 - cosTheta) * y * z + x * sinTheta)	* viewDir.y;
	newViewDir.z += (cosTheta + (1 - cosTheta) * z * z)		* viewDir.z;

	m_camViewPos.x = m_camPos.x + newViewDir.x;
	m_camViewPos.y = m_camPos.y + newViewDir.y;
	m_camViewPos.z = m_camPos.z + newViewDir.z;
}

bool tFrustum::PortalInFrustum( tPortal& portal )
{
	tPlane plane = portal.plain;

	for(int indVertex = 0; indVertex < NUM_VERT_IN_PORTAL; indVertex++)
	{
		if(this->PointInFrustum(plane.vertices[indVertex].x,
							plane.vertices[indVertex].y,
							plane.vertices[indVertex].z))
		{
			return true;
		}
	}

	//if there is no portal's point which is inside the view-frustum,
	//need to check check plane-plane intersection
	//Some code should be here!!!

	Vector3d leftLine = m_farPlaneVerts[0] - m_nearPlaneVerts[0];//Left line
	Vector3d rightLine = m_farPlaneVerts[1] - m_nearPlaneVerts[1];//Right plane

	//get bottom boints of the portals
	Vector3d* portalVertices = plane.getVertices();
	int indsPortalBottomVerts[2];//indices of the portal's bottom vertices
	indsPortalBottomVerts[0] = 0;
	indsPortalBottomVerts[1] = -1;

	for(int indPortalBottomVert = 1; indPortalBottomVert < NUM_VERT_IN_PORTAL; indPortalBottomVert++)
	{
		if(portalVertices[indPortalBottomVert].y < portalVertices[indsPortalBottomVerts[0]].y)
		{
			indsPortalBottomVerts[0] = indPortalBottomVert;
			indsPortalBottomVerts[1] = indsPortalBottomVerts[0];
		}
	}
	//if second index == -1 it means that first index was first vertex of the portal
	if(indsPortalBottomVerts[1] == -1)
	{
		indsPortalBottomVerts[1] = 1;

		for(int indPortalBottomVert = 2; indPortalBottomVert < NUM_VERT_IN_PORTAL; indPortalBottomVert++)
		{
			if(portalVertices[indPortalBottomVert].y < portalVertices[indsPortalBottomVerts[1]].y)
			{
				indsPortalBottomVerts[1] = indPortalBottomVert;
			}
		}
	}

	Vector3d bottomPortalVert1 = portalVertices[indsPortalBottomVerts[0]];
	Vector3d bottomPortalVert2 = portalVertices[indsPortalBottomVerts[1]];

	//Also need to chack back plane of the view frustum
		//Far plane
		Vector3d v = m_farPlaneVerts[1] - m_farPlaneVerts[2];
		Vector3d u = m_farPlaneVerts[3] - m_farPlaneVerts[2];
		Vector3d normalFar = Vector3d::normalize(v * u);
		float Dfar = Vector3d::dotProduct(-normalFar, m_farPlaneVerts[2]);

	//left plane (line)
	if(((onLeftForWall(m_nearPlaneVerts[0], m_farPlaneVerts[0], bottomPortalVert1) == 1 &&
		!onLeftForWall(m_nearPlaneVerts[0], m_farPlaneVerts[0], bottomPortalVert2) == 1) ||
		!(onLeftForWall(m_nearPlaneVerts[0], m_farPlaneVerts[0], bottomPortalVert1) == 1 &&
		onLeftForWall(m_nearPlaneVerts[0], m_farPlaneVerts[0], bottomPortalVert2) == 1)) &&
		!((onLeftForWall(m_nearPlaneVerts[1], m_farPlaneVerts[1], bottomPortalVert1) == 1 &&
		!onLeftForWall(m_nearPlaneVerts[1], m_farPlaneVerts[1], bottomPortalVert2) == 1) ||
		!(onLeftForWall(m_nearPlaneVerts[1], m_farPlaneVerts[1], bottomPortalVert1) == 1 &&
		onLeftForWall(m_nearPlaneVerts[1], m_farPlaneVerts[1], bottomPortalVert2) == 1)) &&
		(onLeftForWall(m_farPlaneVerts[0], m_farPlaneVerts[1], bottomPortalVert1) == 2 &&
		onLeftForWall(m_farPlaneVerts[0], m_farPlaneVerts[1], bottomPortalVert2) == 2)/* &&
		(onLeftForWall(m_nearPlaneVerts[0], m_nearPlaneVerts[1], bottomPortalVert1) == 2 &&
		onLeftForWall(m_nearPlaneVerts[0], m_nearPlaneVerts[1], bottomPortalVert2) == 2)*/)
	{
		return true;
	}
	//rigth plane (line)
	if( ((onLeftForWall(m_nearPlaneVerts[1], m_farPlaneVerts[1], bottomPortalVert1) == 2 &&
		!onLeftForWall(m_nearPlaneVerts[1], m_farPlaneVerts[1], bottomPortalVert2) == 2) ||
		!(onLeftForWall(m_nearPlaneVerts[1], m_farPlaneVerts[1], bottomPortalVert1) == 2 &&
		onLeftForWall(m_nearPlaneVerts[1], m_farPlaneVerts[1], bottomPortalVert2) == 2)) && 
		!((onLeftForWall(m_nearPlaneVerts[0], m_farPlaneVerts[0], bottomPortalVert1) == 2 &&
		!onLeftForWall(m_nearPlaneVerts[0], m_farPlaneVerts[0], bottomPortalVert2) == 2) ||
		!(onLeftForWall(m_nearPlaneVerts[0], m_farPlaneVerts[0], bottomPortalVert1) == 2 &&
		onLeftForWall(m_nearPlaneVerts[0], m_farPlaneVerts[0], bottomPortalVert2) == 2)) &&
		(onLeftForWall(m_farPlaneVerts[0], m_farPlaneVerts[1], bottomPortalVert1) == 2 &&
		onLeftForWall(m_farPlaneVerts[0], m_farPlaneVerts[1], bottomPortalVert2) == 2) /*&&
		(onLeftForWall(m_nearPlaneVerts[0], m_nearPlaneVerts[1], bottomPortalVert1) == 2 &&
		onLeftForWall(m_nearPlaneVerts[0], m_nearPlaneVerts[1], bottomPortalVert2) == 2)*/)
	{
		return true;
	}

	//if view frustum is between the vertical edges of the portal
	if(((onLeftForWall(this->getViewPoint(), this->getPosition(), bottomPortalVert1) == 1 &&
		onLeftForWall(this->getViewPoint(), this->getPosition(), bottomPortalVert2) == 2) ||
		(onLeftForWall(this->getViewPoint(), this->getPosition(), bottomPortalVert1) == 2 &&
		onLeftForWall(this->getViewPoint(), this->getPosition(), bottomPortalVert2) == 1)) &&
		((onLeftForWall(m_farPlaneVerts[0], m_farPlaneVerts[1], bottomPortalVert1) == 2 &&
		onLeftForWall(m_farPlaneVerts[0], m_farPlaneVerts[1], bottomPortalVert2) == 2)) &&
		
		(onLeftForWall(m_nearPlaneVerts[1], m_nearPlaneVerts[0], bottomPortalVert1) == 2 &&
		onLeftForWall(m_nearPlaneVerts[1], m_nearPlaneVerts[0], bottomPortalVert2) == 2) //&&
		
		//(((onLeftForWall(m_nearPlaneVerts[1], m_farPlaneVerts[1], bottomPortalVert1) == 2 &&
		//!onLeftForWall(m_nearPlaneVerts[1], m_farPlaneVerts[1], bottomPortalVert2) == 2) ||
		//!(onLeftForWall(m_nearPlaneVerts[1], m_farPlaneVerts[1], bottomPortalVert1) == 2 &&
		//onLeftForWall(m_nearPlaneVerts[1], m_farPlaneVerts[1], bottomPortalVert2) == 2)) && 
		//!((onLeftForWall(m_nearPlaneVerts[0], m_farPlaneVerts[0], bottomPortalVert1) == 2 &&
		//!onLeftForWall(m_nearPlaneVerts[0], m_farPlaneVerts[0], bottomPortalVert2) == 2) ||
		//!(onLeftForWall(m_nearPlaneVerts[0], m_farPlaneVerts[0], bottomPortalVert1) == 2 &&
		//onLeftForWall(m_nearPlaneVerts[0], m_farPlaneVerts[0], bottomPortalVert2) == 2)) ||

		//((onLeftForWall(m_nearPlaneVerts[0], m_farPlaneVerts[0], bottomPortalVert1) == 1 &&
		//!onLeftForWall(m_nearPlaneVerts[0], m_farPlaneVerts[0], bottomPortalVert2) == 1) ||
		//!(onLeftForWall(m_nearPlaneVerts[0], m_farPlaneVerts[0], bottomPortalVert1) == 1 &&
		//onLeftForWall(m_nearPlaneVerts[0], m_farPlaneVerts[0], bottomPortalVert2) == 1)) &&
		//!((onLeftForWall(m_nearPlaneVerts[1], m_farPlaneVerts[1], bottomPortalVert1) == 1 &&
		//!onLeftForWall(m_nearPlaneVerts[1], m_farPlaneVerts[1], bottomPortalVert2) == 1) ||
		//!(onLeftForWall(m_nearPlaneVerts[1], m_farPlaneVerts[1], bottomPortalVert1) == 1 &&
		//onLeftForWall(m_nearPlaneVerts[1], m_farPlaneVerts[1], bottomPortalVert2) == 1))
		//)
		)
	{
		return true;
	}


	////Left plane
	//Vector3d normalLeft = -Vector3d::normalize((m_farPlaneVerts[0] - m_farPlaneVerts[3]) *
	//											(m_farPlaneVerts[3] - m_nearPlaneVerts[3]));
	//float leftD = normalLeft.x * m_nearPlaneVerts[3].x + normalLeft.y * m_nearPlaneVerts[3].y +
	//				normalLeft.z * m_nearPlaneVerts[3].z;
	////Right plane
	//Vector3d normalRight = -Vector3d::normalize((m_farPlaneVerts[1] - m_farPlaneVerts[2]) * 
	//											(m_nearPlaneVerts[2] - m_farPlaneVerts[2]));
	//float rightD = normalRight.x * m_farPlaneVerts[2].x + normalRight.y * m_farPlaneVerts[2].y +
	//				normalRight.z * m_farPlaneVerts[2].z;

	////get bottom boints of the portals
	//Vector3d* portalVertices = plane.getVertices();
	//int indsPortalBottomVerts[2];//indices of the portal's bottom vertices
	//indsPortalBottomVerts[0] = 0;1
	//indsPortalBottomVerts[1] = -1;

	//for(int indPortalBottomVert = 1; indPortalBottomVert < NUM_VERT_IN_PORTAL; indPortalBottomVert++)
	//{
	//	if(portalVertices[indPortalBottomVert].y < portalVertices[indsPortalBottomVerts[0]].y)
	//	{
	//		indsPortalBottomVerts[0] = indPortalBottomVert;
	//		indsPortalBottomVerts[1] = indsPortalBottomVerts[0];
	//	}
	//}
	////if second index == -1 it means that first index was first vertex of the portal
	//if(indsPortalBottomVerts[1] == -1)
	//{
	//	indsPortalBottomVerts[1] = 1;

	//	for(int indPortalBottomVert = 2; indPortalBottomVert < NUM_VERT_IN_PORTAL; indPortalBottomVert++)
	//	{
	//		if(portalVertices[indPortalBottomVert].y < portalVertices[indsPortalBottomVerts[1]].y)
	//		{
	//			indsPortalBottomVerts[1] = indPortalBottomVert;
	//		}
	//	}
	//}

	////now need to check left plane of the view frustym with two bottom vertices of the portal and then right plane
	//float distLeft1 = (Vector3d::dotProduct(normalLeft, portalVertices[indsPortalBottomVerts[0]]) - leftD) /
	//					(normalLeft.size());
	//float distLeft2 = (Vector3d::dotProduct(normalLeft, portalVertices[indsPortalBottomVerts[1]]) - leftD) /
	//					(normalLeft.size());
	////cout << "Left:" << endl;
	////cout << "distLeft1 = " << distLeft1 << endl;
	////cout << "distLeft2 = " << distLeft2 << endl;
	//if((distLeft1 >= 0 && distLeft2 < 0) || (distLeft1 < 0 && distLeft2 >= 0))
	//{
	//	return true;
	//}

	//float distRight1 = (Vector3d::dotProduct(normalRight, portalVertices[indsPortalBottomVerts[0]]) + rightD) /
	//					(normalRight.size());
	//float distRight2 = (Vector3d::dotProduct(normalRight, portalVertices[indsPortalBottomVerts[1]]) + rightD) /
	//					(normalRight.size());
	////cout << "Right:" << endl;
	////cout << "distRight1 = " << distRight1 << endl;
	////cout << "distRight2 = " << distRight2 << endl;
	///*if((distRight1 >= 0 && distRight2 < 0) || (distRight1 < 0 && distRight2 >= 0))
	//{
	//	return true;
	//}

	//if((distRight1 >= 0 && distRight2 >= 0 && distLeft1 < 0 && distLeft2 < 0) ||
	//	(distRight1 < 0 && distRight2 < 0 && distLeft1 >= 0 && distLeft2 >= 0))
	//{
	//	return true;
	//}*/

	return false;
}

//parameters - values of the portal's vertice
bool tFrustum::PointInFrustum( float x, float y, float z )
{
	Vector3d dirVect(this->getViewPoint() - this->getPosition());
	Vector3d right(this->getUpVector() * dirVect);

	Vector3d v, u, a;

	//Near plane
	v = m_nearPlaneVerts[1] - m_nearPlaneVerts[2];
	u = m_nearPlaneVerts[3] - m_nearPlaneVerts[2];
	Vector3d normalNear = Vector3d::normalize(-(v * u));
	float Dnear = Vector3d::dotProduct(-normalNear, m_nearPlaneVerts[2]);

	//Far plane
	v = m_farPlaneVerts[1] - m_farPlaneVerts[2];
	u = m_farPlaneVerts[3] - m_farPlaneVerts[2];
	Vector3d normalFar = Vector3d::normalize(v * u);
	float Dfar = Vector3d::dotProduct(-normalFar, m_farPlaneVerts[2]);

	//Left plane
	Vector3d normalLeft = -Vector3d::normalize((m_farPlaneVerts[0] - m_farPlaneVerts[3]) *
												(m_farPlaneVerts[3] - m_nearPlaneVerts[3]));
	float Dleft = normalLeft.x * m_nearPlaneVerts[3].x + normalLeft.y * m_nearPlaneVerts[3].y +
					normalLeft.z * m_nearPlaneVerts[3].z;

	//Right plane
	Vector3d normalRight = -Vector3d::normalize((m_farPlaneVerts[1] - m_farPlaneVerts[2]) * 
												(m_nearPlaneVerts[2] - m_farPlaneVerts[2]));
	float Dright = normalRight.x * m_farPlaneVerts[2].x + normalRight.y * m_farPlaneVerts[2].y +
					normalRight.z * m_farPlaneVerts[2].z;

	//Top plane
	Vector3d normalTop = -Vector3d::normalize((m_farPlaneVerts[0] - m_farPlaneVerts[1]) *
											(m_nearPlaneVerts[1] - m_farPlaneVerts[1]));
	float Dtop = normalTop.x * m_farPlaneVerts[1].x + normalTop.y * m_farPlaneVerts[1].y +
					normalTop.z * m_farPlaneVerts[1].z;

	//Bottom plane
	Vector3d normalBottom = Vector3d::normalize((m_farPlaneVerts[3] - m_farPlaneVerts[2]) *
												(m_nearPlaneVerts[2] - m_farPlaneVerts[2]));
	float Dbottom = normalBottom.x * m_farPlaneVerts[2].x + normalBottom.y * m_farPlaneVerts[2].y +
					normalBottom.z * m_farPlaneVerts[2].z;

	//assign to the array--------------------------
	float m_Frustum[6][4];
	m_Frustum[FRONT][A] = normalNear.x; m_Frustum[FRONT][B] = normalNear.y;
	m_Frustum[FRONT][C] = normalNear.z; m_Frustum[FRONT][D] = Dnear;
	m_Frustum[BACK][A] = normalFar.x; m_Frustum[BACK][B] = normalFar.y;
	m_Frustum[BACK][C] = normalFar.z; m_Frustum[BACK][D] = Dfar;
	m_Frustum[RIGHT][A] = normalRight.x; m_Frustum[RIGHT][B] = normalRight.y;
	m_Frustum[RIGHT][C] = normalRight.z; m_Frustum[RIGHT][D] = -Dright;
	m_Frustum[LEFT][A] = normalLeft.x; m_Frustum[LEFT][B] = normalLeft.y;
	m_Frustum[LEFT][C] = normalLeft.z; m_Frustum[LEFT][D] = -Dleft;
	m_Frustum[TOP][A] = normalTop.x; m_Frustum[TOP][B] = normalTop.y;
	m_Frustum[TOP][C] = normalTop.z; m_Frustum[TOP][D] = -Dtop;
	m_Frustum[BOTTOM][A] = normalBottom.x; m_Frustum[BOTTOM][B] = normalBottom.y;
	m_Frustum[BOTTOM][C] = normalBottom.z; m_Frustum[BOTTOM][D] = -Dbottom;
	//---------------------------------------------

	// Go through all the sides of the frustum
	for(int i = 0; i < 5; i++ )
	{
		// Calculate the plane equation and check if the point is behind a side of the frustum
		if(m_Frustum[i][A] * x + m_Frustum[i][B] * y + m_Frustum[i][C] * z + m_Frustum[i][D] <= 0)
		{
			// The point was behind a side, so it ISN'T in the frustum
			return false;
		}
	}

	// The point was inside of the frustum (In front of ALL the sides of the frustum)
	return true;
}

Vector3d tFrustum::getPosition()
{
	return this->m_camPos;
}

void tFrustum::setSceneManager(cSceneManager* sceneManager_)
{
	this->m_sceneManager = sceneManager_;
}

void tFrustum::setIsIgnoreFrustum(bool value)
{
	this->m_isIgnoreFrustum = value;
}

void tFrustum::eraseIdRoomNeedDraw()
{
	//delete all rooms from it
	if(m_idRoomsNeedDraw.size() > 0)
	{
		m_idRoomsNeedDraw.erase(m_idRoomsNeedDraw.begin(), m_idRoomsNeedDraw.end());
	}
}

void tFrustum::rec_findRoomsToDraw(int roomID)
{
	//data of this room-----------------------------------------
	CRoom* roomWithCam = m_sceneManager->getRoom(roomID);
	vector<tPortal>* roomPortals = (*roomWithCam).getPortals();
	//-----------------------------------------------------------
	m_idRoomsNeedDraw.push_back(roomID);//draw this room

	vector<tPortal> portalsInsideViewFrust;

	//go throw all portals and find portals which are inside view-frustym
	for(int indPortal = 0; indPortal < (*roomPortals).size(); indPortal++)
	{
		//if the portal inside the view-frustum
		if(m_isIgnoreFrustum || this->PortalInFrustum((*roomPortals)[indPortal]))
		{
			portalsInsideViewFrust.push_back((*roomPortals)[indPortal]);
		}
	}

	vector<int> idRoomsWithThisPortals;
	//go throw portals which are inside the view-frustym
	for(int indPortal = 0; indPortal < portalsInsideViewFrust.size(); indPortal++)
	{
		findRoomsWithThisPortal(portalsInsideViewFrust[indPortal], idRoomsWithThisPortals);
	}

	for(int indIdRoom = 0; indIdRoom < idRoomsWithThisPortals.size(); indIdRoom++)
	{
		//only ncontinue recursion function if there is no rooms with this id
		if(idRoomsWithThisPortals[indIdRoom] != roomID)
		{
			bool continueRec = true;

			for(int indRoomNeedDraw = 0; indRoomNeedDraw < m_idRoomsNeedDraw.size(); indRoomNeedDraw++)
			{
				if(idRoomsWithThisPortals[indIdRoom] == m_idRoomsNeedDraw[indRoomNeedDraw])
				{
					continueRec = false;
				}
			}

			if(continueRec)
			{
				rec_findRoomsToDraw(idRoomsWithThisPortals[indIdRoom]);
			}
		}
	}
}

void tFrustum::findRoomsWithThisPortal(tPortal& portal, vector<int>& idRoomsWithThisPortal)
{
	vector<CRoom*>* rooms = m_sceneManager->getRooms();

	for(int indRoom = 0; indRoom < rooms->size(); indRoom++)
	{
		vector<tPortal>* roomPortals = (*rooms)[indRoom]->getPortals();

		for(int indPortal = 0; indPortal < (*roomPortals).size(); indPortal++)
		{
			if(portal.idPortal == (*roomPortals)[indPortal].idPortal)
			{
				idRoomsWithThisPortal.push_back((*rooms)[indRoom]->getId());
			}
		}
	}
}

vector<int>* tFrustum::getIdRoomsNeedDraw()
{
	return &m_idRoomsNeedDraw;
}

Vector3d* tFrustum::getNearPlanesVerts()
{
	return m_nearPlaneVerts;
}

Vector3d* tFrustum::getFarPlanesVerts()
{
	return m_farPlaneVerts;
}

Vector3d* tFrustum::getNearPlaneCenter()
{
	return &m_nearPlaneCenter;
}

Vector3d* tFrustum::getFarPlaneCenter()
{
	return &m_farPlaneCenter;
}

Vector3d tFrustum::getViewPoint()
{
	return this->m_camViewPos;
}

Vector3d tFrustum::getUpVector()
{
	return this->m_upVector;
}

float tFrustum::getWNear()
{
	return this->m_wNear;
}