#pragma once
#include "GameEngine.h"
#include "CBinaryTree.h"
#include "cSceneManager.h"

class tFrustum
{
public:
	tFrustum(Vector3d& camPos, Vector3d& camViewPos, float zNearDist,
			float zFarDist, Vector3d& upVector, float fov, float aspectRation);

	void update();
	void draw();

	Vector3d getPosition();
	Vector3d getViewPoint();
	Vector3d getUpVector();
	float getWNear();

	void setSceneManager(cSceneManager* sceneManager_);
	void setIsIgnoreFrustum(bool value);
	void eraseIdRoomNeedDraw();
	//find set of room which we need to draw
	//(according to the portals of the camera's room inside the frustum)
	void rec_findRoomsToDraw(int roomID);
	vector<int>* getIdRoomsNeedDraw();
	
	Vector3d* getNearPlanesVerts();
	Vector3d* getFarPlanesVerts();
	Vector3d* getNearPlaneCenter();
	Vector3d* getFarPlaneCenter();

private:
	//		VARIABLES:
	Vector3d m_camPos;
	Vector3d m_camViewPos;//look at point
	float m_zNearDist;
	float m_zFarDist;
	Vector3d m_upVector;
	//Vector3d m_rightVector;
	float m_hNear;//height
	float m_wNear;//width
	float m_hFar;
	float m_wFar;
	float m_fov;
	float m_aspectRation;
	//[0] - ntl
	//[1] - ntr
	//[2] - nbr
	//[3] - nbl
	Vector3d m_nearPlaneVerts[4];
	Vector3d m_nearPlaneCenter;
	//[0] - ftl
	//[1] - ftr
	//[2] - fbr
	//[3] - fbl
	Vector3d m_farPlaneVerts[4];
	Vector3d m_farPlaneCenter;

	bool m_isIgnoreFrustum;
	cSceneManager* m_sceneManager;
	vector<int> m_idRoomsNeedDraw;

	//		FUNCTIONS:
	void checkForMovement();
	void updateCenterPosAndCenterView(float speed);
	void updateCornerVertices();
	void rotateView(float angle, float x, float y, float z);

	bool PointInFrustum(float x, float y, float z);
	bool PortalInFrustum( tPortal& portal );

	void findRoomsWithThisPortal(tPortal& portal, vector<int>& idRoomsWithThisPortal);
};